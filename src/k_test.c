#include <stdio.h>
#include <stdbool.h>

int bit_pair_reverse(int num, unsigned int N);
unsigned int gen_weight(unsigned int lin_idx,
                        unsigned int stage,
                        unsigned int N_exp);

int main(void)
{
    unsigned int i, nstages, lin_idx;
    unsigned int stage;
    unsigned int idx, weight;
    bool rdx2_flag = false;
    unsigned int N_exp = 5;
    unsigned int N = (1 << N_exp);

    i = 1;
    while (i < N) {
        i = i << 2;  // i = i*4
        ++nstages;
    }

    stage = 0;
    for (lin_idx = 0; lin_idx < N * nstages; ++lin_idx) {
        if ( lin_idx % N == 0 ) {
            stage = lin_idx >> N_exp;
            if ((stage == nstages-1) && (N_exp % 2 != 0)) {
                rdx2_flag = true;
                printf("\n\nstage %d (rdx2)\n", stage);
            } else {
                rdx2_flag = false;
                printf("\n\nstage %d (rdx4)\n", stage);
            }
        }
        weight = gen_weight(lin_idx, stage, N_exp);
    }


    return 0;
}

/*
 * Split a number into bit pairs and reverse their order. If there is an odd
 * number of bits, the last (LSB) "bit pair" is only one bit.
 */
int bit_pair_reverse(int num, unsigned int nbits)
{
    unsigned int npairs = (nbits + 1) / 2;
    int bit_pairs[npairs];  // [0] ... LSB
    int bit_pair, sh;
    int i = 0;

    //printf("\nnum: %x\n", num);
    //printf("npairs: %d\n", npairs);
    if (nbits % 2 != 0) {
        bit_pairs[i] = (0x00000001 & num);
        num >>= 1;
        num += (bit_pair << (nbits-1));
        //printf("bit pair %d: %d\n", i, bit_pairs[i]);
        ++nbits;
        ++i;
    }
    while (i < npairs) {
        bit_pairs[i] = (0x00000003 & num);
        num >>= 2;
        //printf("bit pair %d: %d\n", i, bit_pairs[i]);
        ++i;
    }
    num = 0;
    while (--i >= 0) {
        sh = (nbits-2)-2*i;
        num += (bit_pairs[i] << sh);
        //printf("bit pair %d, num: %x, sh: %d\n", i, num, sh);
    }

    return num;
}

/*
 * Function for generating k for twiddle factors.
 */

unsigned int gen_weight(unsigned int lin_idx,
                        unsigned int stage,
                        unsigned int N_exp)
{
    unsigned int idx, weight;

    idx = lin_idx;
    lin_idx >>= (N_exp-2*stage);
    N_exp -= N_exp-2*stage;
    weight = bit_pair_reverse(lin_idx, N_exp);
    printf("i: %d\tk: %d\tli: %d\tN: %d", idx, weight, lin_idx, N_exp);

    return weight;
}
