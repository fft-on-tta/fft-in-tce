# src #

C sources of TTA programs. Contains some old testing programs. They can be
compiled with 'tcecc'.

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/fft-in-tce/src
├── k_test.c
├── main_test.c
└── README.md
~~~~
