#define DBG  // debug

#ifdef DBG
#include <stdio.h>
#endif /* DBG */

#include "tceops.h"
#include "/home/zadnik/.tce/opset/custom/complex_int.h"

volatile unsigned int address = 0;               // output

int main (void)
{
    unsigned int lin_idx, N_exp, base_addr;  // inputs
    unsigned int i, nstages, k;
    unsigned int N = (1 << N_exp);
    unsigned int tf_word;
    Complex tf;
    short temp;

    Complex in1, in2, in3, in4, res;
    unsigned char opcode, rdx2_flag;

    N_exp     = 4;
    base_addr = 0;

    i = 1;
    nstages = 0;
    while (i < N) {
        i = i << 2;  // i = i*4
        ++nstages;
    }

    for (lin_idx = 0; lin_idx < N * nstages; ++lin_idx) {
        if ( lin_idx % N == 0 )
            iprintf("stage %d\n", lin_idx >> N_exp);
        //_TCE_AG(lin_idx, N_exp, base_addr, address);
        _TCE_TFG(lin_idx, N_exp, Word(tf));
        //_TCE_TFGEN(lin_idx, N_exp, k);
        iprintf("%3d: %7d\t+ %7d i\t%d\n", lin_idx%N, Re(tf), Im(tf), Word(tf));
        //iprintf("%3d: %d\n", lin_idx%N, k);
    }

    /*for (i = 0; i < 4; ++i) {
        opcode = (i << 1) + rdx2_flag;
        //_TCE_CADD_BFLY(Word(in1), Word(in1), Word(in1), Word(in1),
        //               opcode, Word(res));
        //_TCE_CMUL(Word(in1), Word(in1), Word(res));
        iprintf("%3d + %3d i\n", Re(res), Im(res));
    }*/




#ifdef DBG
    iprintf("debug_mode");
#endif /* DBG */

    return 0;
}
