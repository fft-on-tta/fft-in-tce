#!/bin/sh
# Script for generating the processor. It requires internal TCE version. This
# version is only available to the developers and fellow researchers with
# a special permission. This restriction is necessary since my processor uses
# a loop buffer available only under the internal version.
#
# tcemc-secrets is a private repository where I store files from the internal
# TCE version.
base_name='fft_full_lbuf_par'
ADF_FILE=$GITDIR"/tcemc-secrets/adf/${base_name}.adf"
OUT_DIR=$GITDIR"/fft-in-tce/proge_out/${base_name}"
IDF_FILE=$GITDIR"/fft-in-tce/idf/${base_name}.idf"
TPEF_FILE=$GITDIR"/tcemc-secrets/tpef/${base_name}.tpef"

# Generate architecture without testbench
generateprocessor -i $IDF_FILE -o $OUT_DIR $ADF_FILE
# Generate files with constants from compiled program
generatebits -w 4 -x $OUT_DIR -p $TPEF_FILE $ADF_FILE
# Remove memory image files as they are already precompiled
rm -rf *.img
