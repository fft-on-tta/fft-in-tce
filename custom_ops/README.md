# custom_ops #

Definitions of my custom functional units (FU). *.opp* files are used by OSAL to
define operation properties. The behaviour of the FUs is defined by a C code in
the *src* folder. This implementation of FUs is only used for simulation.

## Symlinks ##
~/.tce/opset/custom/:
* complex_int.h -> custom_ops/src/complex_int.h
* fft_mixedradix_4_2.cc -> custom_ops/src/fft_mixedradix_4_2.cc
* fft_mixedradix_4_2_1inst.cc -> custom_ops/src/fft_mixedradix_4_2_1inst.cc
* fft_mixedradix_4_2.opp -> custom_ops/fft_mixedradix_4_2.opp
* fft_mixedradix_4_2_1inst.opp -> custom_ops/fft_mixedradix_4_2_1inst.opp
* tf_lut_16384.h -> fft-in-tce/custom_ops/src/tf_lut_16384.h

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/fft-in-tce/custom_ops
├── src                           // Source files describing the FU's behaviour
├── fft_mixedradix_4_2_1inst.opp  // Updates some FUs for the final version
├── fft_mixedradix_4_2.opp        // Contains my initial FUs
└── README.md
~~~~
