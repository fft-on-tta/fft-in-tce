/*
 * Complex number data type and its usage macros
 */

#ifndef COMPLEX_INT_H
#define COMPLEX_INT_H

typedef short Scalar;
typedef union {
    int word;
    struct {
        Scalar real;
        Scalar imag;
    } cplx;
} Complex;

#define Re(a)   (a.cplx.real)
#define Im(a)   (a.cplx.imag)
#define Word(a) (a.word)

#endif /* COMPLEX_INT_H */
