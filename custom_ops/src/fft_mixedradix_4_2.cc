/*
 * OSAL behavior definition file.
 */

#include "OSAL.hh"
#include "complex_int.h"
#include "tf_lut_16384.h"

#define N_EXP_MAX 14
#define N_MAX     (1 << N_EXP_MAX)

// Scale addresses according to word length and MAU size
// wordl: 32b & MAU: 8b => 4
#define AG_SCALE 4

/*
 * Helper functions
 */

int bit_pair_reverse(int num, unsigned int N);

/*
 * Functional units
 */

/********** AG **********/
OPERATION(AG)
TRIGGER

unsigned int lin_idx   = UINT(1);  // Linear index
unsigned int N_exp     = UINT(2);  // FFT length
unsigned int base_addr = UINT(3);  // Base address (= address offset)
unsigned int addr;                 // Output calculated address

unsigned int stage, last_stage;
unsigned int idx;
unsigned int lsbs, pos, left, right;

/* Determine current stage */
stage = lin_idx >> N_exp;

/* Last stage? */
last_stage = 0;
if ( stage == (N_exp+1)/2 - 1 ) {
    last_stage = 1;
}

/*
 * Generate address (stride permutation)
 * In the last stage the operand access is linear => no need to compute
 */
idx = lin_idx % (1 << N_exp);
if (!last_stage) {
    /* Position where to divide idx and insert the two LSBs */
    pos   = N_exp - 2*stage;
    /* Divide idx in 3 parts and move them to the right position */
    lsbs  = (idx & 3);
    left  = (idx >> pos)        << pos;
    right = (idx - left - lsbs) >> 2;
    lsbs  = lsbs                << (pos-2);
    /* Combine the parts together */
    addr = left + lsbs + right;
} else {
    addr = idx;
}

addr = ((addr + base_addr) * AG_SCALE);

IO(4) = static_cast<unsigned> (addr);
END_TRIGGER;
END_OPERATION(AG);

/********** CMUL **********/
/*
 * Using CMUL for real numbers (y = r1 * r2):
 * r1 << 8, r2 << 7 (or the other way) before feeding into CMUL to compensate
 * the >>15 shift.
 * Result will be ((r1<<8)*(r2<<7)) >> 15 = r1*r2
 */
OPERATION(CMUL)
TRIGGER

Complex in1, in2, res;
Word(in1) = INT(1);
Word(in2) = INT(2);

/* One operand comes from CADD so it is already divided by 2
 * => shift 15 is enough
 */
Re(res) = ((Re(in1) * Re(in2)) >> 15) - ((Im(in1) * Im(in2)) >> 15);
Im(res) = ((Re(in1) * Im(in2)) >> 15) + ((Im(in1) * Re(in2)) >> 15);

IO(3) = Word(res);
END_TRIGGER;
END_OPERATION(CMUL);

/********** CADD **********/
/* Overflow is prevented by dividing every addition by two */
OPERATION(CADD)
TRIGGER

Complex in1, in2, in3, in4, res;
Word(in1) = INT(1);
Word(in2) = INT(2);
Word(in3) = INT(3);
Word(in4) = INT(4);
unsigned char opcode = UINT(5);
unsigned char rdx2_flag = (opcode & 1);
opcode = opcode >> 1;

/* Debug output */
/*IO(6) = (((opcode<<1)+rdx2_flag)<<16)
        + (Word(in1)<<12)
        + (Word(in2)<<8)
        + (Word(in3)<<4)
        + (Word(in4)<<0); */

if (rdx2_flag == 0) {    // rdx4 butterfly
    switch (opcode) {
    case 0:
        Re(res) = ( ((Re(in1) + Re(in2)) >> 1) +
                    ((Re(in3) + Re(in4)) >> 1) ) >> 1;
        Im(res) = ( ((Im(in1) + Im(in2)) >> 1) +
                    ((Im(in3) + Im(in4)) >> 1) ) >> 1;
        break;
    case 1:
        Re(res) = ( ((Re(in1) + Im(in2)) >> 1) -
                    ((Re(in3) + Im(in4)) >> 1) ) >> 1;
        Im(res) = ( ((Im(in1) - Re(in2)) >> 1) -
                    ((Im(in3) - Re(in4)) >> 1) ) >> 1;
        break;
    case 2:
        Re(res) = ( ((Re(in1) - Re(in2)) >> 1) +
                    ((Re(in3) - Re(in4)) >> 1) ) >> 1;
        Im(res) = ( ((Im(in1) - Im(in2)) >> 1) +
                    ((Im(in3) - Im(in4)) >> 1) ) >> 1;
        break;
    case 3:
        Re(res) = ( ((Re(in1) + Im(in2)) >> 1) -
                    ((Re(in3) + Im(in4)) >> 1) ) >> 1;
        Im(res) = ( ((Im(in1) - Re(in2)) >> 1) -
                    ((Im(in3) - Re(in4)) >> 1) ) >> 1;
        break;
    default:
        Re(res) = 0;
        Im(res) = 0;
        break;
    }
} else {                 // rdx2 butterfly
    switch (opcode) {
    case 0:
        Re(res) = (Re(in1) + Re(in2)) >> 1;
        Im(res) = (Im(in1) + Im(in2)) >> 1;
        break;
    case 1:
        Re(res) = (Re(in1) - Re(in2)) >> 1;
        Im(res) = (Im(in1) - Im(in2)) >> 1;
        break;
    case 2:
        Re(res) = (Re(in3) + Re(in4)) >> 1;
        Im(res) = (Im(in3) + Im(in4)) >> 1;
        break;
    case 3:
        Re(res) = (Re(in3) - Re(in4)) >> 1;
        Im(res) = (Im(in3) - Im(in4)) >> 1;
        break;
    default:
        Re(res) = 0;
        Im(res) = 0;
        break;
    }
}

IO(6) = Word(res);
END_TRIGGER;
END_OPERATION(CADD);

/********** TFG **********/
OPERATION(TFG)
TRIGGER

unsigned int lin_idx   = UINT(1);
unsigned int N_exp     = UINT(2);
Complex tf;

unsigned int k, stage, weight, N_exp_sc, idx, eighth;
unsigned char sh;
unsigned char radix = 4;
unsigned int rdx2_flag = 0;

/* Current stage */
stage = lin_idx >> N_exp;
/* For odd N_exp there will be radix-2 */
if ( N_exp % 2 ) {
    /* Last stage? (=> radix-2 now?) */
    if ( stage == (N_exp+1)/2 - 1 ) {
        radix = 2;
        rdx2_flag = 1;
    }
}

/* After determining the stage, remove the stage information from lin_idx */
sh = 32 - N_exp;
lin_idx = (lin_idx << sh) >> sh;

/* Generate k */
weight = 0;

/* Base k */
k = lin_idx % radix;
/* Calculate k */
idx      = lin_idx >> (N_exp - (stage << 1));
N_exp_sc = (stage << 1);
weight   = bit_pair_reverse(idx, N_exp_sc);
k        = k * weight;
/* Scale k */
k <<= ( N_EXP_MAX - (stage<<1) - 2 );
if (radix == 2) {
    k <<= 1;
}

eighth = N_MAX/8;

if (k <= eighth) {                                 // B0
    Re(tf) = tf_lut_real[k];
    Im(tf) = tf_lut_imag[k];
} else if ( (k > 1*eighth) && (k <= 2*eighth) ) {  // B1
    Re(tf) = -tf_lut_imag[2*eighth-k];
    Im(tf) = -tf_lut_real[2*eighth-k];
} else if ( (k > 2*eighth) && (k <= 3*eighth) ) {  // B2
    Re(tf) = tf_lut_imag[k-2*eighth];
    Im(tf) = -tf_lut_real[k-2*eighth];
} else if ( (k > 3*eighth) && (k <= 4*eighth) ) {  // B3
    Re(tf) = -tf_lut_real[4*eighth-k];
    Im(tf) = tf_lut_imag[4*eighth-k];
} else if ( (k > 4*eighth) && (k <= 5*eighth) ) {  // B4
    Re(tf) = -tf_lut_real[k-4*eighth];
    Im(tf) = -tf_lut_imag[k-4*eighth];
} else if ( (k > 5*eighth) && (k <= 6*eighth) ) {  // B5
    Re(tf) = tf_lut_imag[6*eighth-k];
    Im(tf) = tf_lut_real[6*eighth-k];
} else {                                           // Shouldn't be here
    Re(tf) = 0;
    Im(tf) = 0;
}


IO(3) = Word(tf);
IO(4) = rdx2_flag;//static_cast<unsigned> (rdx2_flag);
END_TRIGGER;
END_OPERATION(TFG);


/******** TFG_K (testing only) ***************/
/* Just copy-pasted TFG with uint output set to k. */
OPERATION(TFG_K)
TRIGGER

unsigned int lin_idx   = UINT(1);
unsigned int N_exp     = UINT(2);
Complex tf;

unsigned int k, stage, weight, N_exp_sc, idx;
unsigned char sh;
unsigned char radix = 4;

/* Current stage */
stage = lin_idx >> N_exp;
/* For odd N_exp there will be radix-2 */
if ( N_exp % 2 ) {
    /* Last stage? (=> radix-2 now?) */
    if ( stage == (N_exp+1)/2 - 1 ) {
        radix = 2;
    }
}

/* After determining the stage, remove the stage information from lin_idx */
sh = 32 - N_exp;
lin_idx = (lin_idx << sh) >> sh;

/* Generate k */
weight = 0;

/* Base k */
k = lin_idx % radix;
/* Calculate k */
idx      = lin_idx >> (N_exp - (stage << 1));
N_exp_sc = (stage << 1);
weight   = bit_pair_reverse(idx, N_exp_sc);
k        = k * weight;
/* Scale k */
k <<= ( N_EXP_MAX - (stage<<1) - 2 );
if (radix == 2) {
    k <<= 1;
}

IO(3) = k;
END_TRIGGER;
END_OPERATION(TFG_K);

/*
 * Helper functions
 */

/*
 * Split a number into bit pairs and reverse their order. Start splitting
 * from LSB. If there is an odd number of bits, the last (MSB) "bit pair" is
 * only one bit.
 */
int bit_pair_reverse(int num, unsigned int nbits)
{
    unsigned int nbits_is_odd = nbits % 2;
    unsigned int npairs = (nbits + 1) / 2;  // number of bit pairs
    int bit_pairs[npairs];  // [0] ... LSB
    int sh;
    int i = 0;

    while (i < npairs) {
        if ( (nbits_is_odd) && (i >= npairs-1) ) {
                bit_pairs[i] = (0x00000001 & num);
        } else {
            bit_pairs[i] = (0x00000003 & num);
        }
        num >>= 2;
        ++i;
    }
    num = 0;
    while (--i >= 0) {
        sh = 2*(npairs - (1+i)) - nbits_is_odd;
        if (sh < 0) {
            sh = 0;
        }
        num += (bit_pairs[i] << sh);
    }

    return num;
}
