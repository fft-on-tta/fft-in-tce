/*
 * Testing file for custom operations before they get added to the opset
 */

#include <stdio.h>
#include <stdint.h>

#include "tf_lut_16384.h"

/*
 * Helper functions declarations
 */

uint32_t det_stages (uint32_t N);

/*
 * Custom operation functions declarations
 */

/* Address generator */
uint32_t ag ( uint32_t lin_idx,
              uint32_t N,
              uint32_t base_addr );



/* Main */
int main (void)
{
    uint32_t N_exp = 7;
    uint32_t N = (1 << N_exp);
    uint32_t nstages = det_stages(N);
    uint32_t addr, i;

    /*for (i = 0; i < (N * nstages); ++i) {
        if ( i % N == 0 )
            printf("stage %d\n", i >> N_exp);
        addr = ag(i, N_exp, 0);
        printf("%3d: %3d\n", i%N, addr);
    }*/

    return 0;
}

/*
 * Helper functions definitions
 */

uint32_t det_stages (uint32_t N)
{
    uint32_t i   = 1;
    uint32_t exp = 0;

    while (i < N) {
        i = i << 2;  // i = i*4
        ++exp;
    }

    return exp;
}

/*
 * Custom operation functions definitions
 */
uint32_t ag ( uint32_t lin_idx,
              uint32_t N_exp,
              uint32_t base_addr )
{
    /* Determine current stage */
    uint32_t stage = lin_idx >> N_exp;

    /* Last stage? */
    uint32_t last_stage = 0;
    if ( stage == (N_exp+1)/2 - 1 ) {
        last_stage = 1;
    }

    /*
     * Generate address (stride permutation)
     * In the last stage the operand access is linear => no need to compute
     */
    uint32_t idx = lin_idx % (1 << N_exp);
    uint32_t lsbs, pos, left, right, addr;
    if (!last_stage) {
        /* Position where to divide idx and insert the two LSBs */
        pos   = N_exp - 2*stage;
        /* Divide idx in 3 parts and move them to the right position */
        lsbs  = (idx & 3);
        left  = (idx >> pos)        << pos;
        right = (idx - left - lsbs) >> 2;
        lsbs  = lsbs                << (pos-2);
        /* Combine the parts together */
        addr = left + lsbs + right;
    } else {
        addr = idx;
    }

    return addr + base_addr;
}
