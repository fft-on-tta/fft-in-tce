# custom_ops/src #

Source files describing a behaviour of my functional units (FUs).

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/fft-in-tce/custom_ops/src
├── complex_int.h                // Custom datatype for complex numbers
├── fft_mixedradix_4_2_1inst.cc  // Some updated FUs for the final version
├── fft_mixedradix_4_2.cc        // Most FUs are described here
├── main_test_fu.c               // Old file for testing FUs in C
├── main_test_tf.c  // Generating twiddle factors from the previous project
├── README.md
└── tf_lut_16384.h  // Contains twiddle factors necessary for FFT of size 16384
~~~~
