/*
 * OSAL behavior definition file.
 */

#include "OSAL.hh"
#include "complex_int.h"
#include "tf_lut_16384.h"

#define N_EXP_MAX 14
#define N_MAX     (1 << N_EXP_MAX)


/*
 * Functional units
 */

/********** CADD1 internal register state  *************/
DEFINE_STATE(CADD1_REG)
    int opcode;     // modulo 4
    unsigned int rx2_mem;
    unsigned int rdx2_flag;
    Complex in0, in1, in2, in3;  // cadd inputs
    Complex mem[4];              // temporary input storage

INIT_STATE(CADD1_REG)
    opcode = 0;

    rx2_mem = 0;
    rdx2_flag = 0;

    Word(mem[0]) = 0;
    Word(mem[1]) = 0;
    Word(mem[2]) = 0;
    Word(mem[3]) = 0;

    Word(in0) = 0;
    Word(in1) = 0;
    Word(in2) = 0;
    Word(in3) = 0;

END_INIT_STATE;
END_DEFINE_STATE

/********** CADD1 **********/
/* Complex adder with one input.
 *
 * Input values are fed in series one after each other. Every 3rd cycle the 4
 * values from memory are fed into the cadd inputs. Therefore the
 * inputs stay the same for 4 triggers with only opcode changing. Radix-2
 * info is loaded every 0th trigger and used for computation every 3rd trigger
 * with the operands.
 *
 * Overflow is prevented by dividing every addition by two.
 */
OPERATION_WITH_STATE(CADD1, CADD1_REG)
TRIGGER

    Complex res;  // result

    Word(STATE.mem[STATE.opcode]) = INT(1);

    /* Every 0th trigger load radix-2 info */
    if ((STATE.opcode % 4) == 0) {
        STATE.rx2_mem = UINT(2) & 0x00000001;
    /* Every 3rd trigger load previous 3 operands + the current one */
    } else if ((STATE.opcode % 4) == 3) {
        Word(STATE.in0) = Word(STATE.mem[0]);
        Word(STATE.in1) = Word(STATE.mem[1]);
        Word(STATE.in2) = Word(STATE.mem[2]);
        Word(STATE.in3) = Word(STATE.mem[3]);
        STATE.rdx2_flag = STATE.rx2_mem;
    }
    /* Increment opcode so the result is ready the next trigger */
    if (++STATE.opcode > 3) {
        STATE.opcode = 0;
    }

    /* Debug output (feed it with 0, 1, 2, 3, ...) */
    /*IO(3) = ( ((STATE.opcode<<1) + STATE.rdx2_flag) << 16)
            | (Word(STATE.in0) << 12)
            | (Word(STATE.in1) << 8)
            | (Word(STATE.in2) << 4)
            | (Word(STATE.in3) << 0);*/

    /* Start computation for the next cycle */
    if (STATE.rdx2_flag == 0) {    // rdx4 butterfly
        switch (STATE.opcode) {
        case 0:
            Re(res) = (((Re(STATE.in0) + Re(STATE.in1)) >> 1) +
                       ((Re(STATE.in2) + Re(STATE.in3)) >> 1)) >> 1;
            Im(res) = (((Im(STATE.in0) + Im(STATE.in1)) >> 1) +
                       ((Im(STATE.in2) + Im(STATE.in3)) >> 1)) >> 1;
            break;
        case 1:
            Re(res) = (((Re(STATE.in0) + Im(STATE.in1)) >> 1) -
                       ((Re(STATE.in2) + Im(STATE.in3)) >> 1)) >> 1;
            Im(res) = (((Im(STATE.in0) - Re(STATE.in1)) >> 1) -
                       ((Im(STATE.in2) - Re(STATE.in3)) >> 1)) >> 1;
            break;
        case 2:
            Re(res) = (((Re(STATE.in0) - Re(STATE.in1)) >> 1) +
                       ((Re(STATE.in2) - Re(STATE.in3)) >> 1)) >> 1;
            Im(res) = (((Im(STATE.in0) - Im(STATE.in1)) >> 1) +
                       ((Im(STATE.in2) - Im(STATE.in3)) >> 1)) >> 1;
            break;
        case 3:
            Re(res) = (((Re(STATE.in0) - Im(STATE.in1)) >> 1) -
                       ((Re(STATE.in2) - Im(STATE.in3)) >> 1)) >> 1;
            Im(res) = (((Im(STATE.in0) + Re(STATE.in1)) >> 1) -
                       ((Im(STATE.in2) + Re(STATE.in3)) >> 1)) >> 1;
            break;
        default:
            Re(res) = 0;
            Im(res) = 0;
            break;
        }
    } else {                 // rdx2 butterfly
        switch (STATE.opcode) {
        case 0:
            Re(res) = (Re(STATE.in0) + Re(STATE.in1)) >> 1;
            Im(res) = (Im(STATE.in0) + Im(STATE.in1)) >> 1;
            break;
        case 1:
            Re(res) = (Re(STATE.in0) - Re(STATE.in1)) >> 1;
            Im(res) = (Im(STATE.in0) - Im(STATE.in1)) >> 1;
            break;
        case 2:
            Re(res) = (Re(STATE.in2) + Re(STATE.in3)) >> 1;
            Im(res) = (Im(STATE.in2) + Im(STATE.in3)) >> 1;
            break;
        case 3:
            Re(res) = (Re(STATE.in2) - Re(STATE.in3)) >> 1;
            Im(res) = (Im(STATE.in2) - Im(STATE.in3)) >> 1;
            break;
        default:
            Re(res) = 0;
            Im(res) = 0;
            break;
        }
    }


    IO(3) = Word(res);

END_TRIGGER;
END_OPERATION_WITH_STATE(CADD1);

/*********** DLY ***************/
OPERATION(DLY);
TRIGGER;

    IO(2) = INT(1);

END_TRIGGER;
END_OPERATION(DLY);
