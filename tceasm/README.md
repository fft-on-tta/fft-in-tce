# tceasm #

Assembly sources for TTA processors

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/fft-in-tce/tceasm
├── bfly_1inst.tceasm    // Final schedule using comparing logic
├── bfly_1loop.tceasm    // Previous schedule with longer kernel
├── fft_full_lbuf.tceasm // Final schedule using a loop buffer
├── README.md
├── test_ag.tceasm       // All 'test_*' are testing programs for reftest
├── test_cadd1_2.tceasm  // ...
├── test_cadd1.tceasm    // ...
├── test_teemu_tf.tceasm // ...
├── test_tfg_k.tceasm    // ...
└── test_tfg_tf.tceasm   // ...
~~~~
