--===========================================================================--
-- Processor top-level module
--
-- This file is a modified version of concatenated proc_ent and proc_arch VHDL
-- templates from TTA-Based Codesign Environment:
-- https://github.com/cpc/tce/blob/release-1.16/tce/data/ProGe/tb/proc_ent.vhdl.tmpl
-- https://github.com/cpc/tce/blob/release-1.16/tce/data/ProGe/tb/proc_arch.vhdl.tmpl
--
-- It is a complete synthesizable processor with its data and instruction
-- memories. Allows external access to the memories and external reset.
--===========================================================================--


--===========================================================================--
-- Entity of proc
-------------------------------------------------------------------------------
-- Inputs:
--   clk               : Clock
--   rst_x             : Asynchronous reset, active low
--   mem_wr_lock       : Blocks the processor but allows memories to be written
--                       to. Only meaningful with par_mem_logic. Active high.
--   -- interface to access internal data memory
--   dmem_ext_bit_wr_x : Bit-mask to select which bits are written (active low)
--   dmem_ext_wr_x     : Write enable (active low)
--   dmem_ext_en_x     : Memory enable (active low)
--   dmem_ext_data     : Data and address for internal data memory
--   dmem_ext_addr     : Data and address for internal data memory
--   -- interface to access internal program (instruction) memory
--   imem_ext_bit_wr_x : Bit-mask to select which bits are written (active low)
--   imem_ext_wr_x     : Write enable (active low)
--   imem_ext_en_x     : Memory enable (active low)
--   imem_ext_data     : Data and address for internal data memory
--   imem_ext_addr     : Data and address for internal data memory
--
-- Outputs:
--   data_out          : Data from internal data memory
--   dmem_busy         : Indication of data memory being used by core
--   imem_busy         : Indication of instruction memory being used by core
--   locked            : Status signal from core
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

use work.proc_constants.all;
use work.tta0_globals.all;
use work.tta0_params.all;
use work.tta0_imem_mau.all;

entity proc is
    port(
        clk               : in  std_logic;
        rst_x             : in  std_logic;
        mem_wr_lock       : in  std_logic;
        dmem_ext_bit_wr_x : in  std_logic_vector(DMEMDATAWIDTH-1 downto 0);
        dmem_ext_wr_x     : in  std_logic;
        dmem_ext_en_x     : in  std_logic;
        dmem_ext_data     : in  std_logic_vector(DMEMDATAWIDTH-1 downto 0);
        dmem_ext_addr     : in  std_logic_vector(DMEMADDRWIDTH-1 downto 0);
        data_out          : out std_logic_vector(DMEMDATAWIDTH-1 downto 0);
        imem_ext_bit_wr_x : in  std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                                downto 0);
        imem_ext_wr_x     : in  std_logic;
        imem_ext_en_x     : in  std_logic;
        imem_ext_data     : in  std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                                downto 0);
        imem_ext_addr     : in  std_logic_vector(IMEMADDRWIDTH-1 downto 0);

        core_busy         : in  std_logic;
        dmem_busy         : out std_logic;
        imem_busy         : out std_logic;
        locked            : out std_logic);
end proc;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of proc
-------------------------------------------------------------------------------
-- Components:
--   tta0                         : Processor core
--   synch_dualport_mem_wrapper   : Doubleport data memory supporting 2 LSUs
--   synch_singleport_mem_wrapper : Singleport instruction memory
--   control_arbiter              : Arbiter for reset and lock signals*
--   mem_arbiter                  : Arbiter for data memory*
--   imem_arbiter                 : Arbiter for instruction memory*
--
-- Configurations of:
--   synch_dualport_mem_wrapper
--   synch_singleport_mem_wrapper
--
-- * Arbiters select whether internal or external components are in charge.
-------------------------------------------------------------------------------
architecture structural of proc is

    component tta0
    generic (
        core_id            : integer := 0);
    port (
        clk                : in  std_logic;
        rstx               : in  std_logic;
        busy               : in  std_logic;
        imem_en_x          : out std_logic;
        imem_addr          : out std_logic_vector(IMEMADDRWIDTH-1 downto 0);
        imem_data          : in  std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH
                                                  -1 downto 0);
        locked             : out std_logic;
        fu_LSU_R_lock_from_pmem : in std_logic_vector(0 downto 0);
        fu_LSU_R_data_in   : in  std_logic_vector(fu_LSU_R_dataw-1 downto 0);
        fu_LSU_R_data_out  : out std_logic_vector(fu_LSU_R_dataw-1 downto 0);
        fu_LSU_R_addr      : out std_logic_vector(fu_LSU_R_addrw-2-1 downto 0);
        fu_LSU_R_mem_en_x  : out std_logic_vector(0 downto 0);
        fu_LSU_R_wr_en_x   : out std_logic_vector(0 downto 0);
        fu_LSU_R_wr_mask_x : out std_logic_vector(fu_LSU_R_dataw-1 downto 0);
        fu_LSU_W_lock_from_pmem : in std_logic_vector(0 downto 0);
        fu_LSU_W_data_in   : in  std_logic_vector(fu_LSU_W_dataw-1 downto 0);
        fu_LSU_W_data_out  : out std_logic_vector(fu_LSU_W_dataw-1 downto 0);
        fu_LSU_W_addr      : out std_logic_vector(fu_LSU_W_addrw-2-1 downto 0);
        fu_LSU_W_mem_en_x  : out std_logic_vector(0 downto 0);
        fu_LSU_W_wr_en_x   : out std_logic_vector(0 downto 0);
        fu_LSU_W_wr_mask_x : out std_logic_vector(fu_LSU_W_dataw-1 downto 0));
    end component;

    component synch_dualport_mem_wrapper
    generic (
        DATAW        : integer;
        ADDRW        : integer);
    port (
        clk          : in  std_logic;
        d_a          : in  std_logic_vector(DATAW-1 downto 0);
        d_b          : in  std_logic_vector(DATAW-1 downto 0);
        addr_a       : in  std_logic_vector(ADDRW-1 downto 0);
        addr_b       : in  std_logic_vector(ADDRW-1 downto 0);
        en_a_x       : in  std_logic;
        en_b_x       : in  std_logic;
        wr_a_x       : in  std_logic;
        wr_b_x       : in  std_logic;
        bit_wr_a_x   : in  std_logic_vector(DATAW-1 downto 0);
        bit_wr_b_x   : in  std_logic_vector(DATAW-1 downto 0);
        q_a          : out std_logic_vector(DATAW-1 downto 0);
        q_b          : out std_logic_vector(DATAW-1 downto 0);
        rst_x        : in  std_logic;
        glock        : in  std_logic;
        lockrq       : out std_logic);
    end component;

    component synch_singleport_mem_wrapper
    generic (
        DATAW    : integer;
        ADDRW    : integer);
    port (
        clk      : in  std_logic;
        d        : in  std_logic_vector(DATAW-1 downto 0);
        addr     : in  std_logic_vector(ADDRW-1 downto 0);
        en_x     : in  std_logic;
        wr_x     : in  std_logic;
        bit_wr_x : in  std_logic_vector(DATAW-1 downto 0);
        rst_x    : in  std_logic;
        glock    : in  std_logic;
        q        : out std_logic_vector(DATAW-1 downto 0));
    end component;

    component control_arbiter
    port (
        mem_wr_lock : in  std_logic;
        rstx        : in  std_logic;
        locked      : in  std_logic;
        rstx_proc   : out std_logic;
        rstx_mem    : out std_logic;
        glock_mem   : out std_logic);
    end component;

    component mem_arbiter
    generic (
        PORTW     : integer;
        ADDRWIDTH : integer);
    port (
        d_1        : in  std_logic_vector(PORTW-1 downto 0);
        d_2        : in  std_logic_vector(PORTW-1 downto 0);
        d          : out std_logic_vector(PORTW-1 downto 0);
        addr_1     : in  std_logic_vector(ADDRWIDTH-1 downto 0);
        addr_2     : in  std_logic_vector(ADDRWIDTH-1 downto 0);
        addr       : out std_logic_vector(ADDRWIDTH-1 downto 0);
        en_1_x     : in  std_logic;
        en_2_x     : in  std_logic;
        en_x       : out std_logic;
        wr_1_x     : in  std_logic;
        wr_2_x     : in  std_logic;
        wr_x       : out std_logic;
        bit_wr_1_x : in  std_logic_vector(PORTW-1 downto 0);
        bit_wr_2_x : in  std_logic_vector(PORTW-1 downto 0);
        bit_wr_x   : out std_logic_vector(PORTW-1 downto 0);
        mem_busy   : out std_logic);
    end component;

    component imem_arbiter
    generic (
        PORTW      : integer := 32;
        ADDRWIDTH  : integer := 11);
    port (
        d_2        : in  std_logic_vector(PORTW-1 downto 0);
        d          : out std_logic_vector(PORTW-1 downto 0);
        addr_1     : in  std_logic_vector(ADDRWIDTH-1 downto 0);
        addr_2     : in  std_logic_vector(ADDRWIDTH-1 downto 0);
        addr       : out std_logic_vector(ADDRWIDTH-1 downto 0);
        en_1_x     : in  std_logic;
        en_2_x     : in  std_logic;
        en_x       : out std_logic;
        wr_2_x     : in  std_logic;
        wr_x       : out std_logic;
        bit_wr_2_x : in  std_logic_vector(PORTW-1 downto 0);
        bit_wr_x   : out std_logic_vector(PORTW-1 downto 0);
        mem_busy   : out std_logic);
    end component;

    signal dmem_q_a          : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
    signal dmem_d_a          : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
    signal dmem_addr_a       : std_logic_vector(DMEMADDRWIDTH-1 downto 0);
    signal dmem_en_a_x       : std_logic;
    signal dmem_wr_a_x       : std_logic;
    signal dmem_bit_wr_a_x   : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
    signal dmem_q_b          : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
    signal dmem_d_b          : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
    signal dmem_addr_b       : std_logic_vector(DMEMADDRWIDTH-1 downto 0);
    signal dmem_en_b_x       : std_logic;   --:='1';
    signal dmem_wr_b_x       : std_logic;
    signal dmem_bit_wr_b_x   : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
    signal s_dmem_busy       : std_logic;

    signal imem_data         : std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                              downto 0);
    signal imem_addr         : std_logic_vector(IMEMADDRWIDTH-1 downto 0);
    signal imem_en_x         : std_logic;
    signal s_imem_busy       : std_logic;

    signal q_a               : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
    signal d_a               : std_logic_vector(DMEMDATAWIDTH-1 downto 0);
    signal addr_a            : std_logic_vector(DMEMADDRWIDTH-1 downto 0);
    signal en_a_x            : std_logic;
    signal en_b_x            : std_logic;
    signal wr_a_x            : std_logic;
    signal bit_wr_a_x        : std_logic_vector(DMEMDATAWIDTH-1 downto 0);

    signal imem_d_arb        : std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                                downto 0);
    signal imem_addr_arb     : std_logic_vector(IMEMADDRWIDTH-1 downto 0);
    signal imem_en_arb_x     : std_logic;
    signal imem_wr_arb_x     : std_logic;
    signal imem_bit_wr_arb_x : std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1
                                                downto 0);

    -- addresses from the TTA's LSUs (they might have different widths than
    -- data memory addresses, therefore the need for separate signals)
    signal addr_lsu_r : std_logic_vector(fu_LSU_R_addrw-2-1 downto 0);
    signal addr_lsu_w : std_logic_vector(fu_LSU_W_addrw-2-1 downto 0);

    -- resets and locks
    signal rstx_core         : std_logic;
    signal locked_core       : std_logic;
    signal glock_mem         : std_logic;
    signal rstx_mem          : std_logic;
    -- lock request from par_mem_logic to LSUs which will fetch it to the core
    signal lock_rq_from_pmem : std_logic_vector(0 downto 0);
    signal lock_rq           : std_logic; -- helper signal

begin  -- structural

    lock_rq_from_pmem(0) <= lock_rq;

    core : tta0
        port map (
            clk                     => clk,
            rstx                    => rstx_core,
            busy                    => core_busy,
            imem_en_x               => imem_en_x,
            imem_addr               => imem_addr,
            imem_data               => imem_data,
            locked                  => locked_core,
            fu_LSU_R_lock_from_pmem => lock_rq_from_pmem,
            fu_LSU_R_data_in        => dmem_q_a,
            fu_LSU_R_data_out       => dmem_d_a,
            fu_LSU_R_addr           => addr_lsu_r,
            fu_LSU_R_mem_en_x(0)    => dmem_en_a_x,
            fu_LSU_R_wr_en_x(0)     => dmem_wr_a_x,
            fu_LSU_R_wr_mask_x      => dmem_bit_wr_a_x,
            fu_LSU_W_lock_from_pmem => lock_rq_from_pmem,
            fu_LSU_W_data_in        => dmem_q_b,
            fu_LSU_W_data_out       => dmem_d_b,
            fu_LSU_W_addr           => addr_lsu_w,
            fu_LSU_W_mem_en_x(0)    => dmem_en_b_x,
            fu_LSU_W_wr_en_x(0)     => dmem_wr_b_x,
            fu_LSU_W_wr_mask_x      => dmem_bit_wr_b_x);

    dmem_addr_a <= addr_lsu_r(DMEMADDRWIDTH-1 downto 0);
    dmem_addr_b <= addr_lsu_w(DMEMADDRWIDTH-1 downto 0);

    rstx_glock_arbiter : control_arbiter
        port map (
            mem_wr_lock => mem_wr_lock,
            rstx        => rst_x,
            locked      => locked_core,
            rstx_proc   => rstx_core,
            rstx_mem    => rstx_mem,
            glock_mem   => glock_mem);

    gen_dmem_par_mem : if DP_MEM_CONF = ParMem generate
        datamem : entity work.synch_dualport_mem_wrapper(struct_par_mem)
            generic map (
                DATAW        => DMEMDATAWIDTH,
                ADDRW        => DMEMADDRWIDTH)
            port map (
                clk          => clk,
                d_a          => d_a,
                d_b          => dmem_d_b,
                addr_a       => addr_a,
                addr_b       => dmem_addr_b,
                en_a_x       => en_a_x,
                en_b_x       => dmem_en_b_x,
                wr_a_x       => wr_a_x,
                wr_b_x       => dmem_wr_b_x,
                bit_wr_a_x   => bit_wr_a_x,
                bit_wr_b_x   => dmem_bit_wr_b_x,
                q_a          => q_a,
                q_b          => dmem_q_b,
                rst_x        => rstx_mem,
                glock        => glock_mem,
                lockrq       => lock_rq);
    end generate gen_dmem_par_mem;

    gen_dmem_dualport: if DP_MEM_CONF = Dualport generate
        datamem : entity work.synch_dualport_mem_wrapper(struct_dualport)
            generic map (
                DATAW        => DMEMDATAWIDTH,
                ADDRW        => DMEMADDRWIDTH)
            port map (
                clk          => clk,
                d_a          => d_a,
                d_b          => dmem_d_b,
                addr_a       => addr_a,
                addr_b       => dmem_addr_b,
                en_a_x       => en_a_x,
                en_b_x       => dmem_en_b_x,
                wr_a_x       => wr_a_x,
                wr_b_x       => dmem_wr_b_x,
                bit_wr_a_x   => bit_wr_a_x,
                bit_wr_b_x   => dmem_bit_wr_b_x,
                q_a          => q_a,
                q_b          => dmem_q_b,
                rst_x        => rstx_mem,
                glock        => glock_mem,
                lockrq       => lock_rq);
    end generate gen_dmem_dualport;

    dmem_q_a <= q_a;

    dmem_arbiter : mem_arbiter
        generic map (
            PORTW      => DMEMDATAWIDTH,
            ADDRWIDTH  => DMEMADDRWIDTH)
        port map (
            d_1        => dmem_d_a,
            d_2        => dmem_ext_data,
            d          => d_a,
            addr_1     => dmem_addr_a,
            addr_2     => dmem_ext_addr,
            addr       => addr_a,
            en_1_x     => dmem_en_a_x,
            en_2_x     => dmem_ext_en_x,
            en_x       => en_a_x,
            wr_1_x     => dmem_wr_a_x,
            wr_2_x     => dmem_ext_wr_x,
            wr_x       => wr_a_x,
            bit_wr_1_x => dmem_bit_wr_a_x,
            bit_wr_2_x => dmem_ext_bit_wr_x,
            bit_wr_x   => bit_wr_a_x,
            mem_busy   => s_dmem_busy);

    gen_imem_cacti : if IMEM_CONF = Cacti generate
        instmem : entity work.synch_singleport_mem_wrapper(struct_cacti)
            generic map (
                DATAW    => IMEMWIDTHINMAUS*IMEMMAUWIDTH,
                ADDRW    => IMEMADDRWIDTH)
            port map (
                clk      => clk,
                d        => imem_d_arb,
                addr     => imem_addr_arb,
                en_x     => imem_en_arb_x,
                wr_x     => imem_wr_arb_x,
                bit_wr_x => imem_bit_wr_arb_x,
                rst_x    => rstx_mem,
                glock    => glock_mem,
                q        => imem_data);
    end generate gen_imem_cacti;

    gen_imem_cacti_blocks : if IMEM_CONF = CactiBlocks generate
        instmem : entity work.synch_singleport_mem_wrapper(struct_cacti_blocks)
            generic map (
                DATAW    => IMEMWIDTHINMAUS*IMEMMAUWIDTH,
                ADDRW    => IMEMADDRWIDTH)
            port map (
                clk      => clk,
                d        => imem_d_arb,
                addr     => imem_addr_arb,
                en_x     => imem_en_arb_x,
                wr_x     => imem_wr_arb_x,
                bit_wr_x => imem_bit_wr_arb_x,
                rst_x    => rstx_mem,
                glock    => glock_mem,
                q        => imem_data);
    end generate gen_imem_cacti_blocks;

    gen_imem_singleport : if IMEM_CONF = Singleport generate
        instmem : entity work.synch_singleport_mem_wrapper(struct_singleport)
            generic map (
                DATAW    => IMEMWIDTHINMAUS*IMEMMAUWIDTH,
                ADDRW    => IMEMADDRWIDTH)
            port map (
                clk      => clk,
                d        => imem_d_arb,
                addr     => imem_addr_arb,
                en_x     => imem_en_arb_x,
                wr_x     => imem_wr_arb_x,
                bit_wr_x => imem_bit_wr_arb_x,
                rst_x    => rstx_mem,
                glock    => glock_mem,
                q        => imem_data);
    end generate gen_imem_singleport;

    instmem_arbiter : imem_arbiter
        generic map (
            PORTW      => IMEMWIDTHINMAUS*IMEMMAUWIDTH,
            ADDRWIDTH  => IMEMADDRWIDTH)
        port map (
            d_2        => imem_ext_data,
            d          => imem_d_arb,
            addr_1     => imem_addr,
            addr_2     => imem_ext_addr,
            addr       => imem_addr_arb,
            en_1_x     => imem_en_x,
            en_2_x     => imem_ext_en_x,
            en_x       => imem_en_arb_x,
            wr_2_x     => imem_ext_wr_x,
            wr_x       => imem_wr_arb_x,
            bit_wr_2_x => imem_ext_bit_wr_x,
            bit_wr_x   => imem_bit_wr_arb_x,
            mem_busy   => s_imem_busy);

    -- Outputs
    data_out  <= q_a;
    dmem_busy <= s_dmem_busy;
    imem_busy <= s_imem_busy;
    locked    <= locked_core;

end structural;
--===========================================================================--
