--===========================================================================--
-- Clock generator for a testbench
--
-- It is a modified clkgen.vhdl from TTA-Based Codesign Environment:
-- https://github.com/cpc/tce/blob/release-1.16/tce/data/ProGe/tb/clkgen.vhdl
--===========================================================================--


--===========================================================================--
-- Entity of clock generator
-------------------------------------------------------------------------------
-- Generic:
--   PERIOD : Clock period
-- Input:
--   en     : Clock enable, active high
-- Output:
--   clk    : Clock signal
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity clkgen is
    generic (
        PERIOD : time := 100 ns );
    port (
        en     : in std_logic := '1';
        clk    : out std_logic);
end clkgen;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of clock generator
-------------------------------------------------------------------------------
architecture simulation of clkgen is
    signal tmp : std_logic := '1';
begin

    process(tmp)
    begin
        if en = '1' then
            tmp <= not tmp after PERIOD/2;
            clk <= tmp ;
        else
            clk <='0';
        end if;
    end process;

end simulation;
--===========================================================================--
