# This script creates necessary output directories and copies init *.img files
# to the simulation directory. Add it to the project as a simulation source.
#
# Note: The script is called AFTER the simulation runs => folders are not
# available in the first run. Just relaunch the simulation after the first run.

file mkdir res/conflicts res/ParMem_CactiBlocks

set imgDir $::env(GITDIR)/fft-in-tce/vhdl/tb/sim/img 
catch {file copy -force $imgDir .} errorDoesntMatter
