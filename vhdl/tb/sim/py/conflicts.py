"""
Detection of memory conflicts in parallel memory logic
"""

import itertools
from enum import Enum

import numpy as np

import pdb


nexp_min = 3
nexp_max = 14
rw_delay = 11  # Delay between read and write to the same address
parallel_access = 1 # 1 or 2 (2 works only with even rw_delay)


class MemFunc(Enum):
    HALF      = 0
    PARITY    = 1
    DIV10MOD2 = 2
    MOD2      = 3
    DIV8PAR   = 4


def ag(lin_idx, stage, Nexp, base_addr=0):
    """Calculate addresses of 4 operands.

    From lin_idx it iterates to lin_idx+3, each time computing the address.

    Parameters
    ----------
    lin_idx : int
        Initial address from which start computing addresses.
    stage : int
        Current computation stage of the FFT.
    Nexp : int
        FFT length as a power of two (N = 2**Nexp).
    base_addr : int, optional
        Starting address. Defaults to 0.

    Returns
    -------
    addr : array of uint16 (size 4)
        Indexes of four operands from the input array.
    """

    nbits = Nexp
    addr = np.zeros(4, np.uint16)
    for i in range(4):
        # > ... big endian, u2 ... unsigned 2 bytes (see numpy.ndarray doc)
        idx = np.unpackbits(np.array([lin_idx+i], dtype='>u2').view(np.uint8))
        idx = idx[-nbits:]

        lsbs = np.array(idx[-2:])    # two LSBs
        idx = np.array(idx[:-2])     # remove the two LSBs from idx
        a = np.array(idx[:stage*2])  # split idx to parts a (left)
        b = np.array(idx[stage*2:])  #   and b (right)

        idx = np.concatenate((a, lsbs, b)) # insert lsbs between a and b

        idx = np.lib.pad(idx, (16-nbits, 0), 'constant',
                         constant_values = (0, 0))
        addr[i] = (np.packbits(idx)[0] << 8) + np.packbits(idx)[1] + base_addr
    return addr


def stages_needed(Nexp):
        """Number of necessary radix-2 and -4 stages for FFT of size 2**Nexp.
        """

        return { 'rdx4' : np.uint8(Nexp / 2),
                 'rdx2' : np.uint8(np.mod(Nexp, 2)) }


def parity(num):
    parity = -1
    while (num):
        parity = ~parity
        num = num & (num - 1)
    return(parity & 1)


def try_int(num):
    try:
        return int(num)
    except TypeError:
        return -1


def mem_accesses(addr_1, addr_2, parallel_access):
    if not len(addr_1) == len(addr_2):
        raise ValueError("addr_1 and addr_2 must have the same lengths.")

    mem_acc = np.zeros([len(addr_1), 2], int)

    if parallel_access == 1:
        mem_acc[:,0] = addr_1
        mem_acc[:,1] = addr_2
    elif parallel_access == 2:
        mem_acc[0::2] = addr_1.reshape(try_int(len(addr_1)/2), 2)
        mem_acc[1::2] = addr_2.reshape(try_int(len(addr_2)/2), 2)
    else:
        raise ValueError("Invalid parallel_access value.")

    return mem_acc


def mem_func(addr_in, nexp, func):
    if addr_in >= 0:
        if func == MemFunc.HALF:
            return int((addr_in > 2**nexp/2-1))
        elif func == MemFunc.PARITY:
            return parity(addr_in)
        elif func == MemFunc.DIV10MOD2:
            return np.mod(int(addr_in/10), 2)
        elif func == MemFunc.MOD2:
            return np.mod(addr_in, 2)
        elif func == MemFunc.DIV8PAR:
            return parity(int(addr_in/8))
        else:
            raise ValueError("Invalid MemFunc.")
    else:
        return -1


def addr_func(addr_in):
    return int(addr_in / 2)


def print_table(data, cols, data_formats=['{}']):
    print_data = [cols]
    col_widths = []
    for data_row in data:
        row = [df.format(x) for (x, df) in itertools.zip_longest(
                                                  data_row,
                                                  data_formats,
                                                  fillvalue=data_formats[-1])]
        print_data.append(row)
        col_widths.append([])
    print_data = np.array(print_data, str)
    cell_width = np.max([len(s) for s in print_data.flat])
    col_widths = [ np.max([len(s) for s in col]) for col in print_data.T ]
    for print_row in print_data:
        row = [s.rjust(w) for (s, w) in zip(print_row, col_widths)]
        print('  '.join(row))


if __name__ == "__main__":
    print("Read-write delay: {}\n".format(rw_delay))
    print("Parallel access: {}\n".format(parallel_access))
    nexps = np.array(range(nexp_min, nexp_max+1), int)
    nfuncs = len([f for f in MemFunc])  # Columns
    nnexps = len(nexps)                 # Rows
    nconflicts = np.zeros((nnexps, nfuncs), int)
    percentages = np.zeros((nnexps, nfuncs))
    for nexp in nexps:
        print("FFT size: {}".format(2**nexp))
        # Compute all addresses
        nstages  = sum(stages_needed(nexp).values())
        max_iter = 2**nexp * nstages
        addr     = np.zeros(max_iter, int)
        written  = np.ones(2**nexp, bool)
        invalid_reads = 0
        for i in range(0, max_iter, 4):
            stage = i >> nexp
            addr[i:i+4] = ag(i, stage, nexp)
        # Read/write address schedule
        addr_r  = np.concatenate([addr, np.full(rw_delay, -1)])
        addr_w  = np.concatenate([np.full(rw_delay, -1), addr])
        mem_acc = mem_accesses(addr_r, addr_w, parallel_access)
        # Count conflicts
        max_width = 80
        col_num   = 6
        col_width = int(80/col_num)
        print("  Function:", end='', flush=True)
        for func in MemFunc:
            print(" {}".format(func.name), end='', flush=True)
            conflicts = 0
            same_addr = 0
            for i in range(0, mem_acc.shape[0]):
                mem_1 = mem_func(mem_acc[i,0], nexp, func)
                mem_2 = mem_func(mem_acc[i,1], nexp, func)
                mem_addr_1 = addr_func(mem_acc[i,0])
                mem_addr_2 = addr_func(mem_acc[i,1])
                if all([mem_1 >= 0, mem_2 >= 0, (mem_1 == mem_2)]):
                    conflicts += 1
                #  print("{:<3} {:<3} {:<3} {:<3} {:<3}".format(mem_acc[i,0], mem_1, mem_acc[i,1], mem_2, conflicts))
                nconflicts[nexp-nexp_min][func.value] = conflicts
                percentages[nexp-nexp_min][func.value] = \
                                              conflicts / (max_iter + 13)
        # Check for invalid read attempts
        for r, w in zip(addr_r, addr_w):
            if r == w:
                same_addr += 1
            if r >= 0:
                if not written[r]:
                    invalid_reads += 1
                written[r] = False
            if w >= 0:
                written[w] = True
        print('\n  Reads before write: {:<2} Same address reads: {}'
                .format(invalid_reads, same_addr), flush=True)
    # Total number of conflicts
    total = [np.sum(nconflicts[:, f.value]) for f in MemFunc]
    min_idx = np.argmin(total)
    # Average percentages
    perc_avgs = [np.sum(percentages[:, f.value])/len(nexps) for f in MemFunc]
    # Print
    print("\nNumber of conflicts:")
    sizes = [2**nexp for nexp in nexps]
    data = np.insert(nconflicts, 0, sizes, axis=1)
    cols = ["size"] + [f.name for f in MemFunc]
    print_table(data, cols)
    print("\nTotal conflicts:")
    total_row = [['total'] + total]
    print_table(total_row, cols)
    print("\nPercentages:")
    data = np.insert(percentages, 0, sizes, axis=1)
    print_table(data, cols, ['{:.0f}', '{:.2f}'])
    print("\nAverage percentages:")
    avg_row = [['avg'] + perc_avgs]
    print_table(avg_row, cols, ['{}', '{:.2f}'])
    print("\nWinner is {}!".format(MemFunc(min_idx).name))
