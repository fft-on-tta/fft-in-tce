% Generate a signal composed from harmonics and compute FFT from it.
%
% The function generates the signal `y` as an addition of a several
% harmonic signals. The number of the harmonics is controlled by the
% `nharm` argument. One harmonic looks like this:
%   A(k) * ( cos(omega*k*n/fs + phi) + j*cos(omega*k*n/fs + phi) )
%
% where
%   k    ... number of the harmonic (1..nharm)
%   A(k) ... amplitude as a cosine function of k
%
% Additionally, `y` is scaled to fit the fixed-point repr. boundaries (that
% is -1+2^-15..1-2^-15).
%
% The number of periods of the signal is also scaled according to the FFT
% size so that the non-zero FFT results are spread across the whole
% interval instead of having a few non-zeros in the beginning followed by
% only zeros.
%
% FFT output can be optionally scaled according to the hardware CADD unit
% (divide by 4/2 per radix-4/2 stage => divide by the FFT size (N)). CMUL
% also scales the output by 2 per stage.
%
% Inputs:
%   Nexp - FFT size as power of two (FFT size = 2^Nexp)
%   nharm* - How many harmonics to use for composing the signal
%   display (boolean) - Whether to show the plots of the signals, or not
%   scale (boolean) - Whether to scale the output FFT or not.
%
% Outputs:
%   y - Composed harmonic signal
%   Y - FFT of y
%
% * For reasonable results nharm should be less than the FFT size. <= half
%   of the FFT size is better. Also higher values decrease the values of
%   the FFT results, potentially making them too small to notice when
%   considering the fixed point implementation.


function [y, Y] = gen_fft(Nexp, nharm, display, scale)

f = 512;
phi = 0;
omega = 2*pi*f;

N = 2^Nexp;

num_periods = max(1, floor(N/nharm/2));
T = 1/f;
t = num_periods*T;

fs = N / t;

n = linspace(0, N-1, N);

% Compose input signal
y = zeros(1, N);
for k=1:nharm
     A = cos(2*pi*k/nharm);
     y = y + A * ( sin(omega*k*n/fs + phi) + 1i*sin(omega*k*n/fs + phi) );
end

% Scale y to fit 16-bit fixed-point range (-1+2^-15 .. 1-2^-15)
max_re = max(abs(real(y)));
max_im = max(abs(imag(y)));
y = y ./ max(max_re, max_im) * (1-2^-15);
y(1) = 1-2^-15 - 1j;

% Compute FFT
Y = fft(y, N);

if scale
    nstages = round(Nexp/2);
    sc = N * 2 * nstages;
    Y = Y/sc;
end

if display
    figure(1)
    subplot(2,1,1)
    plot(n, real(y), '.');
    grid on
    xlabel('{\itn}(-)  \rightarrow')
    ylabel('{\its}(-)')
    title('\bfGenerated signal y (real)')

    subplot(2,1,2)
    plot(n, imag(y), '.');
    grid on
    xlabel('{\itn}(-)  \rightarrow')
    ylabel('{\its}(-)')
    title('\bfGenerated signal y (imag)')

    figure(2)
    subplot(3,1,1)
    plot(n, abs(Y), '.');
    grid on
    xlabel('{\itn}(-)  \rightarrow')
    ylabel('{\its}(-)')
    title('\bfFourier transform of y (magnitude)')

    subplot(3,1,2)
    plot(n, real(Y), '.');
    grid on
    xlabel('{\itn}(-)  \rightarrow')
    ylabel('{\its}(-)')
    title('\bfFourier transform of y (real)')

    subplot(3,1,3)
    plot(n, imag(Y), '.');
    grid on
    xlabel('{\itn}(-)  \rightarrow')
    ylabel('{\its}(-)')
    title('\bfFourier transform of y (imag)')
end

end
