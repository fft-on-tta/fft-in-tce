# From refftta/utils.py (reversed)

import numpy as np

def bit_pair_reverse(num, nbits):
    """Split an input number into bit pairs and reverse their order.

    Start splitting from MSB. If there is an odd number of bits, the last
    (LSB) "bit pair" is only one bit.

    Parameters
    ----------
    num : int
        Input number.
    nbits : numpy.int8 (max. 16)
        Number of bits to consider when reversing.

    Returns
    -------
    result : int
        Bit-pair reversed input.

    Examples
    --------
    >>> bit_pair_reverse(1,4)
    4  # 00|01 --> 01|00

    >>> bit_pair_reverse(1,5)
    8  # 00|00|1 --> 1|00|00

    >>> bit_pair_reverse(5,7)
    96  # 00|00|10|1 --> 1|10|00|00
    """

    # nbits have to be signed integer; otherwise -nbits will be nonsense
    if nbits is not np.int8:
        nbits = np.int8(nbits)
    # Convert num to bitarray
    # > ... big endian, u2 ... unsigned 2 bytes (see numpy.ndarray doc)
    bits = np.unpackbits(np.array([num], dtype='>u2').view(np.uint8))
    bits = bits[-nbits:]
    # Split to bit pairs
    pairs = [bits[i:i+2] for i in range(0, len(bits), 2)]
    # Reverse pairs
    pairs.reverse()
    # From bits back to numbers
    try:
        pairs = np.concatenate(pairs)
    except:
        pairs = [0]
    pairs = np.lib.pad(pairs, (16-nbits, 0), 'constant',
                       constant_values = (0, 0))
    result = (np.packbits(pairs)[0] << 8) + np.packbits(pairs)[1]
    return int(result)
