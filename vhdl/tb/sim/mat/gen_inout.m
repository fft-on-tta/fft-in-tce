% Generate inputs and output references files for testing the full FFT
% schedule.
%
% Inputs are generated
%   a) from the in_14.csv file (random values, taken from refftta*). Each
%      FFT size (N) is selected as the first N from the file.
%   b) using the `gen_fft` function
%
% * https://gitlab.com/fft-on-tta/refftta
%
% Outputs are computed by the `fft` MATLAB function and then scaled 
% according to the hardware CADD unit (divide by 4/2 per radix-4/2 stage 
% => divide by the FFT size (N)). CMUL also scales the output by 2 per
% stage. Also, the output is unpermuted (the same as from the hardware
% output).
%
% This script writes into following files (inside fft-in-tce/vhdl/tb/sim
% directory; xx denotes the `nexp` value):
%
% mat/in_asm/in_asm_xx:
%   Input data memory in a hex format; Ready to use in .tceasm programs
%
% img/dmem_iodata_init_xx.img:
%   Input data memory in a binary format; Used in HDL simulations
%
% ref/<mem-type>/ref_xx:
%   FFT output in a hex format; Used as a reference when evaluating HDL 
%   simulations
%
% Assuming the script is ran from its folder (fft-in-tce/vhdl/tb/sim/mat)

close all
clear variables
clc

% 'file': load from `infile`
% 'func': use `gen_fft` function
mode = 'file';

infile = 'in_14.csv';

% Load inputs to `inp`
if strcmp(mode, 'file')
    try
        inp = csvread(infile);
        fprintf('Inputs read from `%s`.\n', infile)
        inp = inp(:, 1) + j*inp(:, 2);
    catch
        error('Input file `%s` not found.\n', infile)
    end
elseif strcmp(mode, 'func')
    fprintf('Inputs generated for each Nexp separately.\n')
else
    error('Invalid mode.\n')
end

% Compute FFT and write to files
for nexp = 3:14
    N = 2^nexp;    

    % Compute FFT
    if strcmp(mode, 'func')
        nharm = 16;
        [inp, out] = gen_fft(nexp, nharm, false, false);
    else
        out = fft(inp(1:N), N);
    end

    % Scale the result
    nstages = round(nexp/2);
    sc_factor = N * 2 * nstages;
    out = out / sc_factor;

    % Undo output permutation
    out_noperm = zeros(N, 1);
    for i = 1:N
        idx = double(py.bit.bit_pair_reverse(i-1,nexp));
        out_noperm(i) = out(idx(1)+1);
    end

    % Convert to fixed point
    fp_inp    = fi(inp, true, 16, 15);
    fp_out_re = fi(real(out_noperm), true, 16, 15);
    fp_out_im = fi(imag(out_noperm), true, 16, 15);

    % Write input to `mat/in_asm` and `img/`
    asm_dir = sprintf('in_asm');
    img_dir = sprintf('../img');
    %e = exist(asm_dir, 'dir');
    if ~exist(asm_dir, 'dir')
        mkdir(asm_dir);
    end
    %e = exist(img_dir, 'dir');
    if ~exist(img_dir, 'dir')
        mkdir(img_dir);
    end    
    inp_asm_file = sprintf('%s/in_asm_%02d', asm_dir, nexp);
    inp_img_file = sprintf('%s/dmem_iodata_init_%d.img', img_dir, nexp);
    fprintf('Writing input values to %s and %s\n', inp_asm_file, ...
                                                   inp_img_file);
    fid_asm = fopen(inp_asm_file, 'wt');
    fid_img = fopen(inp_img_file, 'wt');
    for i = 1:N
        fprintf(fid_asm, '4:0x%s%s\n', hex(imag(fp_inp(i))), ...
                                       hex(real(fp_inp(i))));
        fprintf(fid_img, '%s%s\n', bin(imag(fp_inp(i))), ...
                                   bin(real(fp_inp(i))));
    end
    fclose(fid_asm);
    fclose(fid_img);
    
    % Write output to `ref/`
    % -- open
    filename = sprintf('ref_%d', nexp);
    dirs     = [ string('ParMem_Singleport'),
                 string('ParMem_Cacti'),
                 string('ParMem_CactiBlocks')];
    outfiles = strings(length(dirs), 1);
    fids     = zeros(length(dirs), 1);
    for i = 1:length(dirs)
        dir = sprintf('../ref/%s', dirs(i));
        e = exist(dir, 'dir');
        if ~e
            mkdir(dir);
        end
        outfiles(i) = string(sprintf('%s/%s', dir, filename));
        fids(i)     = fopen(outfiles(i), 'wt');
    end
    fprintf('Writing to %s\n', join(outfiles, ', '));
    % -- write
    for i = 1:N
        re = fp_out_re(i);
        im = fp_out_im(i);
        for j = 1:length( fids)
            fprintf(fids(j), '%s%s\n', upper(hex(im)), upper(hex(re)));
        end
    end
    % -- close
    for i = 1:length(fids)
        fclose(fids(i));
    end
end

fprintf('Done\n');
