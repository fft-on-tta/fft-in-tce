package constants is

    -- addressing compensation respecting processor's MAU (i.e. 1 byte)
    -- ex.: MAU = 8 bits => need to scale addr by 4 (dataw/mau) => shl by 2
    constant MAU_ADDR_SHIFT : natural := 2;

    -- following are used in TFG
    constant N_EXP_MAX  : positive := 14;  -- max. supported FFT size
    constant N_MAX      : positive := 2**N_EXP_MAX;
    constant EIGHTH     : positive := N_MAX / 8;
    constant LUT_ADDR_W : natural  := N_EXP_MAX - 3 + 1;  -- max. addr = N/8+1

    -- bus widths
    constant DATA_W     : natural := 32;
    constant HALF_W     : natural := DATA_W / 2;
    constant BUS_W      : natural := 32;
    -- bits needed to express N_EXP_MAX
    constant NEXP_W     : natural :=  4;
    -- max is 7 stages (N_EXP_MAX+1 >> 1)
    constant STAGE_W    : natural := NEXP_W - 1;
    -- does not iterate over 2**N_EXP_MAX; address shift protection
    constant IDX_W      : natural := N_EXP_MAX + MAU_ADDR_SHIFT;

end constants;
