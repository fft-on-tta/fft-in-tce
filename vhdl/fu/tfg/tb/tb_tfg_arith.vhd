--==========================================================================--
-- Testbench for tfg_arith
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- standard: VHDL-2008
--==========================================================================--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
-- reding/writing std_logic(_vector) from/to files:
--use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;

entity tb_tfg_arith is
    -- port ( );
end tb_tfg_arith;

architecture Behavioral of tb_tfg_arith is

    -- uut
    component tfg_arith
        generic (
            dataw     : integer;
            halfw     : integer;
            nexpw     : integer;
            idxw      : integer;
            lut_addrw : integer;
            stagew    : integer);
        port (
            lidx_i    : in  std_logic_vector(dataw-1 downto 0);
            nexp_i    : in  std_logic_vector(nexpw-1 downto 0);
            tf_o      : out std_logic_vector(dataw-1 downto 0);
            rx2_o     : out std_logic);
    end component;
    -- inputs
    signal lidx         : std_logic_vector(31 downto 0) := (others => '0');
    signal nexp         : std_logic_vector(3 downto 0)  := (others => '0');
    -- outputs
    signal tf           : std_logic_vector(31 downto 0) := (others => '0');
    signal rx2          : std_logic := '0';
    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time := 10 ns;
    -- constants
    constant C_NEXP    : positive := 10;
    constant C_LATENCY : natural  := 0;

begin

    uut: tfg_arith
        generic map (
            dataw     => DATA_W,
            halfw     => HALF_W,
            nexpw     => NEXP_W,
            idxw      => IDX_W,
            lut_addrw => LUT_ADDR_W,
            stagew    => STAGE_W)
        port map (
            lidx_i    => lidx,
            nexp_i    => nexp,
            tf_o      => tf,
            rx2_o     => rx2);

    -- clock process
    clk_proc: process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;

    -- stimulus process
    stim_proc : process
        file f_tf       : text;
        file f_rx2       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable tffile  : string(1 to 6) := "tf.txt";
        variable rx2file : string(1 to 7) := "rx2.txt";
        variable outline : line;
        variable cnt     : integer := 0;
        variable offset  : integer := C_LATENCY;
        variable repeats : integer := ((C_NEXP+1)/2)*(2**C_NEXP) + C_LATENCY;
    begin

        wait for clk_period*1.5; -- synchronize to rising edge
        nexp <= std_logic_vector(to_unsigned(C_NEXP, nexp'length));
        wait for clk_period*2;

        -- open files
        report "Opening " & tffile & " ...";
        file_open(fstatus, f_tf, tffile, write_mode);
        assert fstatus = OPEN_OK report "File " & tffile & " does not exist."
                                          severity error;
        report "Opening " & rx2file & " ...";
        file_open(fstatus, f_rx2, rx2file, write_mode);
        assert fstatus = OPEN_OK report "File " & rx2file & " does not exist."
                                          severity error;

        -- sim loop
        report "Simulation loop. " & integer'image(repeats) & " rounds.";
        for cnt in 0 to repeats-1 loop
            -- load new values
            lidx <= std_logic_vector(to_unsigned(cnt, lidx'length));
            -- wait 1 clock cycle
            wait for clk_period;
            -- write values to file
             hwrite(outline, tf, right, 8);
            writeline(f_tf, outline);
             write(outline, rx2, left, 1);
            writeline(f_rx2, outline);
        end loop;

        report "Closing " & tffile & " ...";
        file_close(f_tf);
        report "Closing " & rx2file & " ...";
        file_close(f_rx2);

        wait for clk_period;

        report "Simulation END";
        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;


end Behavioral;
