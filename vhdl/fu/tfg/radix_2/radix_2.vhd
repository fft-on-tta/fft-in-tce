-- Raises an rx2 flag if the curernt stage is radix-2

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity radix_2 is
    generic (
        stagew  : integer;
        nexpw   : integer);
    port (
        stage_i : in  std_logic_vector(stagew-1 downto 0);
        nexp_i  : in  std_logic_vector(nexpw-1 downto 0);
        rx2_o   : out std_logic);
end radix_2;

architecture Behavioral of radix_2 is

    signal stage : unsigned(stage_i'range);
    signal nexp  : unsigned(nexp_i'range);
    signal rx2   : std_logic;

begin -- Behavioral

    stage <= unsigned(stage_i);
    nexp  <= unsigned(nexp_i);

    process(stage, nexp)
        variable last_stage : unsigned(nexpw-1 downto 0);
        variable rx2_ever   : std_logic;
    begin
        rx2_ever := std_logic(nexp(0));
        if rx2_ever = '1' then
            last_stage := shift_right(nexp-1, 1);
            if stage = last_stage then
                rx2 <= '1';
            else
                rx2 <= '0';
            end if;
        else
            rx2 <= '0';
        end if;
    end process;

    rx2_o <= rx2;

end Behavioral;
