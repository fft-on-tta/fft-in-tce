% Generate input and reference files for tfg testbench

close all
clc

% Load variables  (from get_k.m & gen_lut.m, saved from workspace)
load('k_all.mat')
load('tf_lut.mat')

e = exist('fp_res', 'var');
if e == 0
    eighth = 2^14/8;
    fp_res = fi(zeros(length(k_all),12), true, 16, 15);
    for nexp = 3:14
        nstages = int32(nexp/2);
        nsamples = 2^nexp * nstages;
        fprintf('Nexp: %d, %d samples\n', nexp, nsamples)   

        k_tf = k_all(1:nsamples, nexp-2);
        for n = 1:length(k_tf)
            k = k_tf(n);
            if k <= eighth
                fp_res(n, nexp-2) = fp_tf(k+1);
            elseif (k > 1*eighth) && (k <= 2*eighth)
                fp_res(n, nexp-2) = -1j * conj(fp_tf(2*eighth-k+1));
            elseif (k > 2*eighth) && (k <= 3*eighth)
                fp_res(n, nexp-2) = -1j * fp_tf(k-2*eighth+1);
            elseif (k > 3*eighth) && (k <= 4*eighth)
                fp_res(n, nexp-2) = -conj(fp_tf(4*eighth-k+1));
            elseif (k > 4*eighth) && (k <= 5*eighth)
                fp_res(n, nexp-2) = -fp_tf(k-4*eighth+1);
            elseif (k > 5*eighth) && (k <= 6*eighth)
                fp_res(n, nexp-2) = 1j * conj(fp_tf(6*eighth-k+1));
            else
                error('k in wrong range')
            end
        end    
    end
end

% Generate reference tf files
for nexp = 3:14
    nstages = int32(nexp/2);
    nsamples = 2^nexp * nstages;
    fname = sprintf('../ref/tf/ref_tf_%02d', nexp);
    fprintf('Writing to %s\n', fname)
    
    fid = fopen(fname, 'wt');
    for n = 1:nsamples
        re = real(fp_res(n, nexp-2));
        im = imag(fp_res(n, nexp-2));
        fprintf(fid, '%s%s\n', upper(hex(im)), upper(hex(re)));
        % Format for comparison with Python
        %fprintf(fid, '0x%s%s\n', hex(im), hex(re));
    end
    fclose(fid);
end

% Generate reference rx2 files
for nexp = 3:14
    nstages = int32(nexp/2);
    nsamples = 2^nexp * nstages;    
    fname = sprintf('../ref/rx2/ref_rx2_%02d', nexp);
    fprintf('Writing to %s\n', fname)
    
    if mod(nexp, 2) == 1
        rx2 = [ zeros(2^nexp*(nstages-1), 1) ; ones(2^nexp, 1) ];
    else
        rx2 = [ zeros(2^nexp*(nstages), 1) ];
    end
    
    fid = fopen(fname, 'wt');
    for n = 1:nsamples
        fprintf(fid, '%d\n', rx2(n));
    end
    fclose(fid);
end






