% Get values of k (scaled) from Python reference
% (see https://gitlab.com/fft-on-tta/share/tree/master/py_ref/tfg_k)
%
% Needs to be executed in a folder with "ref_tfg_k_xx" files.

close all
clear all
clc

k = zeros(2^14*7, 12);
for nexp = 3:14
    nstages = int32(nexp/2);
    nsamples = 2^nexp * nstages;
    fname = sprintf('ref_tfg_k_%02d', nexp);
    fprintf('%s: %d stages, %d samples\n', fname, nstages, nsamples);
    fid = fopen(fname, 'r');
    k(1:nsamples, nexp-2) = fscanf(fid, ['0x' '%x' '\n']);    
    fclose(fid);
end