#!/bin/sh

####
# Variables
####

SCRIPTDIR=$(dirname "$0")
cd $SCRIPTDIR
WORKDIR="../work"

GHDLFLAGS="--workdir=$WORKDIR --std=08"
SIMFLAGS="--assert-level=none"

FILES="../../shared/constants.vhd"
FILES+=" ../../shared/idx_stage_split.vhd"
FILES+=" ../radix_2/radix_2.vhd"
FILES+=" ../weight/weight.vhd"
FILES+=" ../scale_k/scale_k.vhd"
FILES+=" ../lut_addr/lut_addr.vhd"
FILES+=" ../tf_lut/tf_lut.vhd"
FILES+=" ../tf_lut/distr_2049x32.vhd"
FILES+=" ../twiddle_twist/twiddle_twist.vhd"
FILES+=" ../tfg.vhd"
FILES+=" ../tb/tb_tfg_arith.vhd"

ENTITY="tb_tfg_arith"
FOUT_TF="tf.txt"
FOUT_RX2="rx2.txt"
LOGFILE="log.txt"

####
# Functions
####

clean()
{
    echo "Cleaning ..."
    rm -rf $WORKDIR
    rm -rf $ENTITY
    rm -rf e~$ENTITY.o
    rm -rf $FOUT_TF
    rm -rf $FOUT_RX2
    rm -rf $LOGFILE
}

compile()
{
    mkdir -p $WORKDIR
    echo "Compiling..."
    if [ $# -eq 0 ]; then
        ghdl -s $GHDLFLAGS $FILES
        ghdl -a $GHDLFLAGS $FILES
        ghdl -e $GHDLFLAGS $ENTITY
    fi
}

simulate()
{
    echo "Simulating ..."
    if [ -e $ENTITY ]; then
        ./$ENTITY &> $LOGFILE
    else
        # Some GHDL versions do not produce binary.
        ghdl -r $GHDLFLAGS $ENTITY $SIMFLAGS &> $LOGFILE
    fi
}

####
# Main script
####

if [ "$1" = "sim" ]; then
    simulate
elif [ "$1" = "clean" ]; then
    clean
elif [ $# -eq 0 ]; then
    clean
    compile
else
    echo "Nothing ..."
fi
