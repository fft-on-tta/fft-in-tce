--===========================================================================--
-- Twiddle factor generator (pipelined, 4 stages)
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- Structural description of a complete twiddle factor generator.
-- Contains 7 components:
--     u0 : Split a linear index into a separate stage and index
--     u1 : Radix-2 detector
--     u2 : Weight generator
--     u3 : Computation of tw. factor's index k and scaling it
--     u4 : Computation of tw. factor's address in the lookup table (LUT)
--     u5 : LUT with 2049 stored twiddle factors
--     u6 : Manipulate tw. factor retrieved from LUT to get the result
--
-- In case of pipelining of one of the components, care must be taken to ensure
-- that other signals get delayed as well (i.e. in case of synchronous u5, opc
-- signal needs to be delayed 1 clock cycle).

-- Dataflow graph:
--     - | / \ ... signal paths
--     +       ... signal junction
--     (u*)    ... component
--     =       ... component's output
--     -***--  ... name of a signal (simplified)
--     >-- --> ... input/output to/from tfg
--     *D      ... delay of a FU or signal (component) below this mark
--                                   1D
--                 +--idx-------+-------+
--                /             |    1D  \
--               /  +-----------|-------+ \
--              /  /            |    1D  \ \   1D          2D              tf_o
--             /  +--stage-----(u2)=-w---(u3)=--k-(u4)=--opc---------(u6)=---->
-- lidx_i     =  /              |    1D   /        =  1D   1D        /
--  >-------(u0)=----(u1)=--rx2-|--------+        addr--->(u5)=--tf-+
-- nexp_i   /        /          |         \                3D             rx2_o
--  >------+--------+-----------+          +---------------------------------->
--
--===========================================================================--


--===========================================================================--
-- Entity of TFG
-------------------------------------------------------------------------------
-- Generics
--     dataw     : Width of the data word
--     halfw     : Half of the width of the data word
--       nexpw     : Width of the nexp signal
--       idxw      : Width if the indesx signal
--       lut_addrw : Width of the LUT address signal
--       stagew    : Width of the stage signal

-- Inputs
--     lidx_i    : Linear index (from a linear counter)
--     nexp_i    : Current FFT size (as a power of two)
--     clk       : Clock
--     rstx      : Asynchronous reset
--     glock     : Lock signal
--
-- Outputs
--     tf_o      : Twiddle factor
--     rx2_o     : Info about current radix (1: radix-2, 0: radix-4)
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tfg_pipe_4 is
    generic (
        dataw     : natural;
        halfw     : natural;
        nexpw     : natural;
        idxw      : natural;
        lut_addrw : natural;
        stagew    : natural);
    port (
        lidx_i    : in  std_logic_vector(dataw-1 downto 0);
        nexp_i    : in  std_logic_vector(nexpw-1 downto 0);
        tf_o      : out std_logic_vector(dataw-1 downto 0);
        rx2_o     : out std_logic;
        clk       : in std_logic;
        rstx      : in std_logic;
        glock     : in std_logic);
end tfg_pipe_4;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of TFG
-------------------------------------------------------------------------------
architecture Structural of tfg_pipe_4 is

    -- Components
    component idx_stage_split
        generic (
            dataw   : natural;
            nexpw   : natural;
            stagew  : natural;
            idxw    : natural);
        port (
            lidx_i  : in  std_logic_vector(dataw -1 downto 0);
            nexp_i  : in  std_logic_vector(nexpw -1 downto 0);
            idx_o   : out std_logic_vector(idxw  -1 downto 0);
            stage_o : out std_logic_vector(stagew-1 downto 0));
    end component;

    component radix_2
        generic (
            stagew  : natural;
            nexpw   : natural);
        port (
            stage_i : in  std_logic_vector(stagew-1 downto 0);
            nexp_i  : in  std_logic_vector(nexpw -1 downto 0);
            rx2_o   : out std_logic);
    end component;

    component weight
        generic (
            idxw      : natural;
            stagew    : natural;
            nexpw     : natural;
            lut_addrw : natural);
        port (
            idx_i     : in  std_logic_vector(idxw  -1 downto 0);
            stage_i   : in  std_logic_vector(stagew-1 downto 0);
            nexp_i    : in  std_logic_vector(nexpw -1 downto 0);
            w_o       : out std_logic_vector(idxw  -1 downto 0));
    end component;

    component scale_k
        generic (
            idxw      : natural;
            stagew    : natural);
        port (
            idx_i     : in  std_logic_vector(idxw  -1 downto 0);
            w_i       : in  std_logic_vector(idxw  -1 downto 0);
            stage_i   : in  std_logic_vector(stagew-1 downto 0);
            rx2_i     : in  std_logic;
            k_o       : out std_logic_vector(idxw  -1 downto 0));
    end component;

    component lut_addr
        generic (
            idxw       : natural;
            lut_addrw  : natural);
        port (
            k_i        : in  std_logic_vector(idxw-1 downto 0);
            lut_addr_o : out std_logic_vector(lut_addrw-1 downto 0);
            opc_o      : out std_logic_vector(2 downto 0));
    end component;

    component tf_lut_synch
        generic (
            dataw     : natural;
            lut_addrw : natural);
        port (
            addr_i    : in  std_logic_vector(lut_addrw-1 downto 0);
            tf_o      : out std_logic_vector(dataw    -1 downto 0);
            clk       : in std_logic;
            rstx      : in std_logic;
            glock     : in std_logic);
    end component;

    component twiddle_twist
        generic (
            dataw    : natural;
            halfw    : natural);
        port (
            tf_lut_i : in  std_logic_vector(dataw-1 downto 0);
            opc_i    : in  std_logic_vector(2 downto 0);
            tf_o     : out std_logic_vector(dataw-1 downto 0));
    end component;

    -- Connecting signals
    signal s_idx        : std_logic_vector(idxw     -1 downto 0);
    signal s_stage      : std_logic_vector(stagew   -1 downto 0);
    signal s_w          : std_logic_vector(idxw     -1 downto 0);
    signal s_k          : std_logic_vector(idxw     -1 downto 0);
    signal s_rx2        : std_logic;
    signal s_lut_addr   : std_logic_vector(lut_addrw-1 downto 0);
    -- opcode for the final tf manipulation
    -- opc(0)  - Negate real?
    -- opc(1)  - Negate imag?
    -- opc(2)  - Switch real <-> imag?
    signal s_opc        : std_logic_vector(2 downto 0);
    signal s_tf_lut     : std_logic_vector(dataw-1 downto 0);
    signal s_tf         : std_logic_vector(dataw-1 downto 0);
    -- Registers
    constant N_RX2REG   : natural := 4;
    constant N_OPCREG   : natural := 2;
    type t_opc_reg is array (0 to N_OPCREG-1) of std_logic_vector(s_opc'range);
    -- signal opc_reg    : std_logic_vector(s_opc'range);
    signal opc_reg      : t_opc_reg;
    signal k_reg        : std_logic_vector(s_k'range);
    signal w_reg        : std_logic_vector(s_w'range);
    signal idx_reg      : std_logic_vector(s_idx'range);
    signal stage_reg    : std_logic_vector(s_stage'range);
    signal lut_addr_reg : std_logic_vector(s_lut_addr'range);
    signal rx2_reg      : std_logic_vector(N_RX2REG-1 downto 0);

begin -- Structural

    -- Extract the stage and current index (idx) within the stage from lidx_i
    u0 : idx_stage_split
        generic map (
            dataw   => dataw,
            nexpw   => nexpw,
            stagew  => stagew,
            idxw    => idxw)
        port map (
            lidx_i  => lidx_i,
            nexp_i  => nexp_i,
            idx_o   => s_idx,
            stage_o => s_stage);

    -- Is current stage radix-2?
    u1 : radix_2
        generic map (
            stagew  => stagew,
            nexpw   => nexpw)
        port map (
            stage_i => s_stage,
            nexp_i  => nexp_i,
            rx2_o   => s_rx2);

    -- Compute the weight of k
    u2 : weight
        generic map (
            idxw      => idxw,
            stagew    => stagew,
            nexpw     => nexpw,
            lut_addrw => lut_addrw)
        port map (
            idx_i   => s_idx,
            stage_i => s_stage,
            nexp_i  => nexp_i,
            w_o     => s_w);

    -- Compute k and scale it
    u3 : scale_k
        generic map (
            idxw    => idxw,
            stagew  => stagew)
        port map (
            idx_i   => idx_reg,
            w_i     => w_reg,
            stage_i => stage_reg,
            rx2_i   => rx2_reg(0),
            k_o     => s_k);

    -- Compute LUT address from a scaled k
    u4 : lut_addr
        generic map (
            idxw       => idxw,
            lut_addrw  => lut_addrw)
        port map (
            k_i        => k_reg,
            lut_addr_o => s_lut_addr,
            opc_o      => s_opc);

    -- LUT with twiddle factors
    u5 : tf_lut_synch
        generic map (
            dataw     => dataw,
            lut_addrw => lut_addrw)
        port map (
            addr_i    => lut_addr_reg,
            tf_o      => s_tf_lut,
            clk       => clk,
            rstx      => rstx,
            glock     => glock);

    -- Manipulate the twiddle factor obtained from the LUT
    u6 : twiddle_twist
        generic map (
            dataw    => dataw,
            halfw    => halfw)
        port map (
            tf_lut_i => s_tf_lut,
            opc_i    => opc_reg(opc_reg'high),
            tf_o     => s_tf);

    reg : process(clk, rstx)
        variable i : natural;
    begin --reg
        if rstx = '0' then
            idx_reg <= (others => '0');
            stage_reg <= (others => '0');
            w_reg <= (others => '0');
            k_reg <= (others => '0');
            lut_addr_reg <= (others => '0');
            -- opc_reg <= (others => '0');
            for i in 0 to N_OPCREG-1 loop
                opc_reg(i) <= (others => '0');
            end loop;
            rx2_reg <= (others => '0');
        end if;
        if rising_edge(clk) then
            if glock = '0' then
                -- idx reg (1)
                idx_reg <= s_idx;
                -- stage reg (1)
                stage_reg <= s_stage;
                -- w reg (1)
                w_reg <= s_w;
                -- rx2 reg (4)
                rx2_reg(0) <= s_rx2;
                for i in 1 to N_RX2REG-1 loop
                    rx2_reg(i) <= rx2_reg(i-1);
                end loop;
                -- k reg (1)
                k_reg <= s_k;
                -- lut address reg
                lut_addr_reg <= s_lut_addr;
                -- opcode reg (2)
                opc_reg(0) <= s_opc;
                for i in 1 to N_OPCREG-1 loop
                    opc_reg(i) <= opc_reg(i-1);
                end loop;
            end if;
        end if;
    end process reg;

    tf_o  <= s_tf;
    rx2_o <= rx2_reg(rx2_reg'high);

end Structural;
--===========================================================================--
