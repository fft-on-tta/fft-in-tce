-- From scaled k compute address for LUT read

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity lut_addr is
    generic (
        idxw       : natural;
        lut_addrw  : natural);
    port (
        k_i        : in  std_logic_vector(idxw-1 downto 0);
        lut_addr_o : out std_logic_vector(lut_addrw-1 downto 0);
        opc_o      : out std_logic_vector(2 downto 0));
end lut_addr;

architecture Behavioral of lut_addr is

    signal k        : unsigned(k_i'range);
    signal lut_addr : unsigned(lut_addr_o'range);
    signal opc      : std_logic_vector(opc_o'range);

begin -- Behavioral

    k <= unsigned(k_i);

    address : process(k)
        variable k_int    : natural;
        variable addr_int : natural;
    begin -- address
        k_int := to_integer(k);
        if (k_int <= EIGHTH) then
            addr_int := k_int;
            opc <= "000";
        elsif (k_int > EIGHTH) and (k_int <= 2*EIGHTH) then
            addr_int := 2*EIGHTH-k_int;
            opc <= "111";
        elsif (k_int > 2*EIGHTH) and (k_int <= 3*EIGHTH) then
            addr_int := k_int-2*EIGHTH;
            opc <= "101";
        elsif (k_int > 3*EIGHTH) and (k_int <= 4*EIGHTH) then
            addr_int := 4*EIGHTH-k_int;
            opc <= "001";
        elsif (k_int > 4*EIGHTH) and (k_int <= 5*EIGHTH) then
            addr_int := k_int-4*EIGHTH;
            opc <= "011";
        elsif (k_int > 5*EIGHTH) and (k_int <= 6*EIGHTH) then
            addr_int := 6*EIGHTH-k_int;
            opc <= "100";
        else
            addr_int := 0;
            opc <= "000";
        end if;
        lut_addr <= to_unsigned(addr_int, lut_addr'length);
    end process address;

    lut_addr_o <= std_logic_vector(lut_addr);
    opc_o      <= std_logic_vector(opc);

end Behavioral;
