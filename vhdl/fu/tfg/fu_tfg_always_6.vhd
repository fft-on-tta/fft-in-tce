--===========================================================================--
-- Twiddle factor generator (TFG) wrapper for TCE (latency 6)
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- It is a top level entity for tfg_arith providing a standard TCE-compatible
-- interface
--===========================================================================--


--===========================================================================--
-- Entity of TFG, latency 6
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity fu_tfg_always_6 is
    generic (
        dataw  : positive := DATA_W;
        busw   : positive := BUS_W;
        nexpw  : positive := NEXP_W);
    port (
        t1data : in  std_logic_vector(dataw-1 downto 0);
        t1load : in  std_logic;
        o1data : in  std_logic_vector(nexpw-1 downto 0);
        o1load : in  std_logic;
        r1data : out std_logic_vector(busw -1 downto 0);
        r2data : out std_logic_vector(0 downto 0);  -- can't be std_logic
        glock  : in  std_logic;
        rstx   : in  std_logic;
        clk    : in  std_logic);
end fu_tfg_always_6;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of TFG, latency 6
-------------------------------------------------------------------------------
architecture rtl of fu_tfg_always_6 is

    component tfg_pipe_4
        generic (
            dataw     : positive;
            halfw     : positive;
            nexpw     : positive;
            idxw      : positive;
            lut_addrw : positive;
            stagew    : positive);
        port (
            lidx_i    : in  std_logic_vector(dataw-1 downto 0);
            nexp_i    : in  std_logic_vector(nexpw-1 downto 0);
            tf_o      : out std_logic_vector(dataw-1 downto 0);
            rx2_o     : out std_logic;
            clk       : in std_logic;
            rstx      : in std_logic;
            glock     : in std_logic);
    end component;

    signal t1reg   : std_logic_vector(dataw-1 downto 0);
    signal o1reg   : std_logic_vector(nexpw-1 downto 0);
    -- o1tmp is used for o1reg to be loaded only on trigger
    signal o1tmp   : std_logic_vector(nexpw-1 downto 0);
    signal control : std_logic_vector(1 downto 0);
    signal r1      : std_logic_vector(dataw-1 downto 0);
    signal r1reg   : std_logic_vector(dataw-1 downto 0);
    signal r2      : std_logic;
    signal r2reg   : std_logic;
    -- Latency of this FU. Must be >=3.
    constant LATENCY : positive := 6;
    signal result_en_reg : std_logic_vector(LATENCY-2 downto 0);

begin -- rtl

    fu_arch : tfg_pipe_4
        generic map (
            dataw     => dataw,
            halfw     => HALF_W,
            nexpw     => nexpw,
            idxw      => IDX_W,
            lut_addrw => LUT_ADDR_W,
            stagew    => STAGE_W)
        port map (
            lidx_i    => t1reg,
            nexp_i    => o1reg,
            tf_o      => r1,
            rx2_o     => r2,
            clk       => clk,
            rstx      => rstx,
            glock     => glock);

    control <= o1load & t1load;

    regs : process (clk, rstx)
        variable i : positive;
    begin
        if rstx = '0' then
            t1reg <= (others => '0');
            o1reg <= (others => '0');
            o1tmp <= (others => '0');
            r1reg <= (others => '0');
            r2reg <= '0';
            result_en_reg <= (others => '0');
        elsif rising_edge(clk) then
            if glock = '0' then
                case control is
                    when "01" =>
                        o1reg <= o1tmp;
                        t1reg <= t1data;
                    when "10" =>
                        o1tmp <= o1data;
                    when "11" =>
                        o1reg <= o1data;
                        o1tmp <= o1data;
                        t1reg <= t1data;
                    when others => null;
                end case;

                result_en_reg(0) <= t1load;
                for i in 1 to result_en_reg'high loop
                    result_en_reg(i) <= result_en_reg(i-1);
                end loop;
                if result_en_reg(result_en_reg'high) = '1' then
                    r1reg <= r1;
                    r2reg <= r2;
                end if;
            end if;
        end if;
    end process regs;

    r1data <= std_logic_vector(resize(signed(r1reg), busw));
    r2data(0) <= r2reg;

end rtl;
--===========================================================================--
