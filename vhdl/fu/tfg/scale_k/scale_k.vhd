-- 1. Generate k (modulo 4 or 2 - depending on current radix)
-- 2. Scale it by weight
-- 3. Scale it according to radix-2 and current stage

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity scale_k is
    generic (
        idxw      : integer;
        stagew    : integer);
    port (
        idx_i     : in  std_logic_vector(idxw  -1 downto 0);
        w_i       : in  std_logic_vector(idxw  -1 downto 0);
        stage_i   : in  std_logic_vector(stagew-1 downto 0);
        rx2_i     : in  std_logic;
        k_o       : out std_logic_vector(idxw  -1 downto 0));
end scale_k;

architecture Behavioral of scale_k is

    signal idx    : unsigned(idx_i'range);
    signal w      : unsigned(w_i'range);
    signal stage  : unsigned(stage_i'range);

    signal k_base : unsigned(1 downto 0);
    signal k_unsc : unsigned(w_i'range);
    signal k      : unsigned(w_i'range);

begin -- Behavioral

    idx   <= unsigned(idx_i);
    w     <= unsigned(w_i);
    stage <= unsigned(stage_i);

    -- Generate base for k
    base_k : process (idx, rx2_i)
    begin -- base_k
        if rx2_i = '1' then
            k_base <= '0' & idx(0);    -- modulo 2
        else
            k_base <= idx(1 downto 0); -- modulo 4
        end if;
    end process base_k;

    -- Weight it
    weight_k : process (w, k_base)
    begin -- weight_k
        case k_base is
            when "01"   => k_unsc <= w;                    -- 1*w
            when "10"   => k_unsc <= shift_left(w, 1);     -- 2*w
            when "11"   => k_unsc <= shift_left(w, 1) + w; -- 3*w
            when others => k_unsc <= (others => '0');      -- 0*w
        end case;
    end process weight_k;

    -- Scale it
    scale_k : process (stage, rx2_i, k_unsc)
        variable stage_sc : unsigned(stage'high+1 downto 0);
        variable shift    : integer;
    begin -- scale_k
        stage_sc := resize(stage, stage_sc'length);
        shift    := N_EXP_MAX - to_integer(shift_left(stage_sc, 1)) - 2;
        if rx2_i = '1' then
            k <= shift_left(k_unsc, shift+1);
        else
            k <= shift_left(k_unsc, shift);
        end if;
    end process scale_k;

    k_o <= std_logic_vector(k);

end Behavioral;
