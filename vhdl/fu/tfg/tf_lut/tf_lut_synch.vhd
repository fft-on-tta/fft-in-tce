library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tf_lut_synch is
    generic (
        dataw     : natural;
        lut_addrw : natural);
    port (
        addr_i    : in  std_logic_vector(lut_addrw-1 downto 0);
        tf_o      : out std_logic_vector(dataw    -1 downto 0);
        clk       : in std_logic;
        rstx      : in std_logic;
        glock     : in std_logic);
end tf_lut_synch;

architecture Structural of tf_lut_synch is

    component distr_2049x32_synch
        generic (
            dataw     : natural;
            lut_addrw : natural);
        port (
            addr_i    : in  std_logic_vector(lut_addrw-1 downto 0);
            tf_o      : out std_logic_vector(dataw    -1 downto 0);
            clk       : in std_logic;
            rstx      : in std_logic;
            glock     : in std_logic);
    end component;

begin -- Structural

    lut : distr_2049x32_synch
        generic map (
            dataw     => dataw,
            lut_addrw => lut_addrw)
        port map (
            addr_i    => addr_i,
            tf_o      => tf_o,
            clk       => clk,
            rstx      => rstx,
            glock     => glock);

end Structural;
