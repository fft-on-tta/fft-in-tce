-- Modify a twiddle factor obtained from (N/8+1) LUT according to opcode

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity twiddle_twist is
    generic (
        dataw    : natural;
        halfw    : natural);
    port (
        tf_lut_i : in  std_logic_vector(dataw-1 downto 0);
        opc_i    : in  std_logic_vector(2 downto 0);
        tf_o     : out std_logic_vector(dataw-1 downto 0));
end twiddle_twist;

architecture Behavioral of twiddle_twist is

    signal tf_lut : std_logic_vector(dataw-1 downto 0);
    signal opc    : std_logic_vector(2 downto 0);
    signal tf     : std_logic_vector(dataw-1 downto 0);

begin -- Behavioral

    tf_lut <= tf_lut_i;
    opc    <= opc_i;

    twiddle_twist : process(tf_lut, opc)
        variable tf_lut_re : signed(halfw-1 downto 0);
        variable tf_lut_im : signed(halfw-1 downto 0);
        variable tf_re     : signed(halfw-1 downto 0);
        variable tf_im     : signed(halfw-1 downto 0);
    begin
        -- negate real part
        if opc(0) = '1' then
            tf_lut_re := - signed(tf_lut(halfw-1 downto 0));
        else
            tf_lut_re :=   signed(tf_lut(halfw-1 downto 0));
        end if;
        -- negate imag part
        if opc(1) = '1' then
            tf_lut_im := - signed(tf_lut(dataw-1 downto halfw));
        else
            tf_lut_im :=   signed(tf_lut(dataw-1 downto halfw));
        end if;
        -- switch real <-> imag
        if opc(2) = '1' then
            tf_re := tf_lut_im;
            tf_im := tf_lut_re;
        else
            tf_re := tf_lut_re;
            tf_im := tf_lut_im;
        end if;

        tf <= std_logic_vector(tf_im) & std_logic_vector(tf_re);
    end process twiddle_twist;

    tf_o <= tf;

end Behavioral;
