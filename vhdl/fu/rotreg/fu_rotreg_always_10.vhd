--===========================================================================--
-- Rotating register as wrapper for TCE (latency 10)
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- It is a top level entity providing a standard TCE-compatible interface.
--===========================================================================--

--===========================================================================--
-- Entity of rotreg, latency 10
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity fu_rotreg_always_10 is
    generic (
        dataw  : positive := DATA_W;
        busw   : positive := BUS_W);
    port (
        t1data : in  std_logic_vector(dataw-1 downto 0);
        t1load : in  std_logic;
        r1data : out std_logic_vector(busw-1 downto 0);
        glock  : in  std_logic;
        rstx   : in  std_logic;
        clk    : in  std_logic);
end fu_rotreg_always_10;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of rotreg, latency 10
-------------------------------------------------------------------------------
architecture rtl of fu_rotreg_always_10 is

    component rotreg
        generic (
            dataw     : positive;
            reg_count : positive);
        port (
            d_i       : in  std_logic_vector(dataw-1 downto 0);
            load_i    : in  std_logic;
            q_o       : out std_logic_vector(dataw-1 downto 0);
            clk       : in  std_logic;
            glock     : in  std_logic;
            rstx      : in  std_logic);
    end component;

    signal t1reg         : std_logic_vector(dataw-1 downto 0);
    signal control       : std_logic;
    signal r1            : std_logic_vector(dataw-1 downto 0);
    signal r1reg         : std_logic_vector(dataw-1 downto 0);
    -- Latency of this FU. Must be >=3.
    constant LATENCY     : positive := 10;
    -- Register count in rotreg.vhd:
    -- 2 registers are consumed by this FU (t1reg, r1reg)
    constant REG_CNT     : positive := LATENCY-1;
    signal result_en_reg : std_logic_vector(LATENCY-2 downto 0);

begin -- rtl

    rotreg_inst : rotreg
        generic map (
            dataw     => dataw,
            reg_count => REG_CNT)
        port map (
            d_i    => t1data,
            load_i => t1load,
            q_o    => r1,
            clk    => clk,
            glock  => glock,
            rstx   => rstx);

    -- control <= t1load;

    regs : process (clk, rstx)
    begin
        if rstx = '0' then
            t1reg <= (others => '0');
            r1reg <= (others => '0');
            result_en_reg <= (others => '0');
        elsif rising_edge(clk) then
            if glock = '0' then
                -- case control is
                --     when '1' =>
                --         t1reg <= t1data;
                --     when others => null;
                -- end case;
                -- update result only when new operation was triggered
                -- This should save power when clock gating is enabled
                result_en_reg(0) <= t1load;
                for i in 1 to result_en_reg'high loop
                    result_en_reg(i) <= result_en_reg(i-1);
                end loop;
                if result_en_reg(result_en_reg'high) = '1' then
                    r1reg <= r1;
                end if;
            end if;
        end if;
    end process regs;

    r1data <= std_logic_vector(resize(unsigned(r1reg), busw));

end rtl;
--===========================================================================--
