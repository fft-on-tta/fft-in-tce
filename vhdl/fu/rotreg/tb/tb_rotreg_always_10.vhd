--===========================================================================--
-- Testbench for rotating register latency 10 as a functional unit for TCE
-- (fu_rotreg_always_10).
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- standard : VHDL-2008
--===========================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
-- reading/writing std_logic(_vector) from/to files:
-- use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;
use work.tb_utils.all;

entity tb_rotreg_always_10 is
    -- empty;
end tb_rotreg_always_10;

architecture Testbench of tb_rotreg_always_10 is

    -- uut component
    component fu_rotreg_always_10
        generic (
            dataw  : positive;
            busw   : positive);
        port (
            t1data : in  std_logic_vector(dataw-1 downto 0);
            t1load : in  std_logic;
            r1data : out std_logic_vector(busw-1 downto 0);
            glock  : in  std_logic;
            rstx   : in  std_logic;
            clk    : in  std_logic);
    end component;

    signal d      : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal q      : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal t1load : std_logic := '0';
    signal glock  : std_logic := '0';
    signal rstx   : std_logic := '0';

    constant ITERS : positive := 20;

    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time      := 10 ns;

begin -- Testbench

    -- uut instance
    fu_rotreg_always_10_inst : fu_rotreg_always_10
        generic map (
            dataw => DATA_W,
            busw  => BUS_W)
        port map (
            t1data => d,
            t1load => t1load,
            r1data => q,
            glock  => glock,
            rstx   => rstx,
            clk    => clk);

    -- clock signal
    clk <= not clk after clk_period/2;

    -- stimulation process
    stim_proc : process
        file f_out       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable outfile : string(1 to 8) := "dump.txt";
        variable outline : line;
        variable cnt     : integer := 0;
        variable repeats : integer := ITERS;
    begin

        -- init
        wait_until_rising_edges(clk, 2);
        rstx <= '1';
        wait until rising_edge(clk);
        glock <= '0';
        wait until rising_edge(clk);

        -- open file
        report "Opening " & outfile & " ...";
        file_open(fstatus, f_out, outfile, write_mode);
        assert fstatus = OPEN_OK report "File " & outfile & " does not exist."
                                          severity error;

        -- sim loop
        report "Simulation loop. " & integer'image(repeats) & " rounds.";
        for cnt in 0 to repeats-1 loop
            -- write values from file
             hwrite(outline, q, right, 8);
            writeline(f_out, outline);
            -- load new values
            t1load <= '1';
            d <= std_logic_vector(to_unsigned(cnt+1, d'length));
            -- wait 1 clock cycle
            wait until rising_edge(clk);
            t1load <= '0';
        end loop;

        report "Closing " & outfile & " ...";
        file_close(f_out);

        rstx <= '0';
        wait until rising_edge(clk);

        report "Simulation END";
        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;

end Testbench;
