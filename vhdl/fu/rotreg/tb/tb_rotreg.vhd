--===========================================================================--
-- Testbench for rotating register (rotreg)
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- standard : VHDL-2008
--===========================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
-- reading/writing std_logic(_vector) from/to files:
-- use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;

entity tb_rotreg is
    -- empty;
end tb_rotreg;

architecture testbench of tb_rotreg is

    -- uut component
    component rotreg
        generic (
            dataw     : positive := DATA_W;
            reg_count : positive);
        port (
            d_i       : in  std_logic_vector(dataw-1 downto 0);
            load_i    : in  std_logic;
            q_o       : out std_logic_vector(dataw-1 downto 0);
            clk       : in  std_logic;
            glock     : in  std_logic;
            rstx      : in  std_logic);
    end component;

    signal d       : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal load    : std_logic := '0';
    signal q       : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal glock   : std_logic := '0';
    signal rstx    : std_logic := '0';

    constant ITERS : positive := 20;

    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time      := 10 ns;

begin -- testbench

    -- uut instance
    uut : rotreg
        generic map (
            dataw     => DATA_W,
            reg_count => 10)
        port map (
            d_i       => d,
            load_i    => load,
            q_o       => q,
            clk       => clk,
            glock     => glock,
            rstx      => rstx);

    -- clock process
    clk_proc: process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process clk_proc;

    -- stimulation process
    stim_proc : process
        file f_out       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable outfile : string(1 to 8) := "dump.txt";
        variable outline : line;
        variable cnt     : integer := 0;
        --variable offset  : integer := LATENCY;
        --variable repeats : integer := ((NEXP+1)/2)*(2**NEXP) + LATENCY;
        variable repeats : integer := ITERS;
    begin

        wait for clk_period*1.5; -- synchronize to rising edge
        rstx <= '1';
        wait for clk_period*2;

        -- open files
        report "Opening " & outfile & " ...";
        file_open(fstatus, f_out, outfile, write_mode);
        assert fstatus = OPEN_OK report "File " & outfile & " does not exist."
                                          severity error;

        -- sim loop
        report "Simulation loop. " & integer'image(repeats) & " rounds.";
        for cnt in 0 to repeats-1 loop
            -- write values from the previous clock cycle
             hwrite(outline, q, right, 8);
            writeline(f_out, outline);
            -- load new values
            d <= std_logic_vector(to_unsigned(cnt+1, d'length));
            load <= '1';
            -- wait 1 clock cycle
            wait for clk_period;
            load <= '0';
        end loop;

        report "Closing " & outfile & " ...";
        file_close(f_out);

        rstx <= '0';
        wait for clk_period;

        report "Simulation END";
        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;

end testbench;
