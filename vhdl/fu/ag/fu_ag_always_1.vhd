--===========================================================================--
-- Address generator wrapper for TCE (latency 1)
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- It is a top level entity for ag_arch providing a standard TCE-compatible
-- interface
--===========================================================================--

-------------------------------------------------------------------------------
-- Entity of AG, latency 1
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity fu_ag_always_1 is
    generic (
        dataw  : positive := DATA_W;
        busw   : positive := BUS_W;
        nexpw  : positive := NEXP_W);
    port (
        t1data : in  std_logic_vector(dataw-1 downto 0);
        t1load : in  std_logic;
        o1data : in  std_logic_vector(nexpw-1 downto 0);
        o1load : in  std_logic;
        o2data : in  std_logic_vector(dataw-1 downto 0);
        o2load : in  std_logic;
        r1data : out std_logic_vector(busw-1 downto 0);
        glock  : in  std_logic;
        rstx   : in  std_logic;
        clk    : in  std_logic);
end fu_ag_always_1;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of AG, latency 1
-------------------------------------------------------------------------------
architecture rtl of fu_ag_always_1 is

    component ag_arith
        generic (
            dataw     : positive;
            nexpw     : positive;
            idxw      : positive;
            stagew    : positive;
            mau_shift : natural);
        port (
            lidx_i    : in  std_logic_vector(dataw-1 downto 0);
            nexp_i    : in  std_logic_vector(nexpw-1 downto 0);
            base_i    : in  std_logic_vector(dataw-1 downto 0);
            addr_o    : out std_logic_vector(dataw-1 downto 0));
    end component;

    signal t1reg   : std_logic_vector(dataw-1 downto 0);
    signal o1reg   : std_logic_vector(nexpw-1 downto 0);
    signal o2reg   : std_logic_vector(dataw-1 downto 0);
    signal control : std_logic_vector(2 downto 0);
    signal r1      : std_logic_vector(dataw-1 downto 0);

begin -- rtl

    fu_arch : ag_arith
        generic map (
            dataw     => dataw,
            nexpw     => nexpw,
            idxw      => IDX_W,
            stagew    => STAGE_W,
            mau_shift => MAU_ADDR_SHIFT)
        port map (
            lidx_i    => t1reg,
            nexp_i    => o1reg,
            base_i    => o2reg,
            addr_o    => r1);

    control <= o1load & o2load & t1load;

    regs : process (clk, rstx)
    begin
        if rstx = '0' then
            t1reg <= (others => '0');
            o1reg <= (others => '0');
            o2reg <= (others => '0');
        elsif rising_edge(clk) then
            if glock = '0' then
                case control is
                    when "001" =>
                        t1reg <= t1data;
                    when "010" =>
                        o2reg <= o2data;
                    when "011" =>
                        o2reg <= o2data;
                        t1reg <= t1data;
                    when "100" =>
                        o1reg <= o1data;
                    when "101" =>
                        o1reg <= o1data;
                        t1reg <= t1data;
                    when "110" =>
                        o1reg <= o1data;
                        o2reg <= o2data;
                    when "111" =>
                        o1reg <= o1data;
                        o2reg <= o2data;
                        t1reg <= t1data;
                    when others => null;
                end case;
            end if;
        end if;
    end process regs;

    r1data <= std_logic_vector(resize(unsigned(r1), busw));

end rtl;
-------------------------------------------------------------------------------
