--===========================================================================--
-- Testbench for address generator - combinatory description (ag_arith)
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- standard : VHDL-2008
--===========================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
-- reding/writing std_logic(_vector) from/to files:
-- use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;

entity tb_ag_arith is
    -- port ( );
end tb_ag_arith;

architecture Behavioral of tb_ag_arith is

    -- uut component
    component ag_arith
        generic (
            dataw     : positive := DATA_W;
            nexpw     : positive := NEXP_W;
            idxw      : positive := IDX_W;
            stagew    : positive := STAGE_W;
            mau_shift : natural  := MAU_ADDR_SHIFT);
        port (
            lidx_i    : in  std_logic_vector(dataw-1 downto 0);
            nexp_i    : in  std_logic_vector(nexpw-1 downto 0);
            base_i    : in  std_logic_vector(dataw-1 downto 0);
            addr_o    : out std_logic_vector(dataw-1 downto 0));
    end component;

    -- constants
    constant C_NEXP    : positive := 8;
    constant C_LATENCY : natural  := 0;
    -- inputs
    signal lidx : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal nexp : std_logic_vector(NEXP_W-1 downto 0) := (others => '0');
    signal base : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    -- outputs
    signal addr : std_logic_vector(DATA_W-1 downto 0);
    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time      := 10 ns;

begin

    -- uut instance
    uut : ag_arith
        generic map (
            dataw     => DATA_W,
            nexpw     => NEXP_W,
            idxw      => IDX_W,
            stagew    => STAGE_W,
            mau_shift => MAU_ADDR_SHIFT)
        port map (
            lidx_i    => lidx,
            nexp_i    => nexp,
            base_i    => base,
            addr_o    => addr);

    -- clock process
    clk_proc: process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;

    -- stimulation process
    stim_proc : process
        file f_out       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable outfile : string(1 to 13) := "res/sim_ag_08";
        variable outline : line;
        variable cnt     : integer := 0;
        variable offset  : integer := C_LATENCY;
        variable repeats : integer := ((C_NEXP+1)/2)*(2**C_NEXP) + C_LATENCY;
    begin

        wait for clk_period*1.5; -- synchronize to rising edge
        nexp <= std_logic_vector(to_unsigned(C_NEXP, nexp'length));
        base <= (others => '0');
        wait for clk_period*2;

        -- open files
        report "Opening " & outfile & " ...";
        file_open(fstatus, f_out, outfile, write_mode);
        assert fstatus = OPEN_OK report "File " & outfile & " does not exist."
                                          severity error;

        -- sim loop
        report "Simulation loop. " & integer'image(repeats) & " rounds.";
        for cnt in 0 to repeats-1 loop
            -- load new values
            lidx <= std_logic_vector(to_unsigned(cnt, lidx'length));
            -- wait 1 clock cycle
            wait for clk_period;
            -- write values to file
             hwrite(outline, addr, right, 8);
            writeline(f_out, outline);
        end loop;

        report "Closing " & outfile & " ...";
        file_close(f_out);

        wait for clk_period;

        report "Simulation END";
        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;

end Behavioral;
