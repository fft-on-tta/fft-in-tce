library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
-- reading/writing std_logic(_vector) from/to files:
use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;

entity tb_ag_always_1 is
    -- port ( );
end tb_ag_always_1;

architecture Behavioral of tb_ag_always_1 is
    -- UUT
    component fu_ag_always_1
        generic (
            dataw  : integer := DATA_W;
            busw   : integer := BUS_W;
            nexpw  : integer := NEXP_W);
        port (
            t1data : in  std_logic_vector(dataw-1 downto 0);
            t1load : in  std_logic;
            o1data : in  std_logic_vector(nexpw-1 downto 0);
            o1load : in  std_logic;
            o2data : in  std_logic_vector(dataw-1 downto 0);
            o2load : in  std_logic;
            r1data : out std_logic_vector(busw-1 downto 0);
            glock  : in  std_logic;
            rstx   : in  std_logic;
            clk    : in  std_logic);
    end component;
    -- inputs
    signal index  : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal n_exp  : std_logic_vector(NEXP_W-1 downto 0) := (others => '0');
    signal base   : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    -- loads
    signal o1load : std_logic := '0';
    signal o2load : std_logic := '0';
    signal t1load : std_logic := '0';
    -- reset and lock
    signal glock  : std_logic := '0';
    signal rstx   : std_logic := '0';
    -- output
    signal addr   : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time := 10 ns;

begin
    uut: fu_ag_always_1
        generic map (
            dataw => DATA_W,
            busw  => BUS_W,
            nexpw => NEXP_W)
        port map (
            t1data => index,
            t1load => t1load,
            o1data => n_exp,
            o1load => o1load,
            o2data => base,
            o2load => o2load,
            r1data => addr,
            glock  => glock,
            rstx   => rstx,
            clk    => clk);

    -- clock process
    clk_proc: process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;

    -- stimulus process
    stim_proc : process
        variable cnt     : integer := 0;
        variable repeats : integer := 192;
    begin
        rstx   <= '0';
        glock  <= '0';
        t1load <= '0';
        o1load <= '0';
        o2load <= '0';
        wait for clk_period;
        rstx  <= '1';
        glock <= '0';
        n_exp <= x"6";
        o1load <= '1';
        base  <= x"00000000";
        o2load <= '1';
        wait for clk_period;
        o1load <= '0';
        o2load <= '0';
        wait for clk_period;

        for cnt in 0 to repeats-1 loop
            index <= std_logic_vector(to_unsigned(cnt, index'length));
            t1load <= '1';
            wait for clk_period;
            t1load <= '0';
        end loop;
        wait;
    end process;

end Behavioral;
