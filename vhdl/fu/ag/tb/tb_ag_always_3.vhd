library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
-- reading/writing std_logic(_vector) from/to files:
-- use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;
use work.tb_utils.all;

entity tb_ag_always_3 is
    -- port ( );
end tb_ag_always_3;

architecture testbench of tb_ag_always_3 is

    -- uut
    component fu_ag_always_3
        generic (
            dataw  : integer := DATA_W;
            busw   : integer := BUS_W;
            nexpw  : integer := NEXP_W);
        port (
            t1data : in  std_logic_vector(dataw-1 downto 0);
            t1load : in  std_logic;
            o1data : in  std_logic_vector(nexpw-1 downto 0);
            o1load : in  std_logic;
            o2data : in  std_logic_vector(dataw-1 downto 0);
            o2load : in  std_logic;
            r1data : out std_logic_vector(busw-1 downto 0);
            glock  : in  std_logic;
            rstx   : in  std_logic;
            clk    : in  std_logic);
    end component;
    -- inputs
    signal index  : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal n_exp  : std_logic_vector(NEXP_W-1 downto 0) := (others => '0');
    signal base   : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    -- loads
    signal o1load : std_logic := '0';
    signal o2load : std_logic := '0';
    signal t1load : std_logic := '0';
    -- reset and lock
    signal glock  : std_logic := '1';
    signal rstx   : std_logic := '0';
    -- output
    signal addr   : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time := 10 ns;
    -- FFT size and latency
    constant NEXP    : positive := 6;
    constant LATENCY : integer := 3;

begin

    uut: fu_ag_always_3
        generic map (
            dataw => DATA_W,
            busw  => BUS_W,
            nexpw => NEXP_W)
        port map (
            t1data => index,
            t1load => t1load,
            o1data => n_exp,
            o1load => o1load,
            o2data => base,
            o2load => o2load,
            r1data => addr,
            glock  => glock,
            rstx   => rstx,
            clk    => clk);

    -- clock signal
    clk <= not clk after clk_period/2;

    -- stimulus process
    stim_proc : process
        file f_out       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable outfile : string(1 to 8) := "dump.txt";
        variable outline : line;
        variable cnt     : integer := 0;
        variable repeats : integer := ((NEXP+1)/2)*(2**NEXP) + LATENCY;
    begin
        -- init
        wait_until_rising_edges(clk, 2);
        rstx  <= '1';
        wait until rising_edge(clk);
        glock <= '0';
        wait until rising_edge(clk);
        n_exp <= std_logic_vector(to_unsigned(NEXP, n_exp'length));
        o1load <= '1';
        base  <= x"00000000";
        o2load <= '1';
        wait until rising_edge(clk);
        o1load <= '0';
        o2load <= '0';
        wait until rising_edge(clk);

        -- open files
        report "Opening " & outfile & " ...";
        file_open(fstatus, f_out, outfile, write_mode);
        assert fstatus = OPEN_OK report "File " & outfile & " does not exist."
                                          severity error;

        -- sim loop
        for cnt in 0 to repeats-1 loop
            index <= std_logic_vector(to_unsigned(cnt, index'length));
            t1load <= '1';
            wait until rising_edge(clk);
             hwrite(outline, addr, right, 8);
            writeline(f_out, outline);
            t1load <= '0';
        end loop;

        report "Closing " & outfile & " ...";
        file_close(f_out);

        wait until rising_edge(clk);

        report "Simulation END";
        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;

end testbench;
