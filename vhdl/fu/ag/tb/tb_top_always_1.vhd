library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use std.textio.all;
-- reading/writing std_logic(_vector) from/to files:
--use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;

entity tb_top_always_1 is
    -- port ( );
end tb_top_always_1;

architecture Behavioral of tb_top_always_1 is
    -- UUT
    component top_ag_always_1
        port (
            clk_in : in  std_logic;
            sw_i   : in  std_logic_vector(1 downto 0);
            led_o  : out std_logic_vector(3 downto 0));
    end component;

    -- clock
    constant clk_period : time := 20 ns; -- 50 MHz
    signal clk : std_logic     := '0';

    signal sw  : std_logic_vector(1 downto 0);

begin

    uut: top_ag_always_1
        port map (
            clk_in => clk,
            sw_i   => sw);

    -- clock process
    clk_proc: process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;

    -- stimulus process
    stim_proc : process
    begin
        sw <= "00";
        wait for clk_period;
        sw <= "10";
        wait for clk_period * 200;
        wait;
    end process;

end Behavioral;
