% Generate input and reference files for testing CMUL. a, b are inputs,
% res is output.
% Values of inputs lie in the interval [-(1-2^(-15)), 1-2^(-15)].
%
% Number format: 32 bit fixed point complex numbers
%    - bits 1..16 real part (Q1.15)
%    - bits 17..32 imaginary part (Q1.15)
%
% Numbers are multiplied as a(i) * b(i) = res(i)
% Inputs a, b are in one file. a(i), b(i) are on one line.
% Line format:
%     aaaaAAAA_bbbbBBBB (uppercase - real, lowercase - imag, '_' - space)

close all
clear all
clc

abfile = '../ab.txt';
resfile = '../ref/ref.txt';
% The maximum value since 1 is out of the allowed range.
% Equals to 1 - 2^(-15) qhere 2^(-15) is the quantization step.
maxval = 0.999969482421875;

% Get input vectors a, b
try
    load('a.mat');
    load('b.mat');
    fprintf('Values a and b loaded from files.\n')
catch
    a = zeros(1000,1);
    b = zeros(1000,1);    

    % First are all combinations of (0, 1, -1) for real/imag parts
    re_im_values = [0, maxval, -maxval];
    n = 0;
    
    for ar = re_im_values
        for ai = re_im_values
            for br = re_im_values
                for bi = re_im_values                
                    n = n + 1;                
                    a(n) = ar + ai*1j;
                    b(n) = br + bi*1j;               
                end
            end
        end
    end

    % The rest are random values (-1, 1)
    a(n+1:1000) = rand(1000-n,1)*2-1 + (rand(1000-n,1)*2-1) * 1j;
    b(n+1:1000) = rand(1000-n,1)*2-1 + (rand(1000-n,1)*2-1) * 1j;
        
    fprintf('Values a and b generated randomly.\n')
end

% Compute results
%res = a .* b / 2;

% Convert to fixed point
fp_a = fi(a, true, 16, 15);
fp_b = fi(b, true, 16, 15);
%fp_res = fi(res, true, 16, 15);


F = fimath;
% Fixed point multiply
fp_res_mul = mpy(F, fp_a, fp_b);
% Resize
fp_res = fi(fp_res_mul / 2, true, 16, 15);

% Print input file (a, b)
abfid = fopen(abfile, 'wt');
for n = 1:1000
    s_a = sprintf('%s%s', hex(imag(fp_a(n))), hex(real(fp_a(n))));
    s_b = sprintf('%s%s', hex(imag(fp_b(n))), hex(real(fp_b(n))));
    fprintf(abfid, '%s %s\n', upper(s_a), upper(s_b));
   
end
fclose(abfid);

% Print reference results file (res)
resfid = fopen(resfile, 'wt');
for n = 1:1000
    s_res = sprintf('%s%s', hex(imag(fp_res(n))), hex(real(fp_res(n))));
    fprintf(resfid, '%s\n', upper(s_res));
end
fclose(resfid);
