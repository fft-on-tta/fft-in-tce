--===========================================================================--
-- Complex multiplier
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- Multiplies 2 complex numbers A and B and produces a result P. LSBs are
-- the real part, MSBs imag part. Both real/imag part are fixed point values
-- with one sign bit (in interval [-1, 1)).
--
-- Adder overflow is avoided by dividing each addition by two.
-- Multiplier overflow (-1 * -1) is not avoided => use only numbers (-1, 1).
-- Rounding after addition (towards nearest, ties to positive infinity).
--
-- TODO: shortcut if multiplying by 1 or 0?
--===========================================================================--


--===========================================================================--
-- Entity of a pure combinatory CMUL
-------------------------------------------------------------------------------
-- Generics:
--    dataw  : Width of the data bus
--    halfw  : Half of the width of the data bus
--
-- Inputs:
--    A      : First input
--    B      : Second input
--
-- Outputs:
--    P      : Result
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity cmul_arith is
    generic (
        dataw : integer;
        halfw : integer);
    port (
        A   : in  std_logic_vector (dataw-1 downto 0);   -- 1st operand
        B   : in  std_logic_vector (dataw-1 downto 0);   -- 2nd operand
        P   : out std_logic_vector (dataw-1 downto 0));  -- product
end cmul_arith;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of pure combinatory CMUL
-------------------------------------------------------------------------------
architecture Behavioral of cmul_arith is

    -- inputs divided to real/imag parts (16b)
    signal a_re   : signed(halfw-1 downto 0);
    signal b_re   : signed(halfw-1 downto 0);
    signal a_im   : signed(halfw-1 downto 0);
    signal b_im   : signed(halfw-1 downto 0);
    -- multiplication results (32b)
    signal mul1   : signed(dataw-1 downto 0);
    signal mul2   : signed(dataw-1 downto 0);
    signal mul3   : signed(dataw-1 downto 0);
    signal mul4   : signed(dataw-1 downto 0);
    -- adder results (33b)
    signal add1   : signed(dataw downto 0);
    signal add2   : signed(dataw downto 0);
    -- truncated adder results (18b - SSI.FFFFFFFFFFFFFFF )
    signal trunc1 : signed(halfw+1 downto 0);
    signal trunc2 : signed(halfw+1 downto 0);
    -- rounded adder result after truncation (18b)
    signal round1 : signed(halfw+1 downto 0);
    signal round2 : signed(halfw+1 downto 0);
    -- final result real/imag parts (16b)
    signal p_re   : signed(halfw-1 downto 0);
    signal p_im   : signed(halfw-1 downto 0);

begin

    a_re <= signed(A(halfw-1 downto 0));
    a_im <= signed(A(dataw-1 downto halfw));
    b_re <= signed(B(halfw-1 downto 0));
    b_im <= signed(B(dataw-1 downto halfw));

    mul1 <= a_re * b_re;
    mul2 <= a_im * b_im;
    mul3 <= a_re * b_im;
    mul4 <= a_im * b_re;

    add1 <= (resize(mul1, add1'length)) - (resize(mul2, add1'length));
    add2 <= (resize(mul3, add2'length)) + (resize(mul4, add2'length));

    -- divide by two
    trunc1 <= shift_right(add1(dataw downto halfw-1), 1);
    trunc2 <= shift_right(add2(dataw downto halfw-1), 1);

    round1 <= trunc1 + 1 when add1(halfw-1) = '1' else
              trunc1;
    round2 <= trunc2 + 1 when add2(halfw-1) = '1' else
              trunc2;

    p_re <= resize(round1, halfw);
    p_im <= resize(round2, halfw);

    P <= std_logic_vector(p_im) & std_logic_vector(p_re);

end Behavioral;
--===========================================================================--


--===========================================================================--
-- Entity of a pipelined CMUL with 1 stage
-------------------------------------------------------------------------------
-- Generics:
--    dataw   : Width of the data bus
--    halfw   : Half of the width of the data bus
--
-- Inputs:
--    A       : First input
--    B       : Second input
--    glock   : Global lock
--    rstx    : Asynchdonous reset (active low)
--    clk     : Clock
--
-- Outputs:
--    P       : Result
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cmul_pipe_1 is
    generic (
        dataw : integer;
        halfw : integer);
    port (
        A       : in  std_logic_vector (dataw-1 downto 0);
        B       : in  std_logic_vector (dataw-1 downto 0);
        P       : out std_logic_vector (dataw-1 downto 0);
        glock   : in  std_logic;
        rstx    : in  std_logic;
        clk     : in  std_logic);
end cmul_pipe_1;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of a pipelined CMUL with 1 stage
-------------------------------------------------------------------------------
architecture Behavioral of cmul_pipe_1 is

    -- inputs divided to real/imag parts (16b)
    signal a_re       : signed(halfw-1 downto 0);
    signal b_re       : signed(halfw-1 downto 0);
    signal a_im       : signed(halfw-1 downto 0);
    signal b_im       : signed(halfw-1 downto 0);
    -- multiplication results (32b)
    signal mul1       : signed(dataw-1 downto 0);
    signal mul2       : signed(dataw-1 downto 0);
    signal mul3       : signed(dataw-1 downto 0);
    signal mul4       : signed(dataw-1 downto 0);
    -- adder results (33b)
    signal add1       : signed(dataw downto 0);
    signal add2       : signed(dataw downto 0);
    -- truncated adder results (18b - SSI.FFFFFFFFFFFFFFF )
    signal trunc1     : signed(halfw+1 downto 0);
    signal trunc2     : signed(halfw+1 downto 0);
    -- rounded adder result after truncation (18b)
    signal round1     : signed(halfw+1 downto 0);
    signal round2     : signed(halfw+1 downto 0);
    -- final result real/imag parts (16b)
    signal p_re       : signed(halfw-1 downto 0);
    signal p_im       : signed(halfw-1 downto 0);
    -- registers
    signal out_reg    : std_logic_vector(P'range);

begin

    a_re <= signed(A(halfw-1 downto 0));
    a_im <= signed(A(dataw-1 downto halfw));
    b_re <= signed(B(halfw-1 downto 0));
    b_im <= signed(B(dataw-1 downto halfw));

    mul1 <= a_re * b_re;
    mul2 <= a_im * b_im;
    mul3 <= a_re * b_im;
    mul4 <= a_im * b_re;

    add1 <= (resize(mul1, add1'length)) - (resize(mul2, add1'length));
    add2 <= (resize(mul3, add2'length)) + (resize(mul4, add2'length));

    -- divide by two
    trunc1 <= shift_right(add1(dataw downto halfw-1), 1);
    trunc2 <= shift_right(add2(dataw downto halfw-1), 1);

    round1 <= trunc1 + 1 when add1(halfw-1) = '1' else
              trunc1;
    round2 <= trunc2 + 1 when add2(halfw-1) = '1' else
              trunc2;

    p_re <= resize(round1, halfw);
    p_im <= resize(round2, halfw);

    P <= out_reg;

    reg : process(clk, rstx)
        variable i : natural;
    begin --reg
        if rstx = '0' then
            out_reg <= (others => '0');
        end if;
        if rising_edge(clk) then
            if glock = '0' then
                -- out reg (1)
                out_reg <= std_logic_vector(p_im) & std_logic_vector(p_re);
            end if;
        end if;
    end process reg;

end Behavioral;
--===========================================================================--


--===========================================================================--
-- Entity of a pipelined CMUL with 2 stages
-------------------------------------------------------------------------------
-- Generics:
--    dataw   : Width of the data bus
--    halfw   : Half of the width of the data bus
--
-- Inputs:
--    A       : First input
--    B       : Second input
--    glock   : Global lock
--    rstx    : Asynchdonous reset (active low)
--    clk     : Clock
--
-- Outputs:
--    P       : Result
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cmul_pipe_2 is
    generic (
        dataw : integer;
        halfw : integer);
    port (
        A       : in  std_logic_vector (dataw-1 downto 0);
        B       : in  std_logic_vector (dataw-1 downto 0);
        P       : out std_logic_vector (dataw-1 downto 0);
        glock   : in  std_logic;
        rstx    : in  std_logic;
        clk     : in  std_logic);
end cmul_pipe_2;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of a pipelined CMUL with 2 stages
-------------------------------------------------------------------------------
architecture Behavioral of cmul_pipe_2 is

    -- inputs divided to real/imag parts (16b)
    signal a_re       : signed(halfw-1 downto 0);
    signal b_re       : signed(halfw-1 downto 0);
    signal a_im       : signed(halfw-1 downto 0);
    signal b_im       : signed(halfw-1 downto 0);
    -- multiplication results (32b)
    signal mul1       : signed(dataw-1 downto 0);
    signal mul2       : signed(dataw-1 downto 0);
    signal mul3       : signed(dataw-1 downto 0);
    signal mul4       : signed(dataw-1 downto 0);
    -- adder results (33b)
    signal add1       : signed(dataw downto 0);
    signal add2       : signed(dataw downto 0);
    -- truncated adder results (18b - SSI.FFFFFFFFFFFFFFF )
    signal trunc1     : signed(halfw+1 downto 0);
    signal trunc2     : signed(halfw+1 downto 0);
    -- rounded adder result after truncation (18b)
    signal round1     : signed(halfw+1 downto 0);
    signal round2     : signed(halfw+1 downto 0);
    -- final result real/imag parts (16b)
    signal p_re       : signed(halfw-1 downto 0);
    signal p_im       : signed(halfw-1 downto 0);
    -- registers
    constant N_OUTREG : natural := 2;
    type t_out_reg is array (0 to N_OUTREG-1) of std_logic_vector(P'range);
    signal out_reg    : t_out_reg;

begin

    a_re <= signed(A(halfw-1 downto 0));
    a_im <= signed(A(dataw-1 downto halfw));
    b_re <= signed(B(halfw-1 downto 0));
    b_im <= signed(B(dataw-1 downto halfw));

    mul1 <= a_re * b_re;
    mul2 <= a_im * b_im;
    mul3 <= a_re * b_im;
    mul4 <= a_im * b_re;

    add1 <= (resize(mul1, add1'length)) - (resize(mul2, add1'length));
    add2 <= (resize(mul3, add2'length)) + (resize(mul4, add2'length));

    -- divide by two
    trunc1 <= shift_right(add1(dataw downto halfw-1), 1);
    trunc2 <= shift_right(add2(dataw downto halfw-1), 1);

    round1 <= trunc1 + 1 when add1(halfw-1) = '1' else
              trunc1;
    round2 <= trunc2 + 1 when add2(halfw-1) = '1' else
              trunc2;

    p_re <= resize(round1, halfw);
    p_im <= resize(round2, halfw);

    P <= out_reg(out_reg'high);

    reg : process(clk, rstx)
        variable i : natural;
    begin --reg
        if rstx = '0' then
            for i in 0 to N_OUTREG-1 loop
                out_reg(i) <= (others => '0');
            end loop;
        end if;
        if rising_edge(clk) then
            if glock = '0' then
                -- out reg (2)
                out_reg(0) <= std_logic_vector(p_im) & std_logic_vector(p_re);
                for i in 1 to N_OUTREG-1 loop
                    out_reg(i) <= out_reg(i-1);
                end loop;
            end if;
        end if;
    end process reg;

end Behavioral;
--===========================================================================--
