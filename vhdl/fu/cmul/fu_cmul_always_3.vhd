--===========================================================================--
-- Complex multiplier wrapper for TCE (latency 3)
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- It is a top level entity for cmul_pipe_2 providing a standard TCE-compatible
-- interface
--===========================================================================--


--===========================================================================--
-- Entity of CMUL, latency 3
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity fu_cmul_always_3 is
    generic (
        dataw : integer := DATA_W;
        halfw : integer := HALF_W;
        busw  : integer := BUS_W);
    port (
        t1data   : in  std_logic_vector(dataw-1 downto 0);
        t1load   : in  std_logic;
        o1data   : in  std_logic_vector(dataw-1 downto 0);
        o1load   : in  std_logic;
        r1data   : out std_logic_vector(busw-1 downto 0);
        glock    : in  std_logic;
        rstx     : in  std_logic;
        clk      : in  std_logic);
end fu_cmul_always_3;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of CMUL, latency 3
-------------------------------------------------------------------------------
architecture rtl of fu_cmul_always_3 is

    component cmul_pipe_1
        generic (
            dataw : integer;
            halfw : integer);
        port (
            A       : in  std_logic_vector(dataw-1 downto 0);
            B       : in  std_logic_vector(dataw-1 downto 0);
            P       : out std_logic_vector(dataw-1 downto 0);
            glock   : in  std_logic;
            rstx    : in  std_logic;
            clk     : in  std_logic);
    end component;

    signal t1reg         : std_logic_vector(dataw-1 downto 0);
    signal o1reg         : std_logic_vector(dataw-1 downto 0);
    -- o1tmp is used for o1reg to be loaded only on trigger
    signal o1tmp         : std_logic_vector(dataw-1 downto 0);
    signal r1reg         : std_logic_vector(dataw-1 downto 0);
    signal control       : std_logic_vector(1 downto 0);
    signal r1            : std_logic_vector(dataw-1 downto 0);
    -- Latency of this FU. Must be >=3.
    constant LATENCY     : positive := 3;
    signal result_en_reg : std_logic_vector(LATENCY-2 downto 0);

begin

    fu_arch : cmul_pipe_1
        generic map (
            dataw => dataw,
            halfw => halfw)
        port map (
            A       => t1reg,
            B       => o1reg,
            P       => r1,
            glock   => glock,
            rstx    => rstx,
            clk     => clk);

    control <= o1load & t1load;

    regs : process (clk, rstx)
        variable i : positive;
    begin -- regs
        if rstx = '0' then
            t1reg <= (others => '0');
            o1reg <= (others => '0');
            o1tmp <= (others => '0');
            r1reg <= (others => '0');
            result_en_reg <= (others => '0');
        elsif rising_edge(clk) then
            if glock = '0' then
                case control is
                    when "11" =>
                        o1reg <= o1data;
                        o1tmp <= o1data;
                        t1reg <= t1data;
                    when "10" =>
                        o1tmp <= o1data;
                    when "01" =>
                        t1reg <= t1data;
                        o1reg <= o1tmp;
                    when others => null;
                end case;

                result_en_reg(0) <= t1load;
                for i in 1 to result_en_reg'high loop
                    result_en_reg(i) <= result_en_reg(i-1);
                end loop;
                if result_en_reg(result_en_reg'high) = '1' then
                    r1reg <= r1;
                end if;
            end if;
        end if;
    end process regs;

    r1data <= std_logic_vector(resize(signed(r1reg), busw));

end rtl;
--===========================================================================--
