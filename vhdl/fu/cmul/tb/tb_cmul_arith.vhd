--==========================================================================--
-- Testbench for cmul_arith
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- standard: VHDL-2008
--==========================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
-- reding/writing std_logic(_vector) from/to files:
--use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

entity tb_cmul_arith is
    -- port ( );
end tb_cmul_arith;

architecture Testbench of tb_cmul_arith is

    -- UUT
    component cmul_arith
        generic (
            dataw : integer := 32;
            halfw : integer := 16);
        port (
            A   : in  std_logic_vector (dataw-1 downto 0);
            B   : in  std_logic_vector (dataw-1 downto 0);
            P   : out std_logic_vector (dataw-1 downto 0));
    end component;
    -- inputs
    signal a : std_logic_vector(31 downto 0) := (others => '0');
    signal b : std_logic_vector(31 downto 0) := (others => '0');
    -- outputs
    signal p : std_logic_vector(31 downto 0);
    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time := 10 ns;

begin
    uut: cmul_arith
        generic map (
            dataw => 32,
            halfw => 16)
        port map (
            A   => a,
            B   => b,
            P   => p);

    -- clock process
    clk_proc: process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;

    -- stimulus process
    stim_proc: process
        file f_in        : text;
        file f_out       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable infile  : string(1 to 6) := "ab.txt";
        variable outfile : string(1 to 7) := "res.txt";
        variable inline  : line;
        variable outline : line;
        variable v_a     : std_logic_vector(31 downto 0);
        variable v_b     : std_logic_vector(31 downto 0);
        variable v_space : character;
    begin

        wait for clk_period * 2;

        -- open files
        report "Opening " & infile & " (read mode) ...";
        file_open(fstatus, f_in, infile, read_mode);
        assert fstatus = OPEN_OK report "File " & infile & " does not exist."
                                          severity error;
        report "Opening " & outfile & " (write mode) ...";
        file_open(fstatus, f_out, outfile, write_mode);
        assert fstatus = OPEN_OK report "File " & outfile & " does not exist."
                                          severity error;

        -- file loop
        while not endfile(f_in) loop
            readline(f_in, inline);
            hread(inline, v_a);
            read(inline, v_space);  -- read in the space character
            hread(inline, v_b);
            a <= v_a;
            b <= v_b;
             wait for clk_period;
             hwrite(outline, p, right, 8);
            writeline(f_out, outline);
        end loop;

        -- close files
        report "Closing " & infile & " ...";
        file_close(f_in);
        report "Closing " & outfile & " ...";
        file_close(f_out);

        wait for clk_period;

        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;

end Testbench;
