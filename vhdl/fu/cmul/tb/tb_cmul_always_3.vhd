--==========================================================================--
-- Testbench for fu_cmul_always_3
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- standard: VHDL-2008
--==========================================================================--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
-- reding/writing std_logic(_vector) from/to files:
--use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;
use work.tb_utils.all;

entity tb_cmul_always_3 is
    -- port ( );
end tb_cmul_always_3;

architecture Testbench of tb_cmul_always_3 is

    -- uut
    component fu_cmul_always_3
        generic (
            dataw : integer := DATA_W;
            halfw : integer := HALF_W;
            busw  : integer := BUS_W);
        port (
            t1data   : in  std_logic_vector(dataw-1 downto 0);
            t1load   : in  std_logic;
            o1data   : in  std_logic_vector(dataw-1 downto 0);
            o1load   : in  std_logic;
            r1data   : out std_logic_vector(busw-1 downto 0);
            glock    : in  std_logic;
            rstx     : in  std_logic;
            clk      : in  std_logic);
    end component;

    -- inputs
    signal a         : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal b         : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    -- loads
    signal a_load    : std_logic := '0';
    signal b_load    : std_logic := '0';
    -- output
    signal p         : std_logic_vector(BUS_W-1 downto 0) := (others => '0');
    -- clock period
    signal clk          : std_logic := '0';
    constant clk_period : time      := 10 ns;
    -- control
    signal glock     : std_logic := '1';
    signal rstx      : std_logic := '0';
    -- constants
    constant LATENCY : natural   := 3;

begin

    uut : fu_cmul_always_3
        generic map (
            dataw  => DATA_W,
            halfw  => HALF_W,
            busw   => BUS_W)
        port map (
            t1data => a,
            t1load => a_load,
            o1data => b,
            o1load => b_load,
            r1data => p,
            glock  => glock,
            rstx   => rstx,
            clk    => clk);

    -- use this to make simulator update clock before signal assignment
    clk <= not clk after clk_period/2;

    -- stimulus process
    stim_proc: process
        file f_in        : text;
        file f_out       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable infile  : string(1 to 12) := "ab_small.txt";
        variable outfile : string(1 to 7) := "res.txt";
        variable inline  : line;
        variable outline : line;
        variable v_a     : std_logic_vector(DATA_W-1 downto 0);
        variable v_b     : std_logic_vector(DATA_W-1 downto 0);
        variable v_space : character;
        variable i       : natural;
    begin

        -- open files
        report "Opening " & infile & " (read mode) ...";
        file_open(fstatus, f_in, infile, read_mode);
        assert fstatus = OPEN_OK report "File " & infile & " does not exist."
                                          severity error;
        report "Opening " & outfile & " (write mode) ...";
        file_open(fstatus, f_out, outfile, write_mode);
        assert fstatus = OPEN_OK report "File " & outfile & " does not exist."
                                          severity error;

        -- init
        wait_until_rising_edges(clk, 2);
        rstx <= '1';
        wait until rising_edge(clk);
        glock <= '0';
        wait until rising_edge(clk);

        -- file loop (inputs)
        while not endfile(f_in) loop
            a_load <= '1';
            b_load <= '1';
            readline(f_in, inline);
            hread(inline, v_a);
            read(inline, v_space);  -- read in the space character
            hread(inline, v_b);
            a <= v_a;
            b <= v_b;
            wait until rising_edge(clk);
             hwrite(outline, p, right, 8);
            writeline(f_out, outline);
        end loop;
        a_load <= '0';
        b_load <= '0';
        -- finish writing the last outputs
        for i in 0 to LATENCY-1 loop
            wait until rising_edge(clk);
             hwrite(outline, p, right, 8);
            writeline(f_out, outline);
        end loop;

        -- close files
        report "Closing " & infile & " ...";
        file_close(f_in);
        report "Closing " & outfile & " ...";
        file_close(f_out);

        wait until rising_edge(clk);

        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;

end Testbench;
