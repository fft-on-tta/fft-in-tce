function Y = butterfly( fp_P, rx2_flag )
    Y = zeros(4,1);
    if rx2_flag == true
        % Two radix-2 butterflies
        Y(1) = fp_P(1) + fp_P(2);
        Y(2) = fp_P(1) - fp_P(2);
        Y(3) = fp_P(3) + fp_P(4);
        Y(4) = fp_P(3) - fp_P(4);
        % Divide by two (one addition)
        Y = Y / 2;
    else
        % One radix-4 butterfly
        Y(1) = fp_P(1) +    fp_P(2) + fp_P(3) +    fp_P(4);
        Y(2) = fp_P(1) - 1j*fp_P(2) - fp_P(3) + 1j*fp_P(4);
        Y(3) = fp_P(1) -    fp_P(2) + fp_P(3) -    fp_P(4);
        Y(4) = fp_P(1) + 1j*fp_P(2) - fp_P(3) - 1j*fp_P(4);
        % Divide by four (two addition)
        Y = Y / 4;
    end
end
