--===========================================================================--
-- Complex adder
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- Performs one radix-4 butterfly or two radix-2 operations on two samples
-- loaded after each other in sequence. Contains 2 parts - sequential and
-- combinatory. Every clock cycle it outputs one number.
--
-- cadd_reg:
--     Internal register. Stores a sequence of 4 samples loaded in series.
--     Every 4th clock cycle they are loaded into the cadd_comb. Every clock
--     cycle an opcode is incremented. Radix-4 or -2 mode is governed by the
--     rx2_i input.
--
-- cadd_comb:
--     Computes one butterfly result per clock cycle. Opcode determines which
--     sample is being computed and whether to use radix-4 or -2.
--
-- Every addition divided by two. Radix-4 mode divides by 4, radix-2 by 2.
-- No rounding.
--===========================================================================--


--===========================================================================--
-- Entity of CADD register and opcode counter
-------------------------------------------------------------------------------
-- Generics
--     dataw  : Width of the data word
--
-- Inputs
--     t_i    : Input data
--     load_i : Load signal (triggers operation)
--     rx2_i  : Radix-2 mode indicator
--     glock  : Global lock
--     rstx   : Asynchronous reset
--     clk    : Clock
--
-- Outputs
--     opc_o  : Computed opcode
--     a_o    : First input
--     b_o    : Second input
--     c_o    : Third input
--     d_o    : Fourth input
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cadd_reg is
    generic (
        dataw : integer := 32);
    port (
        t_i    : in  std_logic_vector(dataw-1 downto 0);
        load_i : in  std_logic;
        rx2_i  : in  std_logic;
        opc_o  : out std_logic_vector(2 downto 0);
        a_o    : out std_logic_vector(dataw-1 downto 0);
        b_o    : out std_logic_vector(dataw-1 downto 0);
        c_o    : out std_logic_vector(dataw-1 downto 0);
        d_o    : out std_logic_vector(dataw-1 downto 0);
        glock  : in  std_logic;
        rstx   : in  std_logic;
        clk    : in  std_logic);
end cadd_reg;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of CADD register and opcode counter
-------------------------------------------------------------------------------
architecture Behavioral of cadd_reg is

    signal cnt     : unsigned(1 downto 0);
    signal rx2_reg : std_logic;
    signal rx2     : std_logic;
    signal a_reg   : std_logic_vector(dataw-1 downto 0);
    signal b_reg   : std_logic_vector(dataw-1 downto 0);
    signal c_reg   : std_logic_vector(dataw-1 downto 0);
    signal a       : std_logic_vector(dataw-1 downto 0);
    signal b       : std_logic_vector(dataw-1 downto 0);
    signal c       : std_logic_vector(dataw-1 downto 0);
    signal d       : std_logic_vector(dataw-1 downto 0);
    signal load_reg : std_logic_vector(3 downto 0);

begin -- Behavioral of cadd_reg

    counter_2b : process (clk, rstx)
    begin
        if rstx = '0' then
            cnt <= "00";
        else
            if rising_edge(clk) then
                if (glock = '0') and (load_i = '1') then
                    cnt <= cnt + 1;
                end if;
            end if;
        end if;
    end process counter_2b;

    operands : process (clk, rstx)
    begin
        if rstx = '0' then
            a_reg   <= (others => '0');
            b_reg   <= (others => '0');
            c_reg   <= (others => '0');
            rx2_reg <= '0';
        end if;
        if rising_edge(clk) then
            if (glock = '0') and (load_i = '1') then
                case cnt is
                    when "00" =>
                        rx2_reg <= rx2_i;
                        a_reg   <= t_i;
                    when "01" =>
                        b_reg  <= t_i;
                    when "10" =>
                        c_reg  <= t_i;
                    when "11" =>
                        a   <= a_reg;
                        b   <= b_reg;
                        c   <= c_reg;
                        d   <= t_i;
                        rx2 <= rx2_reg;
                    when others => null;
                end case;
            end if;
        end if;
    end process operands;

    a_o   <= a;
    b_o   <= b;
    c_o   <= c;
    d_o   <= d;
    opc_o <= rx2 & std_logic_vector(cnt);

end Behavioral;
--===========================================================================--


--===========================================================================--
-- Entity of CADD combinatorial description
-------------------------------------------------------------------------------
-- Generics
--     dataw : Width of the data word
--     halfw : Half of the width of the data word
--
-- Inputs
--     a_i   : First input
--     b_i   : Second input
--     c_i   : Third input
--     d_i   : Fourth input
--     opc_i : Opcode
--
-- Outputs
--     res_o : Result
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cadd_arith is
    generic (
        dataw : integer := 32;
        halfw : integer := 16);
    port (
        a_i   : in  std_logic_vector(dataw-1 downto 0);
        b_i   : in  std_logic_vector(dataw-1 downto 0);
        c_i   : in  std_logic_vector(dataw-1 downto 0);
        d_i   : in  std_logic_vector(dataw-1 downto 0);
        opc_i : in  std_logic_vector(2 downto 0);
        res_o : out std_logic_vector(dataw-1 downto 0));
end cadd_arith;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of CADD combinatorial description
-------------------------------------------------------------------------------
architecture Behavioral of cadd_arith is

    signal rx2 : std_logic;
    signal opc : std_logic_vector(1 downto 0);
    -- inputs (17b - ready for addition)
    signal a_re : signed(halfw downto 0);
    signal b_re : signed(halfw downto 0);
    signal c_re : signed(halfw downto 0);
    signal d_re : signed(halfw downto 0);
    signal a_im : signed(halfw downto 0);
    signal b_im : signed(halfw downto 0);
    signal c_im : signed(halfw downto 0);
    signal d_im : signed(halfw downto 0);
    -- intermideate (16b - they get resized after addition)
    signal re_add1 : signed(halfw-1 downto 0);
    signal re_add2 : signed(halfw-1 downto 0);
    signal im_add1 : signed(halfw-1 downto 0);
    signal im_add2 : signed(halfw-1 downto 0);
    -- result (16b - they get resized after addition)
    signal res_re : signed(halfw-1 downto 0);
    signal res_im : signed(halfw-1 downto 0);

begin -- Behavioral of cadd_arith

    rx2 <= opc_i(2);
    opc <= opc_i(1 downto 0);
    a_re <= resize(signed(a_i(halfw-1 downto 0)), halfw+1);
    b_re <= resize(signed(b_i(halfw-1 downto 0)), halfw+1);
    c_re <= resize(signed(c_i(halfw-1 downto 0)), halfw+1);
    d_re <= resize(signed(d_i(halfw-1 downto 0)), halfw+1);
    a_im <= resize(signed(a_i(dataw-1 downto halfw)), halfw+1);
    b_im <= resize(signed(b_i(dataw-1 downto halfw)), halfw+1);
    c_im <= resize(signed(c_i(dataw-1 downto halfw)), halfw+1);
    d_im <= resize(signed(d_i(dataw-1 downto halfw)), halfw+1);

    add_1 : process(rx2, opc, a_re, b_re, c_re, d_re, a_im, b_im, c_im, d_im)
        variable re_tmp1 : signed(halfw downto 0); -- 17b
        variable re_tmp2 : signed(halfw downto 0);
        variable im_tmp1 : signed(halfw downto 0);
        variable im_tmp2 : signed(halfw downto 0);
    begin -- add_1
        case rx2 is
            when '0' =>  -- radix-4 butterfly
                case opc is
                    when "00" =>
                        re_tmp1 :=    a_re + b_re;
                        re_tmp2 :=    c_re + d_re;
                        im_tmp1 :=    a_im + b_im;
                        im_tmp2 :=    c_im + d_im;
                    when "01" =>
                        re_tmp1 :=    a_re + b_im;
                        re_tmp2 := - (c_re + d_im);
                        im_tmp1 :=    a_im - b_re;
                        im_tmp2 := - (c_im - d_re);
                    when "10" =>
                        re_tmp1 :=    a_re - b_re;
                        re_tmp2 :=    c_re - d_re;
                        im_tmp1 :=    a_im - b_im;
                        im_tmp2 :=    c_im - d_im;
                    when "11" =>
                        re_tmp1 :=    a_re - b_im;
                        re_tmp2 := - (c_re - d_im);
                        im_tmp1 :=    a_im + b_re;
                        im_tmp2 := - (c_im + d_re);
                    when others =>
                        re_tmp1 := (others => '0');
                        re_tmp2 := (others => '0');
                        im_tmp1 := (others => '0');
                        im_tmp2 := (others => '0');
                end case;
                -- Division by 2
                re_add1 <= re_tmp1(halfw downto 1);
                re_add2 <= re_tmp2(halfw downto 1);
                im_add1 <= im_tmp1(halfw downto 1);
                im_add2 <= im_tmp2(halfw downto 1);
            when '1' =>  -- radix-2 butterfly
                case opc is
                    when "00" =>
                        re_tmp1 :=   a_re;
                        re_tmp2 :=   b_re;
                        im_tmp1 :=   a_im;
                        im_tmp2 :=   b_im;
                    when "01" =>
                        re_tmp1 :=   a_re;
                        re_tmp2 := - b_re;
                        im_tmp1 :=   a_im;
                        im_tmp2 := - b_im;
                    when "10" =>
                        re_tmp1 :=   c_re;
                        re_tmp2 :=   d_re;
                        im_tmp1 :=   c_im;
                        im_tmp2 :=   d_im;
                    when "11" =>
                        re_tmp1 :=   c_re;
                        re_tmp2 := - d_re;
                        im_tmp1 :=   c_im;
                        im_tmp2 := - d_im;
                    when others =>
                        re_tmp1 := (others => '0');
                        re_tmp2 := (others => '0');
                        im_tmp1 := (others => '0');
                        im_tmp2 := (others => '0');
                end case;
                -- No division (there was no addition)
                re_add1 <= re_tmp1(halfw-1 downto 0);
                re_add2 <= re_tmp2(halfw-1 downto 0);
                im_add1 <= im_tmp1(halfw-1 downto 0);
                im_add2 <= im_tmp2(halfw-1 downto 0);
            when others =>
                re_tmp1 := (others => '0');
                re_tmp2 := (others => '0');
                im_tmp1 := (others => '0');
                im_tmp2 := (others => '0');
                -- Nulls
                re_add1 <= (others => '0');
                re_add2 <= (others => '0');
                im_add1 <= (others => '0');
                im_add2 <= (others => '0');
        end case;
    end process add_1;

    add_2 : process(re_add1, re_add2, im_add1, im_add2)
        variable res_re_tmp : signed(halfw downto 0); -- 17b
        variable res_im_tmp : signed(halfw downto 0);
    begin -- add_2
        res_re_tmp := resize(re_add1, halfw+1) + resize(re_add2, halfw+1);
        res_im_tmp := resize(im_add1, halfw+1) + resize(im_add2, halfw+1);
        -- Division by 2
        res_re <= res_re_tmp(halfw downto 1);
        res_im <= res_im_tmp(halfw downto 1);
    end process add_2;

    res_o <= std_logic_vector(res_im) & std_logic_vector(res_re);

end Behavioral;
--===========================================================================--


--===========================================================================--
-- Entity of full CADD unit
-------------------------------------------------------------------------------
-- Generics
--     dataw : Width of the data word
--     halfw : Half of the width of the data word
--
-- Inputs
--     t_i    : Input data
--     load_i : Load signal (triggers operation)
--     rx2_i  : Radix-2 mode indicator
--     glock  : Global lock
--     rstx   : Asynchronous reset
--     clk    : Clock
--
-- Outputs
--     res_o : Result
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cadd_full is
    generic (
        dataw  : integer := 32;
        halfw  : integer := 16);
    port (
        t_i    : in  std_logic_vector(dataw-1 downto 0);
        load_i : in  std_logic;
        rx2_i  : in  std_logic;
        res_o  : out std_logic_vector(dataw-1 downto 0);
        glock  : in  std_logic;
        rstx   : in  std_logic;
        clk    : in  std_logic);
end cadd_full;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Architecture of full CADD unit
-------------------------------------------------------------------------------
architecture Structural of cadd_full is

    component cadd_reg
        generic (
            dataw : integer := 32);
        port (
            t_i    : in  std_logic_vector(dataw-1 downto 0);
            load_i : in  std_logic;
            rx2_i  : in  std_logic;
            opc_o  : out std_logic_vector(2 downto 0);
            a_o    : out std_logic_vector(dataw-1 downto 0);
            b_o    : out std_logic_vector(dataw-1 downto 0);
            c_o    : out std_logic_vector(dataw-1 downto 0);
            d_o    : out std_logic_vector(dataw-1 downto 0);
            glock  : in  std_logic;
            rstx   : in  std_logic;
            clk    : in  std_logic);
    end component;

    component cadd_arith
        generic (
            dataw : integer := 32;
            halfw : integer := 16);
        port (
            a_i   : in  std_logic_vector(dataw-1 downto 0);
            b_i   : in  std_logic_vector(dataw-1 downto 0);
            c_i   : in  std_logic_vector(dataw-1 downto 0);
            d_i   : in  std_logic_vector(dataw-1 downto 0);
            opc_i : in  std_logic_vector(2 downto 0);
            res_o : out std_logic_vector(dataw-1 downto 0));
    end component;

    signal a   : std_logic_vector(dataw-1 downto 0);
    signal b   : std_logic_vector(dataw-1 downto 0);
    signal c   : std_logic_vector(dataw-1 downto 0);
    signal d   : std_logic_vector(dataw-1 downto 0);
    signal opc : std_logic_vector(2 downto 0);

begin -- Structural of cadd_full

    inst_cadd_reg : cadd_reg
        generic map (
            dataw => dataw)
        port map (
            t_i    => t_i,
            load_i => load_i,
            rx2_i  => rx2_i,
            opc_o  => opc,
            a_o    => a,
            b_o    => b,
            c_o    => c,
            d_o    => d,
            glock  => glock,
            rstx   => rstx,
            clk    => clk);

    inst_cadd_arith : cadd_arith
        generic map (
            dataw => dataw,
            halfw => halfw)
        port map (
            a_i   => a,
            b_i   => b,
            c_i   => c,
            d_i   => d,
            opc_i => opc,
            res_o => res_o);

end Structural;
--===========================================================================--
