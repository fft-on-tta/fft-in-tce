--==========================================================================--
-- Testbench for fu_cadd_always_1
--
-- This file is a part of https://gitlab.com/fft-on-tta/fft-in-tce
--
-- standard: VHDL-2008
--==========================================================================--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
-- reding/writing std_logic(_vector) from/to files:
-- use ieee.std_logic_textio.all; -- from VHDL-2008 included in std.textio

library work;
use work.constants.all;
use work.tb_utils.all;

entity tb_cadd_always_1 is
    -- port ( );
end tb_cadd_always_1;

architecture Testbench of tb_cadd_always_1 is

    -- uut component
    component fu_cadd_always_1 is
        generic (
            dataw : integer := DATA_W;
            halfw : integer := HALF_W;
            busw  : integer := BUS_W);
        port (
            t1data   : in  std_logic_vector(dataw-1 downto 0);
            t1load   : in  std_logic;
            o1data   : in  std_logic_vector(0 downto 0);
            o1load   : in  std_logic;
            r1data   : out std_logic_vector(busw-1 downto 0);
            glock    : in  std_logic;
            rstx     : in  std_logic;
            clk      : in  std_logic);
    end component;

    -- Signals
    signal t      : std_logic_vector(DATA_W-1 downto 0) := (others => '0');
    signal rx2    : std_logic := '0';
    signal res    : std_logic_vector(DATA_W-1 downto 0);
    signal t1load : std_logic := '0';
    signal o1load : std_logic := '0';
    signal glock  : std_logic := '0';
    signal rstx   : std_logic := '0';
    signal clk    : std_logic := '0';

    constant clk_period : time := 10 ns;

begin -- Testbench

    -- uut instance
    uut: fu_cadd_always_1
        generic map (
            dataw => DATA_W,
            halfw => HALF_W,
            busw  => BUS_W)
        port map (
            t1data => t,
            t1load => t1load,
            o1data(0) => rx2,
            o1load => o1load,
            r1data => res,
            glock  => glock,
            rstx   => rstx,
            clk    => clk);

    -- clock signal
    clk <= not clk after clk_period/2;

    -- stimulus process
    stim_proc: process
        file f_in        : text;
        file f_out       : text;
        variable fstatus : FILE_OPEN_STATUS;
        variable infile  : string(1 to 6) := "in.txt";
        variable outfile : string(1 to 7) := "res.txt";
        variable inline  : line;
        variable outline : line;
        variable v_rx2   : std_logic;
        variable v_in    : std_logic_vector(31 downto 0);
        variable v_space : character;
    begin

        -- init
        wait_until_rising_edges(clk, 2);
        rstx <= '1';
        wait until rising_edge(clk);

        -- open files
        report "Opening " & infile & " (read mode) ...";
        file_open(fstatus, f_in, infile, read_mode);
        assert fstatus = OPEN_OK report "File " & infile & " does not exist."
                                          severity error;
        report "Opening " & outfile & " (write mode) ...";
        file_open(fstatus, f_out, outfile, write_mode);
        assert fstatus = OPEN_OK report "File " & outfile & " does not exist."
                                          severity error;

        -- file loop
        while not endfile(f_in) loop
            o1load <= '1';
            t1load <= '1';
            -- read inputs from file
            readline(f_in, inline);
            read(inline, v_rx2);
            read(inline, v_space);  -- read in the space character
            hread(inline, v_in);
            -- load the inputs
            rx2 <= v_rx2;
            t <= v_in;
            -- wait 1 clock cycle
            wait until rising_edge(clk);
            -- write results to file
            hwrite(outline, res, right, 8);
            writeline(f_out, outline);
        end loop;

        -- set loads to 0
        o1load <= '0';
        t1load <= '0';

        -- close files
        report "Closing " & infile & " ...";
        file_close(f_in);
        report "Closing " & outfile & " ...";
        file_close(f_out);

        wait until rising_edge(clk);

        -- VHDL-2008
        std.env.finish(0);

    end process stim_proc;

end Testbench;
