
    library(ram_4096x32) {
        delay_model             : table_lookup;
        revision                : 2015_Spr;
        date                    : "February 06, 2015";
        comment                 : "This is a Automatic memory model generated base on CACTI-P, USE WITH CARE";
        time_unit               : "1ns";
        voltage_unit            : "1V";
        current_unit            : "1mA";
        leakage_power_unit      : "1mW";
        nom_process             : 1.028;
        nom_temperature         : 25.000;
        nom_voltage             : 0.85;
        capacitive_load_unit     (1,pf);

        pulling_resistance_unit : "1kohm";

        /* additional header data */
        default_cell_leakage_power  : 0;
        default_fanout_load         : 1;
        default_inout_pin_cap       : 0.01;
        default_input_pin_cap       : 0.01;
        default_output_pin_cap      : 0.0;
        default_max_transition      : 0.8; 

        /* default attributes */
        default_leakage_power_density : 8.2922e-06;
        slew_derate_from_library      : 1;
        slew_lower_threshold_pct_fall : 20.000;
        slew_upper_threshold_pct_fall : 80.000;
        slew_lower_threshold_pct_rise : 20.000;
        slew_upper_threshold_pct_rise : 80.000;
        input_threshold_pct_fall      : 60.000;
        input_threshold_pct_rise      : 40.000;
        output_threshold_pct_fall     : 60.000;
        output_threshold_pct_rise     : 40.000;

        /* k-factors */
        k_process_cell_fall             : 1;
        k_process_cell_leakage_power    : 4.05986E-06;
        k_process_cell_rise             : 1;
        k_process_fall_transition       : 1;
        k_process_hold_fall             : 1;
        k_process_hold_rise             : 1;
        k_process_internal_power        : 0;
        k_process_min_pulse_width_high  : 1;
        k_process_min_pulse_width_low   : 1;
        k_process_pin_cap               : 0;
        k_process_recovery_fall         : 1;
        k_process_recovery_rise         : 1;
        k_process_rise_transition       : 1;
        k_process_setup_fall            : 1;
        k_process_setup_rise            : 1;
        k_process_wire_cap              : 0;
        k_process_wire_res              : 0;
        k_temp_cell_fall                : -0.000376;
        k_temp_cell_rise                : -0.000376;
        k_temp_hold_fall                : 0.0;
        k_temp_hold_rise                : 0.0;
        k_temp_min_pulse_width_high     : -0.000376;
        k_temp_min_pulse_width_low      : -0.000376;
        k_temp_min_period               : 0.0;
        k_temp_rise_propagation         : 0.0;
        k_temp_fall_propagation         : 0.0;
        k_temp_rise_transition          : 0.0;
        k_temp_fall_transition          : 0.0;
        k_temp_recovery_fall            : 0.0;
        k_temp_recovery_rise            : 0.0;
        k_temp_setup_fall               : 0.0;
        k_temp_setup_rise               : 0.0;
        k_volt_cell_fall                : -2.765714;
        k_volt_cell_rise                : -2.765714;
        k_volt_hold_fall                : 0.0;
        k_volt_hold_rise                : 0.0;
        k_volt_min_pulse_width_high     : -2.765714;
        k_volt_min_pulse_width_low      : -2.765714;
        k_volt_min_period               : 0.0;
        k_volt_rise_propagation         : 0.0;
        k_volt_fall_propagation         : 0.0;
        k_volt_rise_transition          : 0.0;
        k_volt_fall_transition          : 0.0;
        k_volt_recovery_fall            : 0.0;
        k_volt_recovery_rise            : 0.0;
        k_volt_setup_fall               : 0.0;
        k_volt_setup_rise               : 0.0;
        k_volt_internal_power           : 2.5714286;

        operating_conditions(typical) {
            process         : 1.028;
            temperature     : 25.000;
            voltage         : 0.85;
            tree_type       : balanced_tree;
        }

        wire_load("sample") {
            resistance      : 1.6e-05;
            capacitance     : 0.0002;
            area            : 1.7;
            slope           : 500;
            fanout_length    (1,500);
        }
        lu_table_template(mem_delay_template) {
           variable_1 : total_output_net_capacitance;
               index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        lu_table_template(mem_constraint_template_1) {
               variable_1 : related_pin_transition;
                   index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        lu_table_template(mem_constraint_template_2) {
               variable_1 : constrained_pin_transition;
                   index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        lu_table_template(mem_load_template) {
               variable_1 : total_output_net_capacitance;
                   index_1 ("1000, 1001, 1002, 1003, 1004, 1005, 1006");
        }
        power_lut_template(mem_passive_energy_template) {
           variable_1 : input_transition_time;
               index_1 ("1000, 1001");
        }
        library_features(report_delay_calculation);
        type (DATA_BUS) {
            base_type   : array ;
            data_type   : bit ;
            bit_width   : 32;
            bit_from    : 31;
            bit_to      : 0 ;
            downto      : true ;
        }

        type (ADDR_BUS) {
            base_type : array ;
            data_type : bit ;
            bit_width : 12;
            bit_from  : 11;
            bit_to    : 0 ;
            downto    : true ;
        }

        type (BYTEMASK_BUS) {
            base_type : array ;
            data_type : bit ;
            bit_width : 4;
            bit_from  : 3;
            bit_to    : 0 ;
            downto    : true ;
        }


    cell(ram_4096x32) {
        area         : 24572.7;
        dont_use     : TRUE;
        dont_touch   : TRUE;
        interface_timing : TRUE;
        memory() {
            type : ram;
            address_width : 12;
            word_width : 32;
        }


        /********************************************************************
            MEMORY WRITE INTERFACE: CENB (wr_en), AB (wr_addr), DB (wr data), CLKB
        **********************************************************************/
        pin(wr_en_x) {
            direction : input;
            capacitance : 0.0005;
            timing() {
                related_pin : "clk" ;
                timing_type : setup_rising ;
                rise_constraint(mem_constraint_template_1) {
                /*************************************************************
                    CHANGES WITH RELATED PIN (CLK) TRANSITION
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values ( "0.256, 0.258, 0.262, 0.275, 0.302, 0.349, 0.446")
                }
                fall_constraint(mem_constraint_template_2) {
                /*************************************************************
                    CHANGES WITH CONSTRAINED PIN TRANSITION
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values ( "0.506, 0.513, 0.520, 0.550, 0.609, 0.712, 0.920")
                }
            }
            timing() {
                related_pin : "clk" ;
                timing_type : hold_rising ;
                rise_constraint(mem_constraint_template_2) {
                /*************************************************************
                    NO HOLD CONSTRATINT FOR CENB W.R.T CLKB
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000")
                }
                fall_constraint(mem_constraint_template_2) {
                /*************************************************************
                    NO HOLD CONSTRATINT FOR CENB W.R.T CLKB
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000")
                }
            }
        }

        bus (addr)  {
            bus_type : ADDR_BUS;
            direction : input;
            capacitance : 0.0008;
            internal_power(){
                when : "!mem_en_x";
                power(mem_passive_energy_template) {
                    index_1 ("0.0 1.0");
                    values ("0.0371537, 0.0371537") 
                    /* Decoding Power*/
                }
            }
            timing() {
                related_pin : "clk"
                timing_type : setup_rising ;
                rise_constraint(mem_constraint_template_2) {
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.1308, 0.132, 0.1332, 0.1404, 0.1548, 0.1788, 0.228")
                    /*************************************************************
                        CHANGES WITH CONSTRAINED PIN TRANSITION
                    ************************************************************** */
                }
                fall_constraint(mem_constraint_template_2) {
                    /*************************************************************
                        CHANGES WITH CONSTRAINED PIN TRANSITION
                    ************************************************************** */
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.0492, 0.0528, 0.0552, 0.0708, 0.1008, 0.1524, 0.2568")
                }
            }
            timing() {
                related_pin : "clk"
                timing_type : hold_rising ;
                rise_constraint(mem_constraint_template_2) {
                /*************************************************************
                    NO HOLD CONSTRATINT FOR AB W.R.T CLKB
                ************************************************************** */
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000")
                }
                fall_constraint(mem_constraint_template_2) {
                /*************************************************************
                    NO HOLD CONSTRATINT FOR AB W.R.T CLKB
                ************************************************************** */
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000")
                }
            }
        }

        bus (wr_data) {
            bus_type : DATA_BUS;
            direction : input;
            capacitance : 0.0007;
            memory_write() {
                address : addr;
                clocked_on : "clk";
            }
            timing() {
                related_pin : "clk"
                timing_type : setup_rising ;
                rise_constraint(mem_constraint_template_1) {
                /*************************************************************
                    CHANGES WITH RELATED PIN
                ************************************************************** */
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.036, 0.039, 0.041, 0.053, 0.077, 0.119, 0.203")
                }
                fall_constraint(mem_constraint_template_2) {
                    /*************************************************************
                        CHANGES WITH CONSTRAINED PIN
                    ************************************************************** */
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.077, 0.084, 0.094, 0.131, 0.206, 0.337, 0.599")
                }
            }
            timing() {
                related_pin : "clk"
                timing_type : hold_rising ;
                rise_constraint(mem_constraint_template_1) {
                /*************************************************************
                    CHANGES WITH RELATED PIN
                ************************************************************** */
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0")
                }
                fall_constraint(mem_constraint_template_2) {
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0")
                }
            }
        }

        pin(clk) {
            direction   : input;
            capacitance : 0.0006;
            clock       : true;
            min_pulse_width_low         : 0.0511; /*DFPHQ*/
            min_pulse_width_high        : 0.0306;
            min_period                  : 0.191766; 
            max_transition              : 0.8;
            internal_power(){
                when : "mem_en_x";
                power(mem_passive_energy_template) {
                    index_1 ("0.0 1.0");
                    values  ("0.0, 0.0")
                }
            }

            /*MEMORY WRITE*/
            internal_power(){
                when : "!mem_en_x & !wr_en_x";
                rise_power(mem_passive_energy_template) {
                    index_1 ("0.0 1.0");
                    values  ("3.73152, 3.73152")
                /* */
                }
                fall_power(mem_passive_energy_template) {
                    index_1 ("0.0 1.0");
                    values  ("0.0, 0.0")
                }
            }

            /*MEMORY READ*/
            internal_power(){
                when : "!mem_en_x & wr_en_x" ;
                rise_power(mem_passive_energy_template) {
                    index_1 ("0.0 1.0");
                    values  ("3.73152, 3.73152")
                    /* */
                }
                fall_power(mem_passive_energy_template) {
                    index_1 ("0.0 1.0");
                    values  ("0.0, 0.0")
                }
            }
            
        }

        pin(mem_en_x) {
            direction : input;
            capacitance : 0.0005;
            timing() {
                related_pin : "clk" ;
                timing_type : setup_rising ;
                rise_constraint(mem_constraint_template_1) {
                /*************************************************************
                    CHANGES WITH RELATED PIN (CLK) TRANSITION
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values ( "0.256, 0.258, 0.262, 0.275, 0.302, 0.349, 0.446")
                }
                fall_constraint(mem_constraint_template_2) {
                /*************************************************************
                    CHANGES WITH CONSTRAINED PIN TRANSITION
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values ( "0.506, 0.513, 0.520, 0.550, 0.609, 0.712, 0.920")
                }
            }
            timing() {
                related_pin : "clk" ;
                timing_type : hold_rising ;
                rise_constraint(mem_constraint_template_2) {
                /*************************************************************
                    NO HOLD CONSTRATINT FOR CENB W.R.T CLKB
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000")
                }
                fall_constraint(mem_constraint_template_2) {
                /*************************************************************
                    NO HOLD CONSTRATINT FOR CENB W.R.T CLKB
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000")
                }
            }
        }

        bus(bytemask_x){
            bus_type : BYTEMASK_BUS;
            direction : input;
            capacitance : 0.0005;

            timing() {
                related_pin : "clk"
                timing_type : setup_rising ;
                rise_constraint(mem_constraint_template_1) {
                /*************************************************************
                    CHANGES WITH RELATED PIN
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.036, 0.039, 0.041, 0.053, 0.077, 0.119, 0.203")
                }
                fall_constraint(mem_constraint_template_2) {
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    /*************************************************************
                        CHANGES WITH CONSTRAINED PIN
                    ***************************************************************/
                    values  ("0.077, 0.084, 0.094, 0.131, 0.206, 0.337, 0.599")
                }
            }
            timing() {
                related_pin : "clk"
                timing_type : hold_rising ;
                rise_constraint(mem_constraint_template_1) {
                /*************************************************************
                    CHANGES WITH RELATED PIN
                ***************************************************************/
                    index_1 ("0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8");
                    values  ("0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0")
                }
                fall_constraint(mem_constraint_template_2) {
                    values  ("0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0")
                }
            }
        }

        bus(rd_data) {
            bus_type : DATA_BUS;
            direction : output;
            memory_read() {
                address : addr;
            }
            timing() {
                related_pin     : "clk";
                timing_type     : rising_edge;
                timing_sense    : non_unate;

                cell_rise(mem_delay_template) { 
                /*************************************************************
                    CHANGES WITH OUTPUT NET CAPACITANCE
                ***************************************************************/
                    
                    index_1 ("2.0e-4, 0.0019, 0.0037, 0.0075, 0.015, 0.03, 0.0894");
                    values  ("0.227071, 0.231261, 0.234676, 0.241121, 0.253037, 0.276398, 0.368566")
                }
                cell_fall(mem_delay_template) {
                /*************************************************************
                    CHANGES WITH OUTPUT NET CAPACITANCE
                ***************************************************************/
                    index_1 ("2.0e-4, 0.0019, 0.0037, 0.0075, 0.015, 0.03, 0.0894");
                    values  ("0.227071, 0.231261, 0.234676, 0.241121, 0.253037, 0.276398, 0.368566")
                }
                rise_transition(mem_load_template) {
                    index_1 ("2.0e-4, 0.0019, 0.0037, 0.0075, 0.015, 0.03, 0.0894");
                    values  ("0.004968, 0.009573, 0.014518, 0.025155, 0.046752, 0.090941, 0.26652")
                }
                fall_transition(mem_load_template) {
                    index_1 ("2.0e-4, 0.0019, 0.0037, 0.0075, 0.015, 0.03, 0.0894");
                    values  ("0.006053, 0.010449, 0.014412, 0.021981, 0.036817, 0.067923, 0.1952")
                }
            }
        }

        cell_leakage_power : 2.49122;
        }
    }

