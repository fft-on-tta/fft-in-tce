------------------------------------------------------------------------------
-- Title      : Parallel memory logic for TTA processor
-- Project    :
------------------------------------------------------------------------------
-- File       : par_mem_logic_reg.vhdl
-- Author     : Jarno Tanskanen  <jarno.tanskanen@cs.tut.fi>
-- Company    : TUT
-- Created    : 2005/09/26
-- Last update: 2007/10/02
-- Platform   :
-- Standard   : VHDL'87
------------------------------------------------------------------------------
-- Descriptions: This design is intended for a system, which has one
-- register on the route of lockrq and thus, one clock cycle
-- delay between the activation of lockrq and glock (global lock). The lockrq
-- signal can be directly routed in to the Move core, where it translates
-- to the glock (global lock) signal.
--
-- Par_mem_logic is designed to locate between the load-store units of
-- type fu_ld_st_always_3 and synchronous single-port memory modules. With
-- parallel memory, more expensive multi-port memory modules can be replaced
-- with single-port memory modules and/or number of memory ports can be
-- increased over what is available on multi-port memories (typically two
-- ports at most).
--
-- Top-level entity is par_mem_logic (structural_reg) described at the end of
-- this file. This file contains everything needed to produce the top-level
-- architecture. The par_mem_logic consists of 4 modules: ctrl_units,
-- addr_crsbar, wr_crsbar, and rd_crsbar.
--
-- This par_mem_logic provides the needed address computation,
-- interconnection, and conflict-resolving logic for parallel memory
-- architecture. This is a matched memory system, i.e., the number of LSUs
-- and memory modules is the same (PORTS). Currently, memory functions are
-- implemented for power-of-two PORTS values (2,4,8,16). However, by
-- re-implementing only the memory functions (mem_func, addr_func) for other
-- values of PORTS = 2,3,4,5,6,7,8, etc., the par_mem_logic should work
-- properly. In this case, non-power-of-two modulo and division operations
-- might be needed.
--
-- Parallel memory non-optimally emulates multi-port memory. Unlike in
-- multi-port memories, in parallel memories there can be memory conflicts,
-- i.e., a single memory module is tried to be accessed more than once during
-- a single cycle parallel memory access.
--
-- In the case of a conflict, the processor core can be locked by sending lock
-- request directly to the Move core and the conflict is resolved sequentially
-- requiring more cycles. The lock is released after the conflict resolving.
--
-- Also, all the registers in par_mem_logic are locked, if the Move core is
-- locked by someone else and par_mem_logic has not requested the lock by
-- itself.
--
-- For further information on par_mem_logic, please see the parallel memory
-- documentation.
------------------------------------------------------------------------------
-- Copyright (c) 2005
------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author    Description
-- 2005-09-26  1.0     tanskanen    Created
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Package declaration for par_mem_pkg
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

package par_mem_pkg is
------------------------------------------------------------------------------
  subtype par_mem_func is integer range 0 to 3;

  constant LOW_ORDER  : par_mem_func := 0;
  constant HIGH_ORDER : par_mem_func := 1;
  constant FFT_STRIDE_2_PORT : par_mem_func := 2;
  constant FFT_STRIDE_4_PORT : par_mem_func := 3;

  function mem_func(lsu_addr_in : std_logic_vector; LSU_ADDRW : integer;
                    CTRLW : integer; PORTS : integer; PM_FUNC : par_mem_func)
    return std_logic_vector;

  function addr_func(lsu_addr_in : std_logic_vector; LSU_ADDRW : integer;
                     CTRLW : integer; PORTS : integer; PM_FUNC : par_mem_func)
    return std_logic_vector;

  function pri_enc_func(data_in : std_logic_vector; CTRLW : integer;
                        PORTS : integer)
    return std_logic_vector;

  function dec_func(en_x : std_logic; data_in : std_logic_vector;
                    PORTS : integer)
    return std_logic_vector;

  function or_tree_func(data_in : std_logic_vector)
    return std_logic;

  function xor_tree_func(data_in : std_logic_vector)
    return std_logic;

  function xor_tree_func_2(data_in : std_logic_vector)
    return std_logic_vector;

end par_mem_pkg;
------------------------------------------------------------------------------
package body par_mem_pkg is

  ----------------------------------------------------------------------------
  -- Function mem_func corresponds to memory module assignment function,
  -- a key function for parallel memory architecture and determines the
  -- degree of conflict-freeness for a certain application memory address
  -- trace. Module assignment function is a function of the memory address and
  -- results the memory module index, where the addressed data is located
  -- (read) or is to be located (write). Address inside that module is given
  -- by the address assignment function addr_func.
  ----------------------------------------------------------------------------

  function mem_func(lsu_addr_in : std_logic_vector; LSU_ADDRW : integer;
                    CTRLW : integer; PORTS : integer; PM_FUNC : par_mem_func)
    return std_logic_vector is

    -- constant LSU_ADDRW      : integer := lsu_addr_in'length;
    variable module_num : std_logic_vector(CTRLW-1 downto 0);
    variable tmp        : std_logic_vector(lsu_addr_in'range);

  begin
    case PM_FUNC is
      when LOW_ORDER =>
        --  Example : Low-order interleaving (power of two).
        --  module_num := lsu_addr_in(CTRLW-1 downto 0);  -- Or alternatively
        --  =>
        module_num :=
        conv_std_logic_vector((conv_integer(lsu_addr_in) mod PORTS),CTRLW);

      when HIGH_ORDER =>
        --  Example : High-order interleaving (only power of two).
        module_num := lsu_addr_in(LSU_ADDRW-1 downto LSU_ADDRW-CTRLW);

      when FFT_STRIDE_2_PORT =>
        -- According to FFT parallel memory access scheme proposed by Jarmo
        -- Takala et al in ISCAS 2003, when PORTS = 2, the memory function
        -- simply corresponds to parity bit computation for the address.
        module_num := conv_std_logic_vector(xor_tree_func(lsu_addr_in), CTRLW);

      when FFT_STRIDE_4_PORT =>
        -- According to FFT parallel memory access scheme proposed by Jarmo
        -- Takala et al in ISCAS 2003, when PORTS = 4, the memory function
        -- simply corresponds to two parity bit computation for the address.
        module_num := ext(xor_tree_func_2(lsu_addr_in), CTRLW);

      when others => null;
    end case;
    return module_num;
  end mem_func;

  ----------------------------------------------------------------------------
  -- Function addr_func corresponds to address assignment function, another
  -- key function for parallel memory architecture. It is selected according
  -- to the corresponding  module assignment function. Address function
  -- (addr_func) is a function of the memory address and results the address
  -- for the memory module, referred by the module assignment function
  -- (mem_func).
  ----------------------------------------------------------------------------

  function addr_func(lsu_addr_in : std_logic_vector; LSU_ADDRW : integer;
                     CTRLW : integer; PORTS : integer; PM_FUNC : par_mem_func)
    return std_logic_vector is

    -- constant ADDRW        : integer := lsu_addr_in'length;
    variable mem_addr_out : std_logic_vector(LSU_ADDRW-CTRLW-1 downto 0);
    variable module_num     : std_logic_vector(CTRLW-1 downto 0);
  begin
    case PM_FUNC is
      when LOW_ORDER =>
        --  Example : Low-order interleaving (power of two).
        --  mem_addr_out := lsu_addr_in(LSU_ADDRW-1 downto CTRLW);
        -- Or alternat. =>
        mem_addr_out :=
          conv_std_logic_vector((conv_integer(lsu_addr_in)/PORTS),
                                (LSU_ADDRW-CTRLW));

      when HIGH_ORDER =>
        --  Example : High-order interleaving (only power of two).
        mem_addr_out := lsu_addr_in(LSU_ADDRW-CTRLW-1 downto 0);

      when FFT_STRIDE_2_PORT =>
        -- Address function is the same as for the LOW_ORDER.
        mem_addr_out :=
          conv_std_logic_vector((conv_integer(lsu_addr_in)/PORTS),
                                (LSU_ADDRW-CTRLW));

      when FFT_STRIDE_4_PORT =>
        -- have to take account the memory module
        --module_num := mem_func(addrs_in(i), LSU_ADDRW, CTRLW, PORTS,
        --                              PM_FUNC);
        --if module_num = 0 then
        --  mem_addr_out :=
        --    conv_std_logic_vector((conv_integer(lsu_addr_in)/PORTS),
        --                        (LSU_ADDRW-CTRLW));
        --end if;
        --mem_addr_out
        mem_addr_out :=
          conv_std_logic_vector((conv_integer(lsu_addr_in)/PORTS),
                                (LSU_ADDRW-CTRLW));

      when others => null;
    end case;
    return mem_addr_out;

  end addr_func;

  ----------------------------------------------------------------------------
  -- Function pri_enc_func is a priority encoding function and it is used in
  -- the conflict resolving to select the LSU to be served next. The index of
  -- the most significant '1' bit is returned. The function also codes into
  -- the return value if more '1's are left in data_in and activates the
  -- memory module enable bit mem_en_x (active low) when necessary.
  -- The idea for priority encoder function implementation was adapted from
  -- "Synopsys document "Guide to HDL Coding Styles for Synthesis",vV-2004.06,
  -- Example 3-6.
  ----------------------------------------------------------------------------

  function pri_enc_func(data_in : std_logic_vector; CTRLW : integer;
                        PORTS : integer)
    return std_logic_vector is

    variable addr_mux_ctrl : std_logic_vector(CTRLW-1 downto 0);
    variable mem_en_x      : std_logic;
    variable more_to_come  : std_logic;
    variable data_out      : std_logic_vector(2+CTRLW-1 downto 0);

  begin

    mem_en_x      := '1';
    more_to_come  := '0';
    addr_mux_ctrl := (others => '0');

    for i in 0 to (PORTS-1) loop
      if data_in(i) = '1' then
        addr_mux_ctrl := conv_std_logic_vector(i, CTRLW);

        if mem_en_x = '0' then
          more_to_come := '1';
        end if;

        mem_en_x := '0';
      end if;
    end loop;

    data_out(0)                  := more_to_come;
    data_out(1)                  := mem_en_x;
    data_out(2+CTRLW-1 downto 2) := addr_mux_ctrl;

    return data_out;

  end pri_enc_func;

  ----------------------------------------------------------------------------
  -- Decoder function dec_func.
  ----------------------------------------------------------------------------

  function dec_func(en_x : std_logic; data_in : std_logic_vector;
                    PORTS : integer)
    return std_logic_vector is

    variable data_out : std_logic_vector(PORTS-1 downto 0);

  begin

    data_out := (others => '0');

    if en_x = '0' then
      data_out(conv_integer(data_in)) := '1';
    end if;

    return data_out;

  end dec_func;

  ----------------------------------------------------------------------------
  -- Function or_tree_func calls itself iteratively and generates an OR tree
  -- for input vector data_in and returns a single result bit.
  -- The function is borrowed from Synopsys document "Guide to HDL Coding
  -- Styles for Synthesis", vV-2004.06, Example 3-11 VHDL for XOR Tree.
  ----------------------------------------------------------------------------

  function or_tree_func(data_in : std_logic_vector) return std_logic is

    variable upper_tree : std_logic;
    variable lower_tree : std_logic;
    variable mid, len   : natural;
    variable result     : std_logic;
    variable i_data     : std_logic_vector(data_in'length-1 downto 0);

  begin

    i_data := data_in;
    len    := i_data'length;

    if len = 1 then
      result := i_data(i_data'left);
    elsif len = 2 then
     result := i_data(i_data'left) or i_data(i_data'right);
    else
      mid        := (len + 1)/2 + i_data'right;
      upper_tree := or_tree_func(i_data(i_data'left downto mid));
      lower_tree := or_tree_func(i_data(mid-1 downto i_data'right));
      result     := upper_tree or lower_tree;
    end if;

    return result;

  end or_tree_func;

  ----------------------------------------------------------------------------
  -- Function xor_tree_func calls itself iteratively and generates an XOR tree
  -- for input vector data_in and returns a single result bit.
  -- The function is borrowed from Synopsys document "Guide to HDL Coding
  -- Styles for Synthesis", vV-2004.06, Example 3-11 VHDL for XOR Tree.
  ----------------------------------------------------------------------------

  function xor_tree_func(data_in : std_logic_vector) return std_logic is

    variable upper_tree : std_logic;
    variable lower_tree : std_logic;
    variable mid, len   : natural;
    variable result     : std_logic;
    variable i_data     : std_logic_vector(data_in'length-1 downto 0);
    variable result_2 : std_logic_vector(1 downto 0);

  begin

    i_data := data_in;
    len    := i_data'length;

    if len = 1 then
      result := i_data(i_data'left);
    elsif len = 2 then
      result := i_data(i_data'left) xor i_data(i_data'right);
    else
      mid        := (len + 1)/2 + i_data'right;
      upper_tree := xor_tree_func(i_data(i_data'left downto mid));
      lower_tree := xor_tree_func(i_data(mid-1 downto i_data'right));
      result     := upper_tree xor lower_tree;
    end if;

    return result;

  end xor_tree_func;

  function xor_tree_func_2(data_in : std_logic_vector) return std_logic_vector is

    variable upper_tree : std_logic;
    variable lower_tree : std_logic;
    variable mid, len   : natural;
    variable result     : std_logic;
    variable result1   : std_logic;
    variable i_data     : std_logic_vector(data_in'length-1 downto 0);
    variable odd : boolean := false;
    variable i_data0 : std_logic_vector(data_in'length /2 -1 downto 0);
    variable i_data1 : std_logic_vector(data_in'length /2 -1 downto 0);
    variable i  : integer;
    variable result2 : std_logic_vector(1 downto 0);
  begin

    i_data := data_in;
    len    := i_data'length;

    --if (len = 11) then
    result   := i_data(0) xor i_data(2) xor i_data(4) xor i_data(6) xor i_data(8) xor i_data(10);
    result1 := i_data(0) xor i_data(1) xor i_data(3) xor i_data(5) xor i_data(7) xor i_data(9);
    --else
--       if ((len mod 2) /= 0) then
--         odd := true;
--       else
--         odd := false;
--       end if;

--       if len > 2 then
--         if odd = true then
--           i_data1(0) := i_data(0);
--           i_data0(0) := i_data(0);
--         else
--           i_data0(0) := i_data(0);
--           i_data1(0) := i_data(1);
--         end if;
--         i := 1;
--         while i < len/2 loop
--           i_data0(i) := i_data(i*2);
--           i_data1(i) := i_data(i*2+1);
--           i := i+1;
--         end loop;
--       end if;

--       if len = 1 then
--         result := i_data(i_data'left);
--         result_1 := i_data(i_data'left);
--       elsif len = 2 then
--         result := i_data(i_data'left) xor i_data(i_data'right);
--         result_1 := i_data(i_data'left) xor i_data(i_data'right);
--         -- take bit 0 for both and for r(0) bit 2 and r(1) bit 1
--         --elsif len = 3 then
--         --  result := i_data(i_data'left) xor i_data(i_data'right);
--         --  result_2 := i_data(i_data'left-1) xor i_data(i_data'right);
--         -- r(0) = 0 ^ 2, r(1) = 1 ^ 3
--         --elsif len = 4 then
--         --  result := i_data(i_data'left-1) xor i_data(i_data'right);
--         --  result_2 := i_data(i_data'left) xor i_data(i_data'right+1);
--       else
--         result := xor_tree_func(i_data0);
--         result_1 := xor_tree_func(i_data1);
--       end if;
--     end if;

    result2(1) := result1;
    result2(0) := result;
    return result2;

  end xor_tree_func_2;

end par_mem_pkg;
------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- addr_crsbar routes address signals (addrs_from_lsus) and read/write signals
-- (lsu_wr_xs) to correct parallel memory modules. It also takes care of
-- input signal latching and calls parallel memory functions.
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.par_mem_pkg.all;

entity addr_crsbar is
  generic(
    PORTS     : integer := 2;
    LSU_ADDRW : integer := 8;
    CTRLW     : integer := 1;
    PM_FUNC   : par_mem_func := LOW_ORDER
  );
  port(
    addrs_from_lsus : in  std_logic_vector(PORTS*LSU_ADDRW-1 downto 0);
    lsu_wr_xs       : in  std_logic_vector(PORTS-1 downto 0);

    mem_addrs_out   : out std_logic_vector(PORTS*(LSU_ADDRW-CTRLW)-1 downto 0);
    module_nums     : out std_logic_vector(PORTS*CTRLW-1 downto 0);
    lsu_wr_xs_out   : out std_logic_vector(PORTS-1 downto 0);
    mem_wr_xs       : out std_logic_vector(PORTS-1 downto 0);

    ctrl            : in  std_logic_vector(PORTS*CTRLW-1 downto 0);
    in_latch_en     : in  std_logic;

    clk             : in  std_logic;
    rstx            : in  std_logic;
    glock           : in  std_logic;
    lockrq_reg      : in  std_logic
  );
end addr_crsbar;
------------------------------------------------------------------------------
architecture behavioral of addr_crsbar is

  type mem_addr_array is array (PORTS-1 downto 0) of
    std_logic_vector(LSU_ADDRW-CTRLW-1 downto 0);

  type lsu_addr_array is array (PORTS-1 downto 0) of
    std_logic_vector(LSU_ADDRW-1 downto 0);

  type ctrl_array is array (PORTS-1 downto 0) of
    std_logic_vector(CTRLW-1 downto 0);

  signal addrs_in        : lsu_addr_array;
  --signal lsu_wr_xs_in    : std_logic_vector(PORTS-1 downto 0);
  --signal mem_addrs_in    : mem_addr_array;
  signal addrs_latch     : lsu_addr_array;
  signal lsu_wr_xs_latch : std_logic_vector(PORTS-1 downto 0);

  --signal mem_addrs_in_temp : mem_addr_array;

  signal addrs_from_lsus_sig : lsu_addr_array;
  signal ctrl_sig            : ctrl_array;
  signal module_nums_sig     : ctrl_array;
  signal mem_addrs_out_sig   : mem_addr_array;

begin  -- behavioral
------------------------------------------------------------------------------
  addr_crsbar : process(lockrq_reg, addrs_from_lsus_sig, lsu_wr_xs, addrs_latch,
                        lsu_wr_xs_latch, addrs_in, ctrl_sig)
                        --mem_addrs_in, lsu_wr_xs_in,

    variable mem_addrs_in    : mem_addr_array;
    variable lsu_wr_xs_in : std_logic_vector(PORTS-1 downto 0);
    --variable module_nums_sig_temp : ctrl_array;
    --variable mem_addrs_in_temp : mem_addr_array;

  begin
    -- The case statement selects actual input signals directly from the LSUs
    -- or uses the latched values in the case of memory conflict.
    case lockrq_reg is
      when '0'    => addrs_in     <= addrs_from_lsus_sig;
                     lsu_wr_xs_in := lsu_wr_xs;
      when others => addrs_in     <= addrs_latch;
                     lsu_wr_xs_in := lsu_wr_xs_latch;
    end case;

    -- Employing the essential parallel memory functions.
    for i in 0 to PORTS-1 loop
      module_nums_sig(i) <= mem_func(addrs_in(i), LSU_ADDRW, CTRLW, PORTS,
                                      PM_FUNC);
      mem_addrs_in(i)    := addr_func(addrs_in(i), LSU_ADDRW, CTRLW, PORTS,
                                      PM_FUNC);
    end loop;

    -- Crossbar implementations. ctrl-signal is computed in ctrl_unit entity.
    -- The idea for implementing muxes was borrowed from book by M.J.S. Smith,
    -- Application-Specific Integrated Circuits, 1997, p.595., 12.6.3
    -- Multiplexers in VHDL.
    for i in 0 to PORTS-1 loop

      mem_addrs_out_sig(i) <= mem_addrs_in(to_integer(unsigned(ctrl_sig(i))));
      mem_wr_xs(i)     <= lsu_wr_xs_in(to_integer(unsigned(ctrl_sig(i))));
    end loop;

    lsu_wr_xs_out <= lsu_wr_xs_in;

  end process addr_crsbar;

  ----------------------------------------------------------------------------
  -- There is one delay cycle in the locking of the Move core. Thus, in case
  -- of conflict, input signals from LSUs must be saved before they are
  -- overwritten, because they are needed in the conflict resolving. This
  -- process implements latching for addrs_from_lsus and lsu_wr_xs input
  -- signals. Control signal in_latch_en is determined by a state machine
  -- (input_latch_stm) described in cntl_units entity.
  ----------------------------------------------------------------------------
  input_latching : process(clk, rstx)
  begin
    if rstx = '0' then                  -- asynchronous reset (active low)

      for i in 0 to PORTS-1 loop
        addrs_latch(i) <= (others => '0');
      end loop;

      lsu_wr_xs_latch <= (others => '1');

    elsif clk'event and clk = '1' then  -- rising clock edge
      if (glock and (not lockrq_reg)) = '0' then
        if in_latch_en = '1' then
          addrs_latch     <= addrs_from_lsus_sig;
          lsu_wr_xs_latch <= lsu_wr_xs;
        end if;
      end if;
    end if;
  end process input_latching;


  -- Own process for necessary array <-> std_logic_vector conversions.
  array_conversions : process(addrs_from_lsus, mem_addrs_out_sig,
                              module_nums_sig, ctrl)
  begin

    for i in 0 to PORTS-1 loop
      addrs_from_lsus_sig(i) <=
        addrs_from_lsus((i+1)*LSU_ADDRW-1 downto i*LSU_ADDRW);

      mem_addrs_out((i+1)*(LSU_ADDRW-CTRLW)-1 downto i*(LSU_ADDRW-CTRLW)) <=
        mem_addrs_out_sig(i);

      module_nums((i+1)*CTRLW-1 downto i*CTRLW) <=
        module_nums_sig(i);

      ctrl_sig(i) <=
        ctrl((i+1)*CTRLW-1 downto i*CTRLW);
    end loop;

  end process array_conversions;

end behavioral;
------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- wr_crsbar routes signals (st_data_from_lsus) to the correct parallel memory
-- modules. It also takes care of input signal latching.
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.par_mem_pkg.all;

entity wr_crsbar is
  generic(
    PORTS : integer := 2;
    DATAW : integer := 8;
    CTRLW : integer := 1
  );
  port(
    st_data_from_lsus : in  std_logic_vector(PORTS*DATAW-1 downto 0);
    st_data_to_mems   : out std_logic_vector(PORTS*DATAW-1 downto 0);

    ctrl              : in  std_logic_vector(PORTS*CTRLW-1 downto 0);
    in_latch_en       : in  std_logic;

    clk               : in  std_logic;
    rstx              : in  std_logic;
    glock             : in  std_logic;
    lockrq_reg        : in  std_logic
  );
end wr_crsbar;
------------------------------------------------------------------------------
architecture behavioral of wr_crsbar is

  type data_array is array (PORTS-1 downto 0) of
    std_logic_vector(DATAW-1 downto 0);

  type ctrl_array is array (PORTS-1 downto 0) of
    std_logic_vector(CTRLW-1 downto 0);

  signal data_in       : data_array;
  signal st_data_latch : data_array;

  signal st_data_from_lsus_sig : data_array;
  signal st_data_to_mems_sig   : data_array;
  signal ctrl_sig              : ctrl_array;

begin  -- behavioral
------------------------------------------------------------------------------
  wr_crsbar : process(lockrq_reg, st_data_from_lsus_sig, st_data_latch, data_in,
                      ctrl_sig)
  begin
    -- The case statement selects actual input signals directly from the LSUs
    -- or uses the latched values in the case of memory conflict.
    case lockrq_reg is
      when '0'    => data_in <= st_data_from_lsus_sig;
      when others => data_in <= st_data_latch;
    end case;

    -- Crossbar implementations. ctrl-signal is computed in ctrl_unit entity.
    for i in 0 to PORTS-1 loop
      st_data_to_mems_sig(i) <= data_in(to_integer(unsigned(ctrl_sig(i))));
    end loop;

  end process wr_crsbar;

  ----------------------------------------------------------------------------
  -- There is one delay cycle in the locking of the Move core. Thus, in case
  -- of conflict, input signals from LSUs must be saved before they are
  -- overwritten, because they are needed in the conflict resolving. This
  -- process implements latching for st_data_from_lsus input signals. Control
  -- signal in_latch_en is determined by a state machine (input_latch_stm)
  -- described in cntl_units entity.
  ----------------------------------------------------------------------------
  input_latching : process(clk, rstx)
  begin
    if rstx = '0' then                  -- asynchronous reset (active low)
      for i in 0 to PORTS-1 loop
        st_data_latch(i) <= (others => '0');
      end loop;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if (glock and (not lockrq_reg)) = '0' then
        if in_latch_en = '1' then
          st_data_latch <= st_data_from_lsus_sig;
        end if;
      end if;
    end if;
  end process input_latching;


  -- Own process for necessary array <-> std_logic_vector conversions.
  array_conversions : process(st_data_from_lsus, st_data_to_mems_sig, ctrl)
  begin

    for i in 0 to PORTS-1 loop
      st_data_from_lsus_sig(i) <=
        st_data_from_lsus((i+1)*DATAW-1 downto i*DATAW);

      st_data_to_mems((i+1)*DATAW-1 downto i*DATAW) <=
        st_data_to_mems_sig(i);

      ctrl_sig(i) <=
        ctrl((i+1)*CTRLW-1 downto i*CTRLW);
    end loop;

  end process array_conversions;

end behavioral;
------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- rd_crsbar routes memory outputs (ld_data_from_mems) to the correct LSUs. It
-- also takes care of output latching after the crossbar. The latching is
-- needed to save the data when the same memory module is accessed several
-- times during a memory conflict.
------------------------------------------------------------------------------
library IEEE;
use IEEE.numeric_std.all;
use IEEE.std_logic_1164.all;
use work.par_mem_pkg.all;

entity rd_crsbar is
  generic(
    PORTS : integer := 2;
    DATAW : integer := 8;
    CTRLW : integer := 1
  );
  port(
    ld_data_from_mems  : in  std_logic_vector(PORTS*DATAW-1 downto 0);
    ld_data_to_lsus    : out std_logic_vector(PORTS*DATAW-1 downto 0);

    ctrl               : in  std_logic_vector(PORTS*CTRLW-1 downto 0);
    rd_crsbar_latch_en : in  std_logic_vector(PORTS-1 downto 0);
    rd_crsbar_mux_en   : in  std_logic_vector(PORTS-1 downto 0);

    clk        : in  std_logic;
    rstx       : in  std_logic;
    glock      : in  std_logic;
    lockrq_reg : in  std_logic
  );
end rd_crsbar;
------------------------------------------------------------------------------
architecture behavioral of rd_crsbar is

  type data_array is array (PORTS-1 downto 0) of
    std_logic_vector(DATAW-1 downto 0);

  type ctrl_array is array (PORTS-1 downto 0) of
    std_logic_vector(CTRLW-1 downto 0);

  signal crsbar_out       : data_array;
  signal crsbar_out_latch : data_array;

  signal ld_data_from_mems_sig : data_array;
  signal ld_data_to_lsus_sig   : data_array;
  signal ctrl_sig         : ctrl_array;

begin  -- behavioral
------------------------------------------------------------------------------
  rd_crsbar : process(ld_data_from_mems_sig, ctrl_sig, crsbar_out, crsbar_out_latch,
                      rd_crsbar_mux_en)
  begin
    -- Crossbar function. ctrl signal is computed in ctrl_unit entity.
    for i in 0 to PORTS-1 loop
      crsbar_out(i) <= ld_data_from_mems_sig(to_integer(unsigned(ctrl_sig(i))));
    end loop;

    -- Selecting the output. The case statement selects actual output signals
    -- directly from the memory modules or uses the latched values. Control
    -- signal rd_crsbar_mux_en(i) is determined by the state machine
    -- (rd_out_mux_stm(i)) described in cntl_unit entity.
    for i in 0 to PORTS-1 loop
      case rd_crsbar_mux_en(i) is
        when  '0'   => ld_data_to_lsus_sig(i) <= crsbar_out(i);
        when others => ld_data_to_lsus_sig(i) <= crsbar_out_latch(i);
      end case;
    end loop;

  end process rd_crsbar;

  ----------------------------------------------------------------------------
  -- This process implements latching for read crossbar output data
  -- (crsbar_out(i)). Control signal rd_crsbar_latch_en(i) is determined by a
  -- state machine (rd_out_latch_stm(i)) described in cntl_unit(i) entity.
  ----------------------------------------------------------------------------
  output_latching : process(clk, rstx)
  begin
    if rstx = '0' then                    -- asynchronous reset (active low)
      for i in 0 to PORTS-1 loop
        crsbar_out_latch(i) <= (others => '0');
      end loop;
    elsif clk'event and clk = '1' then    -- rising clock edge
      if (glock and (not lockrq_reg)) = '0' then
        for i in 0 to PORTS-1 loop
          if rd_crsbar_latch_en(i) = '1' then
            crsbar_out_latch(i) <= crsbar_out(i);
          end if;
        end loop;
      end if;
    end if;
  end process output_latching;


  -- Own process for necessary array <-> std_logic_vector conversions.
  array_conversions : process(ld_data_from_mems, ld_data_to_lsus_sig, ctrl)
  begin

    for i in 0 to PORTS-1 loop
      ld_data_from_mems_sig(i) <=
        ld_data_from_mems((i+1)*DATAW-1 downto i*DATAW);

      ld_data_to_lsus((i+1)*DATAW-1 downto i*DATAW) <=
        ld_data_to_lsus_sig(i);

      ctrl_sig(i) <=
        ctrl((i+1)*CTRLW-1 downto i*CTRLW);
    end loop;

  end process array_conversions;

end behavioral;
------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Control circuit for a single LSU and memory module. PORTS ctrl_units are
-- employed and connected together in ctrl_unit entity.
------------------------------------------------------------------------------
library IEEE, work;
use IEEE.std_logic_1164.all;
use WORK.par_mem_pkg.all;

entity ctrl_unit is
  generic(
    PORTS : integer := 2;
    CTRLW : integer := 1
  );
  port(
    lsu_en_x             : in  std_logic; -- Corresponding LSU enable,
                                          -- active low.
    lsu_wr_x_in          : in  std_logic; -- LSU R/W, write is active low.
    mem_wr_x             : in  std_logic; -- MemMod R/W, write is active low.
    module_num           : in  std_logic_vector(CTRLW-1 downto 0);
    t_ctrl_matrix_row_in : in  std_logic_vector(PORTS-1 downto 0);
                                          -- The row from the transposed
                                          -- control matrix.
    curr_lsu_en_bit      : in  std_logic; -- This vector tells, which LSU's
                                          -- requests are served at the
                                          -- moment.
    in_latch_en          : in std_logic;

    mem_en_x             : out std_logic; -- Corresponding memory module
                                          -- enable, active low.
    more_to_come         : out std_logic; -- More accesses for this
                                          -- corresponding memory module are
                                          -- coming.
    rd_crsbar_latch_en   : out std_logic; -- Enable for rd_crsbar out latch.
    rd_crsbar_mux_en     : out std_logic; -- Enable for rd_crsbar out mux.

    addr_mux_ctrl        : out std_logic_vector(CTRLW-1 downto 0);
                                          -- Corresponding addr mux control.
    rd_mux_ctrl          : out std_logic_vector(CTRLW-1 downto 0);
                                          -- Corresponding read mux control.
    wr_mux_ctrl          : out std_logic_vector(CTRLW-1 downto 0);
                                          -- Corresponding write mux control.
    curr_lsu             : out std_logic_vector(PORTS-1 downto 0);
                                          -- Single '1' bit in this vector
                                          -- tells, which LSU is currently
                                          -- served by this ctrl_unit.
    ctrl_matrix_row_out  : out std_logic_vector(PORTS-1 downto 0);
                                          -- Submit the control matrix row for
                                          -- transposing.

    clk                  : in  std_logic;
    rstx                 : in  std_logic;
    glock                : in  std_logic;
    lockrq_reg           : in  std_logic
  );
end ctrl_unit;
------------------------------------------------------------------------------
architecture behavioral of ctrl_unit is

  type state_latch is (no_rd_latch, rd_latch);
  type state_mux is (no_rd_mux, rd_mux);

  signal lsu_en_x_latch            : std_logic;
  signal lsu_en_x_in               : std_logic;
  signal rd_mux_ctrl_latch_reg     : std_logic_vector(CTRLW-1 downto 0);
  signal ctrl_row_mux_out          : std_logic_vector(PORTS-1 downto 0);
  signal new_t_ctrl_matrix_row     : std_logic_vector(PORTS-1 downto 0);
  signal new_t_ctrl_matrix_row_reg : std_logic_vector(PORTS-1 downto 0);
  signal pri_enc_out               : std_logic_vector(2+CTRLW-1 downto 0);
  signal dec_out                   : std_logic_vector(PORTS-1 downto 0);

  signal latch_en                  : std_logic;
  signal curr_lsu_en_bit_reg       : std_logic;
  signal lsu_wr_x_in_reg           : std_logic;
  signal rd_mux_en                 : std_logic;
  signal rd_mux_unen               : std_logic;

  signal curr_st, next_st               : state_latch;
  signal curr_st_rd_mux, next_st_rd_mux : state_mux;

begin  -- behavioral
------------------------------------------------------------------------------
  -- Main control process.
  ----------------------------------------------------------------------------
  main_cntrl : process(lsu_en_x, lsu_en_x_latch, lockrq_reg, lsu_en_x_in,
                       mem_wr_x, module_num, t_ctrl_matrix_row_in,
                       ctrl_row_mux_out, new_t_ctrl_matrix_row_reg,
                       pri_enc_out, dec_out, rd_mux_ctrl_latch_reg)
  begin
    -- The case statement selects actual input signals directly from the LSUs
    -- or uses the latched values in the case of memory conflict.
    case lockrq_reg is
      when '0'    => lsu_en_x_in <= lsu_en_x;
      when others => lsu_en_x_in <= lsu_en_x_latch;
    end case;

    ctrl_matrix_row_out <= dec_func(lsu_en_x_in, module_num, PORTS);

    -- This case statement basically enables or disables memory conflict
    -- resolving chain. When there are no conflicts, new input data is
    -- accepted. The selected data is forwarded (after the case) to the
    -- priority encoder.
    case lockrq_reg is
      when '0'    => ctrl_row_mux_out <= t_ctrl_matrix_row_in;
      when others => ctrl_row_mux_out <= new_t_ctrl_matrix_row_reg;
    end case;

    pri_enc_out   <= pri_enc_func(ctrl_row_mux_out, CTRLW, PORTS);
    dec_out       <=
      dec_func(pri_enc_out(1), pri_enc_out(CTRLW+2-1 downto 2), PORTS);
    more_to_come  <= pri_enc_out(0);
    mem_en_x      <= pri_enc_out(1);
    addr_mux_ctrl <= pri_enc_out(CTRLW+2-1 downto 2);

    -- wr_mux_ctrl signal is returned to zero vector when wr_crsbar is not
    -- needed for this ctrl_unit.
    case mem_wr_x is
      when '0'    => wr_mux_ctrl <= pri_enc_out(CTRLW+2-1 downto 2);
      when others => wr_mux_ctrl <= (others => '0');
    end case;

    -- Removing the solved LSU request from the ctrl_matrix_row.
    -- t_ctrl_matrix_row contains the LSU requests to be solved for a single
    -- parallel memory access. These LSU requests are directed for the
    -- corresponding memory module assigned for this ctrl_unit.
    new_t_ctrl_matrix_row <= ctrl_row_mux_out xor dec_out;

    -- Signal rd_mux_ctrl is delayed, because one clock cycle delay comes
    -- from the memory access. This control signal is also latched to reduce
    -- unnecessary switching in a hope of some power saving. The same latching
    -- method could be considered also for addr_crsbar and wr_crsbar. However,
    -- in these cases additional registers might be needed.
    rd_mux_ctrl           <= rd_mux_ctrl_latch_reg;

    curr_lsu              <= dec_out;

  end process main_cntrl;

  ----------------------------------------------------------------------------
  -- Generation of registers for delays (and pipelining) (_reg) and latches
  -- (_latch).
  ----------------------------------------------------------------------------
  main_cntrl_synch : process(clk, rstx)
  begin
    if rstx = '0' then                    -- asynchronous reset (active low)
      lsu_en_x_latch            <= '1';
      new_t_ctrl_matrix_row_reg <= (others => '0');
      rd_mux_ctrl_latch_reg     <= (others => '0');
      curr_lsu_en_bit_reg       <= '0';
      lsu_wr_x_in_reg           <= '1';

    elsif clk'event and clk = '1' then    -- rising clock edge
      if (glock and (not lockrq_reg)) = '0' then

        -- Latching of input signal lsu_en_x.
        if in_latch_en = '1' then
          lsu_en_x_latch <= lsu_en_x;
        end if;

        -- Latching rd_mux_ctrl signal to reduce useless switching in crsbar.
        if ((lsu_wr_x_in and curr_lsu_en_bit) = '1') then
          rd_mux_ctrl_latch_reg <= module_num;
        end if;

        new_t_ctrl_matrix_row_reg <= new_t_ctrl_matrix_row;
        curr_lsu_en_bit_reg       <= curr_lsu_en_bit;
        lsu_wr_x_in_reg           <= lsu_wr_x_in;
      end if;
    end if;
  end process main_cntrl_synch;

  ----------------------------------------------------------------------------
  -- State machine rd_out_latch_stm(i) for rd_crsbar output_latching control.
  -- Every ctrl_unit has its own state machine. Thus, there are PORTS
  -- rd_out_latch_stm state machines.
  ----------------------------------------------------------------------------
  rd_out_latch_stm_combin : process(curr_st, lockrq_reg, latch_en,
                                   curr_lsu_en_bit_reg, lsu_wr_x_in_reg)
  begin
    latch_en <= lockrq_reg and curr_lsu_en_bit_reg and lsu_wr_x_in_reg;

    case curr_st is
      when no_rd_latch =>

        if latch_en = '1' then
          rd_crsbar_latch_en <= '1';
          next_st <= rd_latch;
        else
          rd_crsbar_latch_en <= '0';
          next_st <= no_rd_latch;
        end if;

      when rd_latch =>
        rd_crsbar_latch_en <= '0';
        next_st <= no_rd_latch;

      when others => null;
    end case;

  end process rd_out_latch_stm_combin;

  ----------------------------------------------------------------------------
  rd_out_latch_stm_synch : process(clk, rstx)
  begin
    if rstx = '0' then                    -- asynchronous reset (active low)
      curr_st <= no_rd_latch;
    elsif clk'event and clk = '1' then    -- rising clock edge
      if (glock and (not lockrq_reg)) = '0' then
        curr_st <= next_st;
      end if;
    end if;
  end process rd_out_latch_stm_synch;

  ----------------------------------------------------------------------------
  -- State machine rd_out_mux_stm(i) for rd_crsbar output mux control. The
  -- state is changed only when needed. Every ctrl_unit has its own state
  -- machine. Thus, there are PORTS rd_out_mux_stm state machines.
  ----------------------------------------------------------------------------
  rd_out_mux_stm_combin : process(curr_st_rd_mux, lockrq_reg,
                                 curr_lsu_en_bit_reg, lsu_wr_x_in_reg,
                                 rd_mux_en, rd_mux_unen)
  begin
    rd_mux_en   <= lockrq_reg and curr_lsu_en_bit_reg and lsu_wr_x_in_reg;
    rd_mux_unen <=
      (not(lockrq_reg)) and curr_lsu_en_bit_reg and lsu_wr_x_in_reg;

    case curr_st_rd_mux is
      when no_rd_mux =>
        rd_crsbar_mux_en <= '0';

        if rd_mux_en = '1' then
          next_st_rd_mux <= rd_mux;
        else
          next_st_rd_mux <= no_rd_mux;
        end if;

      when rd_mux =>
        rd_crsbar_mux_en <= '1';

        if rd_mux_unen = '1' then
          rd_crsbar_mux_en <= '0';
          next_st_rd_mux   <= no_rd_mux;
        else
          next_st_rd_mux   <= rd_mux;
        end if;

      when others => null;
    end case;
  end process rd_out_mux_stm_combin;

  ----------------------------------------------------------------------------
  rd_out_mux_stm_synch : process(clk, rstx)
  begin
    if rstx = '0' then                    -- asynchronous reset (active low)
      curr_st_rd_mux <= no_rd_mux;
    elsif clk'event and clk = '1' then    -- rising clock edge
      if (glock and (not lockrq_reg)) = '0' then
        curr_st_rd_mux <= next_st_rd_mux;
      end if;
    end if;
  end process rd_out_mux_stm_synch;

end behavioral;
------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Entity ctrl_units contains PORTS ctrl_unit entitys and provides various
-- control signals for crossbars, other muxes, and latches in par_mem_logic.
-- It also provides memory module enables and lockrq signal.
------------------------------------------------------------------------------
library IEEE, work;
use IEEE.std_logic_1164.all;
use WORK.par_mem_pkg.all;

entity ctrl_units is
  generic(
    PORTS : integer := 2;
    CTRLW : integer := 1
  );
  port(
    lsu_en_xs          : in  std_logic_vector(PORTS-1 downto 0);
    lsu_wr_xs_in       : in  std_logic_vector(PORTS-1 downto 0);
    mem_wr_xs          : in  std_logic_vector(PORTS-1 downto 0);
    module_nums        : in  std_logic_vector(PORTS*CTRLW-1 downto 0);

    mem_en_xs          : out std_logic_vector(PORTS-1 downto 0);
    addr_mux_ctrls     : out std_logic_vector(PORTS*CTRLW-1 downto 0);
    rd_mux_ctrls       : out std_logic_vector(PORTS*CTRLW-1 downto 0);
    in_latch_en        : out std_logic;
    rd_crsbar_latch_en : out std_logic_vector(PORTS-1 downto 0);
    rd_crsbar_mux_en   : out std_logic_vector(PORTS-1 downto 0);
    wr_mux_ctrls       : out std_logic_vector(PORTS*CTRLW-1 downto 0);

    clk                : in  std_logic;
    rstx               : in  std_logic;
    glock              : in  std_logic;
    -- lockrq             : out std_logic;
    lockrq_reg         : out std_logic
  );
end ctrl_units;
------------------------------------------------------------------------------
architecture behavioral of ctrl_units is

  type bus_array is array (PORTS-1 downto 0) of
    std_logic_vector(PORTS-1 downto 0);

  type state is (no_input_latch, input_latch);

  signal curr_lsu_en_bits     : std_logic_vector(PORTS-1 downto 0);
  signal curr_lsus            : bus_array;
  signal curr_lsus_gathered   : bus_array;
  signal more_to_come         : std_logic_vector(PORTS-1 downto 0);
  signal mem_en_xs_sig        : std_logic_vector(PORTS-1 downto 0);
  signal t_ctrl_matrix_row_in : bus_array;
  signal ctrl_matrix_row_out  : bus_array;

  signal lockrq_sig           : std_logic;
  signal lockrq_reg_sig       : std_logic;

  signal in_latch_en_sig      : std_logic;
  signal curr_st, next_st     : state;

------------------------------------------------------------------------------
  component ctrl_unit
    generic(
      PORTS : integer := 2;
      CTRLW : integer := 1
    );
    port(
      lsu_en_x             : in  std_logic;
      lsu_wr_x_in          : in  std_logic;
      mem_wr_x             : in  std_logic;
      module_num           : in  std_logic_vector(CTRLW-1 downto 0);
      t_ctrl_matrix_row_in : in  std_logic_vector(PORTS-1 downto 0);
      curr_lsu_en_bit      : in  std_logic;
      in_latch_en          : in  std_logic;

      mem_en_x             : out std_logic;
      more_to_come         : out std_logic;
      rd_crsbar_latch_en   : out std_logic;
      rd_crsbar_mux_en     : out std_logic;

      addr_mux_ctrl        : out std_logic_vector(CTRLW-1 downto 0);
      rd_mux_ctrl          : out std_logic_vector(CTRLW-1 downto 0);
      wr_mux_ctrl          : out std_logic_vector(CTRLW-1 downto 0);
      curr_lsu             : out std_logic_vector(PORTS-1 downto 0);
      ctrl_matrix_row_out  : out std_logic_vector(PORTS-1 downto 0);

      clk                  : in  std_logic;
      rstx                 : in  std_logic;
      glock                : in  std_logic;
      lockrq_reg           : in  std_logic);
  end component;

begin  -- behavioral
------------------------------------------------------------------------------
  -- A single ctrl_units entity contains PORTS cntrl_unit entitys.
  gen_ctrl_units : for i in 0 to PORTS-1 generate

    ctrl_uniti : ctrl_unit
      generic map(
        PORTS => PORTS,
        CTRLW => CTRLW)
      port map(
        lsu_en_x             => lsu_en_xs(i),
        lsu_wr_x_in          => lsu_wr_xs_in(i),
        mem_wr_x             => mem_wr_xs(i),
        module_num           => module_nums((i+1)*CTRLW-1 downto i*CTRLW),
        t_ctrl_matrix_row_in => t_ctrl_matrix_row_in(i),
        curr_lsu_en_bit      => curr_lsu_en_bits(i),
        in_latch_en          => in_latch_en_sig,

        mem_en_x             => mem_en_xs_sig(i),
        more_to_come         => more_to_come(i),
        rd_crsbar_latch_en   => rd_crsbar_latch_en(i),
        rd_crsbar_mux_en     => rd_crsbar_mux_en(i),

        addr_mux_ctrl        => addr_mux_ctrls((i+1)*CTRLW-1 downto i*CTRLW),
        rd_mux_ctrl          => rd_mux_ctrls((i+1)*CTRLW-1 downto i*CTRLW),
        wr_mux_ctrl          => wr_mux_ctrls((i+1)*CTRLW-1 downto i*CTRLW),
        curr_lsu             => curr_lsus(i),
        ctrl_matrix_row_out  => ctrl_matrix_row_out(i),

        clk                  => clk,
        rstx                 => rstx,
        glock                => glock,
        lockrq_reg           => lockrq_reg_sig);
  end generate;

  -- Signal lockrq is generated from more_to_come by an OR tree.
  lockrq_sig  <= or_tree_func(more_to_come);

  in_latch_en <= in_latch_en_sig;
  -- lockrq      <= lockrq_sig;
  lockrq_reg  <= lockrq_reg_sig;

  ----------------------------------------------------------------------------
  -- This process contains miscellaneous tasks including control binary
  -- matrix transposition, merging of correct curr_lsu signals with OR trees,
  -- and the control of memory module enables.
  ----------------------------------------------------------------------------
  misc_combin : process(ctrl_matrix_row_out, curr_lsus, curr_lsus_gathered,
                        glock, lockrq_reg_sig, mem_en_xs_sig)
  begin
    for i in 0 to PORTS-1 loop
      for j in 0 to PORTS-1 loop
        -- Transposition of the control binary matrix.
        t_ctrl_matrix_row_in(i)(j) <= ctrl_matrix_row_out(j)(i);

        -- Reordering curr_lsu signals.
        curr_lsus_gathered(i)(j)   <= curr_lsus(j)(i);
      end loop; -- j

      -- Generates PORTS separate OR-trees for merging correct curr_lsu
      -- signals (curr_lsus_gathered(i)).
      curr_lsu_en_bits(i) <= or_tree_func(curr_lsus_gathered(i));
    end loop; -- i

    --  To preserve correct pipeline locking, it is important to disable
    --  memory modules during active glock caused by someone else than
    --  par_mem_logic. Then LSU input registers, memory modules, and
    --  LSU output registers (fu_ld_st_always_3) are locked and no data is
    --  lost during global lock.
    if (glock and (not lockrq_reg_sig)) = '0' then
      mem_en_xs <= mem_en_xs_sig;
    else
      mem_en_xs <= (others => '1');   -- Active low.
    end if;

  end process misc_combin;

  ----------------------------------------------------------------------------
  -- State machine input_latch_stm controls input signal latches and saves
  -- correct input data (just before it is overwritten) when conflicting
  -- access is detected. This is a single state machine and control for all
  -- the data coming from all the LSUs.
  ----------------------------------------------------------------------------
  input_latch_stm_combin : process(lockrq_sig, curr_st)
  begin
    case curr_st is
      when no_input_latch =>

        if lockrq_sig = '1' then
          in_latch_en_sig <= '1';
          next_st         <= input_latch;
        else
          in_latch_en_sig <= '0';
          next_st         <= no_input_latch;
        end if;

      when input_latch =>
        in_latch_en_sig <= '0';

        if lockrq_sig = '1' then
          next_st <= input_latch;
        else
          next_st <= no_input_latch;
        end if;

      when others => null;
    end case;
  end process input_latch_stm_combin;

  ----------------------------------------------------------------------------
  input_latch_stm_synch : process(clk, rstx)
  begin
    if rstx = '0' then                    -- asynchronous reset (active low)
      curr_st <= no_input_latch;
    elsif clk'event and clk = '1' then    -- rising clock edge
      if (glock and (not lockrq_reg_sig)) = '0' then
        curr_st <= next_st;
      end if;
    end if;
  end process input_latch_stm_synch;

  ----------------------------------------------------------------------------
  -- Register lock request lockrq. Registered lock request lockrq_reg is used
  -- also internally in par_mem_logic for numerous control purposes (i.e.,
  -- controlling for muxes and state machines).
  ----------------------------------------------------------------------------
  lockrq_reg_gen : process(clk, rstx)
  begin
    if rstx = '0' then                    -- asynchronous reset (active low)
      lockrq_reg_sig <= '0';
    elsif clk'event and clk = '1' then    -- rising clock edge
      if (glock and (not lockrq_reg_sig)) = '0' then
        lockrq_reg_sig <= lockrq_sig;
      end if;
    end if;
  end process lockrq_reg_gen;

end behavioral;
------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Top-level entity par_mem_logic for parallel memory architecture.
------------------------------------------------------------------------------

library IEEE, work;
use IEEE.std_logic_1164.all;
use WORK.par_mem_pkg.all;

entity par_mem_logic is
  generic(
    PORTS     : integer := 2;
    LSU_ADDRW : integer := 8; -- Likely equals to DMEMADDRWIDTH.
    CTRLW     : integer := 1; -- Number of bits required for PORTS.
    DATAW     : integer := 8; -- Likely equals to DMEMDATAWIDTH in
                              -- globals_pkg.vhdl
    PM_FUNC   : par_mem_func := LOW_ORDER
  );
  port(
    -- Connections for the side of the LSUs.
    lsu_en_xs         : in  std_logic_vector(PORTS-1 downto 0);
    lsu_wr_xs         : in  std_logic_vector(PORTS-1 downto 0);
    addrs_from_lsus   : in  std_logic_vector(PORTS*LSU_ADDRW-1 downto 0);
    st_data_from_lsus : in  std_logic_vector(PORTS*DATAW-1 downto 0);
    ld_data_to_lsus   : out std_logic_vector(PORTS*DATAW-1 downto 0);

    -- Connections for the side of the memory modules.
    mem_en_xs         : out std_logic_vector(PORTS-1 downto 0);
    mem_wr_xs         : out std_logic_vector(PORTS-1 downto 0);
    addrs_to_mems : out std_logic_vector(PORTS*(LSU_ADDRW-CTRLW)-1 downto 0);
    st_data_to_mems   : out std_logic_vector(PORTS*DATAW-1 downto 0);
    ld_data_from_mems : in  std_logic_vector(PORTS*DATAW-1 downto 0);

    -- Other important signals.
    clk               : in  std_logic;
    rstx              : in  std_logic;
    glock             : in  std_logic;
    lockrq            : out std_logic
  );
end par_mem_logic;
------------------------------------------------------------------------------
architecture structural_reg of par_mem_logic is

  constant MEM_ADDRW : integer := LSU_ADDRW - CTRLW;

  signal lsu_wr_xs_in       : std_logic_vector(PORTS-1 downto 0);
  signal module_nums        : std_logic_vector(PORTS*CTRLW-1 downto 0);
  signal addr_mux_ctrls     : std_logic_vector(PORTS*CTRLW-1 downto 0);
  signal mem_wr_xs_sig      : std_logic_vector(PORTS-1 downto 0);
  signal rd_mux_ctrls       : std_logic_vector(PORTS*CTRLW-1 downto 0);
  signal rd_crsbar_latch_en : std_logic_vector(PORTS-1 downto 0);
  signal rd_crsbar_mux_en   : std_logic_vector(PORTS-1 downto 0);
  signal wr_mux_ctrls       : std_logic_vector(PORTS*CTRLW-1 downto 0);
  signal in_latch_en        : std_logic;
  signal lockrq_reg         : std_logic;

------------------------------------------------------------------------------
  component ctrl_units
    generic(
      PORTS : integer := 2;
      CTRLW : integer := 1
    );
    port(
      lsu_en_xs          : in  std_logic_vector(PORTS-1 downto 0);
      lsu_wr_xs_in       : in  std_logic_vector(PORTS-1 downto 0);
      mem_wr_xs          : in  std_logic_vector(PORTS-1 downto 0);
      module_nums        : in  std_logic_vector(PORTS*CTRLW-1 downto 0);

      mem_en_xs          : out std_logic_vector(PORTS-1 downto 0);
      addr_mux_ctrls     : out std_logic_vector(PORTS*CTRLW-1 downto 0);
      rd_mux_ctrls       : out std_logic_vector(PORTS*CTRLW-1 downto 0);
      in_latch_en        : out std_logic;
      rd_crsbar_latch_en : out std_logic_vector(PORTS-1 downto 0);
      rd_crsbar_mux_en   : out std_logic_vector(PORTS-1 downto 0);
      wr_mux_ctrls       : out std_logic_vector(PORTS*CTRLW-1 downto 0);

      clk                : in  std_logic;
      rstx               : in  std_logic;
      glock              : in  std_logic;
      -- lockrq             : out std_logic;
      lockrq_reg         : out std_logic);
  end component;

  component addr_crsbar
    generic(
      PORTS     : integer := 2;
      LSU_ADDRW : integer := 8;
      CTRLW     : integer := 1;
      PM_FUNC   : par_mem_func := LOW_ORDER);
    port(
      addrs_from_lsus : in  std_logic_vector(PORTS*LSU_ADDRW-1 downto 0);
      lsu_wr_xs       : in  std_logic_vector(PORTS-1 downto 0);

      mem_addrs_out   : out std_logic_vector(PORTS*MEM_ADDRW-1 downto 0);
      module_nums     : out std_logic_vector(PORTS*CTRLW-1 downto 0);
      lsu_wr_xs_out   : out std_logic_vector(PORTS-1 downto 0);
      mem_wr_xs       : out std_logic_vector(PORTS-1 downto 0);

      ctrl            : in  std_logic_vector(PORTS*CTRLW-1 downto 0);
      in_latch_en     : in  std_logic;

      clk             : in  std_logic;
      rstx            : in  std_logic;
      glock           : in  std_logic;
      lockrq_reg      : in  std_logic);
  end component;

  component wr_crsbar
    generic(
      PORTS : integer := 2;
      DATAW : integer := 8;
      CTRLW : integer := 1);
    port(
      st_data_from_lsus : in  std_logic_vector(PORTS*DATAW-1 downto 0);
      st_data_to_mems   : out std_logic_vector(PORTS*DATAW-1 downto 0);

      ctrl              : in  std_logic_vector(PORTS*CTRLW-1 downto 0);
      in_latch_en       : in  std_logic;

      clk               : in  std_logic;
      rstx              : in  std_logic;
      glock             : in  std_logic;
      lockrq_reg        : in  std_logic);
  end component;

  component rd_crsbar
    generic(
      PORTS : integer := 2;
      DATAW : integer := 8;
      CTRLW : integer := 1);
    port(
      ld_data_from_mems  : in  std_logic_vector(PORTS*DATAW-1 downto 0);
      ld_data_to_lsus    : out std_logic_vector(PORTS*DATAW-1 downto 0);

      ctrl               : in  std_logic_vector(PORTS*CTRLW-1 downto 0);
      rd_crsbar_latch_en : in  std_logic_vector(PORTS-1 downto 0);
      rd_crsbar_mux_en   : in  std_logic_vector(PORTS-1 downto 0);

      clk                : in  std_logic;
      rstx               : in  std_logic;
      glock              : in  std_logic;
      lockrq_reg         : in  std_logic);
  end component;

begin  -- structural_reg
------------------------------------------------------------------------------
  ctrl_units_0 : ctrl_units
    generic map(
      PORTS => PORTS,
      CTRLW => CTRLW)
    port map(
      lsu_en_xs          => lsu_en_xs,
      lsu_wr_xs_in       => lsu_wr_xs_in,
      mem_wr_xs          => mem_wr_xs_sig,
      module_nums        => module_nums,

      mem_en_xs          => mem_en_xs,
      addr_mux_ctrls     => addr_mux_ctrls,
      rd_mux_ctrls       => rd_mux_ctrls,
      in_latch_en        => in_latch_en,
      rd_crsbar_latch_en => rd_crsbar_latch_en,
      rd_crsbar_mux_en   => rd_crsbar_mux_en,
      wr_mux_ctrls       => wr_mux_ctrls,

      clk                => clk,
      rstx               => rstx,
      glock              => glock,
      -- lockrq             => lockrq,
      lockrq_reg         => lockrq_reg);

  addr_crsbar_0 : addr_crsbar
    generic map(
      PORTS     => PORTS,
      LSU_ADDRW => LSU_ADDRW,
      CTRLW     => CTRLW,
      PM_FUNC   => PM_FUNC)
    port map(
      addrs_from_lsus => addrs_from_lsus,
      lsu_wr_xs       => lsu_wr_xs,

      mem_addrs_out   => addrs_to_mems,
      module_nums     => module_nums,
      lsu_wr_xs_out   => lsu_wr_xs_in,
      mem_wr_xs       => mem_wr_xs_sig,

      ctrl            => addr_mux_ctrls,
      in_latch_en     => in_latch_en,

      clk             => clk,
      rstx            => rstx,
      glock           => glock,
      lockrq_reg      => lockrq_reg);

  wr_crsbar_0 : wr_crsbar
    generic map(
      PORTS => PORTS,
      DATAW => DATAW,
      CTRLW => CTRLW)
    port map(
      st_data_from_lsus => st_data_from_lsus,
      st_data_to_mems   => st_data_to_mems,

      ctrl              => wr_mux_ctrls,
      in_latch_en       => in_latch_en,

      clk               => clk,
      rstx              => rstx,
      glock             => glock,
      lockrq_reg        => lockrq_reg);

  rd_crsbar_0 : rd_crsbar
    generic map(
      PORTS => PORTS,
      DATAW => DATAW,
      CTRLW => CTRLW)
    port map(
      ld_data_from_mems  => ld_data_from_mems,
      ld_data_to_lsus    => ld_data_to_lsus,

      ctrl               => rd_mux_ctrls,
      rd_crsbar_latch_en => rd_crsbar_latch_en,
      rd_crsbar_mux_en   => rd_crsbar_mux_en,

      clk                => clk,
      rstx               => rstx,
      glock              => glock,
      lockrq_reg         => lockrq_reg);

  mem_wr_xs <= mem_wr_xs_sig;
  lockrq    <= lockrq_reg;

end structural_reg;
------------------------------------------------------------------------------
------------------------------------------------------------------------------
