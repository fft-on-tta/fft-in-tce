-- Convert 'port_cnt' parallel outputs into serial data stream. Loading the
-- parallel data is enabled by the 'en' signal
--
-- Converts following stream:
--   a c e g ... MSB
--   b d f h ... LSB
-- into
--   a b c d e f g h

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity par_ser_conv is
    generic (
        portw    : integer := 32;
        port_cnt : integer := 2);
    port (
        data_in  : in  std_logic_vector(port_cnt*portw-1 downto 0);
        data_out : out std_logic_vector(portw-1 downto 0);
        load     : in  std_logic;  -- active low
        clk      : in  std_logic;
        rst_x    : in  std_logic;  -- active low
        glock    : in  std_logic); -- active high
end par_ser_conv;

architecture Behavioral of par_ser_conv is

    type reg_array is array (integer range <>) of
                        std_logic_vector(portw-1 downto 0);

    signal inp_reg : std_logic_vector(data_in'high-portw downto 0);
    signal inputs  : reg_array(0 to port_cnt-1);
    signal cnt     : integer range 0 to port_cnt-1;
    signal en_reg  : std_logic;

begin -- ser_par_conv

    -- Counter 0 to port_cnt-1; sequential
    counter : process (clk, rst_x)
        variable i : integer;
    begin -- counter_enable
        if rst_x = '0' then
            cnt <= 0;
        elsif rising_edge(clk) then
            if (glock = '0') and (en_reg = '0' or load = '0') then
                if cnt >= port_cnt-1 then
                    cnt <= 0;
                else
                    cnt <= cnt + 1;
                end if;
            end if;
        end if;
    end process counter;

    -- Input registers (there is port_cnt-1 registers); sequential
    regs : process (clk, rst_x)
    begin -- regs
        if rst_x = '0' then
            inp_reg <= (others => '0');
            en_reg <= '1';
        elsif rising_edge(clk) then
            if (glock = '0') and (load = '0') then
                if cnt = 0 then
                    -- load data_in without first number (MSB part)
                    inp_reg <= data_in(data_in'high-portw downto 0);
                    -- enable register
                    en_reg <= load;
                end if;
            end if;
        end if;
    end process regs;

    -- Convert long signal into array; combinatorial
    in_break_apart : process (inp_reg, data_in)
        variable high_idx : integer;
    begin -- in_break_apart
        inputs(0) <= data_in(data_in'high downto data_in'length-portw);
        for i in 1 to port_cnt-1 loop
            high_idx := data_in'length-i*portw;
            inputs(i) <= inp_reg(high_idx-1 downto high_idx-portw);
        end loop;
    end process in_break_apart;

    -- Output stream
    data_out <= inputs(cnt);

end Behavioral;
