-- Convert serial data stream into 'port_cnt' parallel outputs, outputed every
-- 'port_cnt'th cycle.
--
-- Converts following stream:
--   a b c d e f g h
-- into
--   a c e g ... MSB
--   b d f h ... LSB

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ser_par_conv is
    generic (
        portw    : integer := 32;
        port_cnt : integer := 2);
    port (
        data_in  : in  std_logic_vector(portw-1 downto 0);
        data_out : out std_logic_vector(port_cnt*portw-1 downto 0);
        en       : in  std_logic;  -- active low
        en_out   : out std_logic_vector(port_cnt-1 downto 0);
        clk      : in  std_logic;
        rst_x    : in  std_logic;  -- active low
        glock    : in  std_logic); -- active high
end ser_par_conv;

architecture Behavioral of ser_par_conv is

    constant nulls : std_logic_vector(portw-1 downto 0) := (others => '0');

    type reg_array is array (integer range <>) of
                        std_logic_vector(portw-1 downto 0);

    signal inp_regs : reg_array(0 to port_cnt-2);
    signal outputs  : reg_array(0 to port_cnt-1);
    signal cnt      : integer range 0 to port_cnt-1;

begin -- ser_par_conv

    -- Counter 0 to port_cnt-1; sequential
    counter : process (clk, rst_x)
        variable i : integer;
    begin -- counter_enable
        if rst_x = '0' then
            cnt <= 0;
        elsif rising_edge(clk) then
            if (glock = '0') and (en = '0') then
                if cnt >= port_cnt-1 then
                    cnt <= 0;
                else
                    cnt <= cnt + 1;
                end if;
            end if;
        end if;
    end process counter;

    -- Input registers (there is port_cnt-1 registers); sequential
    regs : process (clk, rst_x)
    begin -- regs
        if rst_x = '0' then
            inp_regs <= (others => nulls);
        elsif rising_edge(clk) then
            if (glock = '0') and (en = '0') then
                -- input register
                if cnt < port_cnt-1 then
                    inp_regs(cnt) <= data_in;
                end if;
            end if;
        end if;
    end process regs;

    -- Assign output and output enable signals; combinatorial
    out_assign : process (inp_regs, data_in, cnt)
    begin -- out_assign
        outputs(0 to port_cnt-2) <= inp_regs;
        outputs(port_cnt-1)      <= data_in;
        if cnt >= port_cnt-1 then
            en_out <= (others => '0');
        else
            en_out <= (others => '1');
        end if;
    end process out_assign;

    -- Concatenate output array into one long signal; combinatorial
    out_concatenate : process (outputs)
        variable high_idx : integer;
        variable i        : integer;
    begin -- out_concatenate
        for i in 0 to port_cnt-1 loop
            high_idx := data_out'high-i*portw;
            data_out(high_idx downto high_idx-portw+1) <= outputs(i);
        end loop;
    end process out_concatenate;

end Behavioral;
