--===========================================================================--
-- Synchronous singleport SRAM
--
-- Copyright (c) 2002-2009 Tampere University of Technology.

-- This file is a modified synch_sram.vhdl from TTA-Based Codesign
-- Environment:
-- https://github.com/cpc/tce/blob/release-1.16/tce/data/ProGe/tb/synch_sram.vhdl
--
-- Author     : Jaakko Sertamo
-- Description: Synchronous static random-access memory with bit write
--              capability. All the control signals are active low.
-- Changes    : Contains only synthesizable architecture (rtl) and uses only
--              standard libraries.
--===========================================================================--


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity synch_sram is
    generic (
        DATAW    : integer := 32;
        ADDRW    : integer := 7);
    port (
        clk      : in  std_logic;
        d        : in  std_logic_vector(DATAW-1 downto 0);
        addr     : in  std_logic_vector(ADDRW-1 downto 0);
        en_x     : in  std_logic;
        wr_x     : in  std_logic;
        bit_wr_x : in  std_logic_vector(DATAW-1 downto 0);
        q        : out std_logic_vector(DATAW-1 downto 0));
end synch_sram;

architecture rtl of synch_sram is

    type std_logic_matrix is array (natural range <>) of
      std_logic_vector (DATAW-1 downto 0);
    subtype word_line_index is integer range 0 to 2**ADDRW-1;

    signal mem_r : std_logic_matrix (0 to 2**ADDRW-1);
    signal line  : word_line_index;

    signal q_r : std_logic_vector(DATAW-1 downto 0);

begin  -- rtl
    line <= to_integer(unsigned(addr));

    -- purpose: read & write memory
    -- type     : sequential
    -- inputs : clk
    regs : process (clk)
    begin    -- process regs
        if rising_edge(clk) then  -- rising clock edge
            -- Memory write
            if (en_x = '0' and wr_x = '0') then
                mem_r(line) <= (d and (not bit_wr_x)) or
                               (mem_r(line) and bit_wr_x);
                -- bypass data to output register
                q_r         <= (d and (not bit_wr_x)) or
                               (mem_r(line) and bit_wr_x);
            -- Memory read
            elsif (en_x = '0') then
                q_r <= mem_r(line);
            end if;
        end if;
    end process regs;

    q <= q_r;

end rtl;
