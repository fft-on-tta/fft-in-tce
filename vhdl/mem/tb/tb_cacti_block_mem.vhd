--===========================================================================--
-- Testbench struct_cacti_blocks architecture of synch_singleport_mem_wrapper
--
-- Used for viewing the waves.
--===========================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_cacti_block_mem is
    -- port ( );
end tb_cacti_block_mem;

architecture testbench of tb_cacti_block_mem is

    component synch_singleport_mem_wrapper
        generic (
            DATAW    : integer;
            ADDRW    : integer);
        port (
            clk      : in  std_logic;
            rst_x    : in  std_logic;
            glock    : in  std_logic;
            d        : in  std_logic_vector(DATAW-1 downto 0);
            addr     : in  std_logic_vector(ADDRW-1 downto 0);
            en_x     : in  std_logic;
            wr_x     : in  std_logic;
            bit_wr_x : in  std_logic_vector(DATAW-1 downto 0);
            q        : out std_logic_vector(DATAW-1 downto 0));
    end component;

    constant ADDRW   : positive := 13;
    constant DATAW   : positive := 32;

    signal d        : std_logic_vector(DATAW-1 downto 0) := (others => '0');
    signal addr     : std_logic_vector(ADDRW-1 downto 0) := (others => '0');
    signal en_x     : std_logic := '1';
    signal wr_x     : std_logic := '1';
    signal bit_wr_x : std_logic_vector(DATAW-1 downto 0) := (others => '0');
    signal q        : std_logic_vector(DATAW-1 downto 0) := (others => '0');

    constant clk_period : time := 10 ns;
    signal clk          : std_logic := '0';
    signal rst_x        : std_logic := '0';
    signal glock        : std_logic := '1';

begin -- testbench

    -- dut
    dut : entity work.synch_singleport_mem_wrapper(struct_cacti_blocks)
        generic map (
            DATAW    => DATAW,
            ADDRW    => ADDRW)
        port map (
            clk      => clk,
            rst_x    => rst_x,
            glock    => glock,
            d        => d,
            addr     => addr,
            en_x     => en_x,
            wr_x     => wr_x,
            bit_wr_x => bit_wr_x,
            q        => q);

    -- clock signal
    clk <= not clk after clk_period/2;

    -- stimulus
    stim_proc : process
        variable int_addr : integer := 0;
        variable addr_max : integer := 2**ADDRW-1;
    begin -- stim_proc

        wait until rising_edge(clk);
        rst_x <= '1';
        glock <= '0';

        -- write values 0..2^ADDRW-1 to the same addresses
        wait until rising_edge(clk);
        en_x <= '0';
        wr_x <= '0';
        while int_addr <= addr_max loop
            addr <= std_logic_vector(to_unsigned(int_addr, addr'length));
            d    <= std_logic_vector(to_unsigned(int_addr, d'length));
            int_addr := int_addr + 1;
            wait until rising_edge(clk);
        end loop;

        wait until rising_edge(clk);
        en_x <= '0';
        wr_x <= '1';

        -- read from all addresses
        int_addr := 0;
        while int_addr <= addr_max loop
            addr <= std_logic_vector(to_unsigned(int_addr, addr'length));
            int_addr := int_addr + 1;
            wait until rising_edge(clk);
        end loop;

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        -- end the simulation
        assert false report "Simulation finished fine" severity failure;

    end process stim_proc;

end testbench;
