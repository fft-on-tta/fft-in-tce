--===========================================================================--
-- Testbench for block_en_wr_arbiter
--
-- Used for viewing the waves.
--===========================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_block_en_wr_arbiter is
    -- port ( );
end tb_block_en_wr_arbiter;

architecture testbench of tb_block_en_wr_arbiter is

    component block_en_wr_arbiter
        generic (
            ADDRW     : positive;
            NBLOCKS   : positive);
        port (
            en        : in  std_logic;
            wr        : in  std_logic;
            addr      : in  std_logic_vector(ADDRW-1 downto 0);
            en_blocks : out std_logic_vector(NBLOCKS-1 downto 0);
            wr_blocks : out std_logic_vector(NBLOCKS-1 downto 0));
    end component;

    constant ADDRW   : positive := 13;
    constant NBLOCKS : positive := 9;

    signal en        : std_logic := '1';
    signal wr        : std_logic := '1';
    signal addr      : std_logic_vector(ADDRW-1 downto 0) := (others => '0');
    signal en_blocks : std_logic_vector(NBLOCKS-1 downto 0) := (others => '1');
    signal wr_blocks : std_logic_vector(NBLOCKS-1 downto 0) := (others => '1');

    signal addr_32  : std_logic_vector(4 downto 0);
    signal addr_64  : std_logic_vector(5 downto 0);
    signal addr_128 : std_logic_vector(6 downto 0);
    signal addr_256 : std_logic_vector(7 downto 0);
    signal addr_512 : std_logic_vector(8 downto 0);
    signal addr_1k  : std_logic_vector(9 downto 0);
    signal addr_2k  : std_logic_vector(10 downto 0);
    signal addr_4k  : std_logic_vector(11 downto 0);

    constant clk_period : time := 10 ns;
    signal clk          : std_logic := '1';

begin -- testbench

    -- dut
    dut : block_en_wr_arbiter
        generic map (
            ADDRW     => ADDRW,
            NBLOCKS   => NBLOCKS)
        port map (
            en        => en,
            wr        => wr,
            addr      => addr,
            en_blocks => en_blocks,
            wr_blocks => wr_blocks);

    -- clock signal
    clk <= not clk after clk_period/2;

    -- preview addresses to different memotry blocks
    addr_32  <= addr(4 downto 0);
    addr_64  <= addr(5 downto 0);
    addr_128 <= addr(6 downto 0);
    addr_256 <= addr(7 downto 0);
    addr_512 <= addr(8 downto 0);
    addr_1k  <= addr(9 downto 0);
    addr_2k  <= addr(10 downto 0);
    addr_4k  <= addr(11 downto 0);

    -- stimulus
    stim_proc : process
        variable int_addr : integer := 0;
        variable addr_max : integer := 2**ADDRW-1;
    begin -- stim_proc

        wait until rising_edge(clk);
        en <= '0';
        wr <= '0';
        wait until rising_edge(clk);

        while int_addr <= addr_max loop
            addr <= std_logic_vector(to_unsigned(int_addr, addr'length));
            int_addr := int_addr + 1;
            wait until rising_edge(clk);
        end loop;

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        -- end the simulation
        assert false report "Simulation finished fine" severity failure;

    end process stim_proc;

end testbench;
