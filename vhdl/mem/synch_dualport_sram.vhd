--===========================================================================--
-- Synchronous dualport SRAM
--
-- Copyright (c) 2002-2009 Tampere University of Technology.

-- This file is a modified synch_dualport_sram.vhdl from TTA-Based Codesign
-- Environment:
-- https://github.com/cpc/tce/blob/release-1.16/tce/data/ProGe/tb/synch_dualport_sram.vhdl
--
-- Author     : Jaakko Sertamo
-- Description: Synchronous static dual-port random-access memory with bit
--              write capability. All the control signals are active low.
-- Changes    : Contains only synthesizable architecture (rtl) and uses only
--              standard libraries.
--===========================================================================--


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity synch_dualport_sram is
    generic (
        DATAW : integer := 32;
        ADDRW : integer := 15);
    port (
        clk : in std_logic;

        d_a : in std_logic_vector(DATAW-1 downto 0);
        d_b : in std_logic_vector(DATAW-1 downto 0);

        addr_a : in std_logic_vector(ADDRW-1 downto 0);
        addr_b : in std_logic_vector(ADDRW-1 downto 0);
        en_a_x : in std_logic;
        en_b_x : in std_logic;
        wr_a_x : in std_logic;
        wr_b_x : in std_logic;

        bit_wr_a_x : in std_logic_vector(DATAW-1 downto 0);
        bit_wr_b_x : in std_logic_vector(DATAW-1 downto 0);

        q_a : out std_logic_vector(DATAW-1 downto 0);
        q_b : out std_logic_vector(DATAW-1 downto 0));
end synch_dualport_sram;

architecture rtl of synch_dualport_sram is

    type std_logic_matrix is array (natural range <>) of
      std_logic_vector (DATAW-1 downto 0);
    subtype mem_line_index is integer range 0 to 2**ADDRW-1;

    signal  mem_r       : std_logic_matrix (2**ADDRW-1 downto 0);
    signal  word_line_a : mem_line_index;
    signal  word_line_b : mem_line_index;

    signal q_a_r : std_logic_vector(DATAW-1 downto 0);
    signal q_b_r : std_logic_vector(DATAW-1 downto 0);

begin  -- rtl

    word_line_a <= to_integer(unsigned(addr_a));
    word_line_b <= to_integer(unsigned(addr_b));

    -- purpose: read & write memory
    -- type   : sequential
    -- inputs : clk
    regs : process (clk)
    begin  -- process regs
        if rising_edge(clk) then -- rising clock edge

            -- Memory read a
            if (en_a_x = '0' and wr_a_x = '0') then
                -- bypass data to output register
                q_a_r <= (d_a and (not bit_wr_a_x)) or
                         (mem_r(word_line_a) and bit_wr_a_x);
            elsif (en_a_x = '0') then
                q_a_r <= mem_r(word_line_a);
            end if;
            -- Memory read b
            if (en_b_x = '0' and wr_b_x = '0') then
                -- bypass data to output register
                q_b_r <= (d_b and (not bit_wr_b_x)) or
                         (mem_r(word_line_b) and bit_wr_b_x);
            elsif (en_b_x = '0') then
                q_b_r <= mem_r(word_line_b);
            end if;

            -- Memory write a
            if (en_a_x = '0') and (wr_a_x = '0') then
                mem_r(word_line_a) <= (d_a and (not bit_wr_a_x)) or
                                      (mem_r(word_line_a) and bit_wr_a_x);
            end if;
            -- Memory write b
            if (en_b_x = '0') and (wr_b_x = '0') then
                mem_r(word_line_b) <= (d_b and (not bit_wr_b_x)) or
                                      (mem_r(word_line_b) and bit_wr_b_x);
            end if;

        end if;
    end process regs;

    q_a <= q_a_r;
    q_b <= q_b_r;

end rtl;
