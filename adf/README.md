# adf #

Architecture definition files. They are used in ProDe.

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/fft-in-tce/adf
├── bfly_1inst.adf      // The most final architecture
├── bfly.adf            // Previous FFT architecture
├── minimal_fft_4b.adf  // Minimal architectures are used for testing
├── minimal_fft_7b.adf  // ...
├── minimal_fft.adf     // ...
└── README.md
~~~~
